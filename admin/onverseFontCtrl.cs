
populateFontCacheRange("Arial", 8, 1, 256);
populateFontCacheRange("Arial", 12, 1, 256);
populateFontCacheRange("Arial", 13, 1, 256);
populateFontCacheRange("Arial", 14, 1, 256);
populateFontCacheRange("Arial", 15, 1, 256);
populateFontCacheRange("Arial", 16, 1, 256);
populateFontCacheRange("Arial", 18, 1, 256);
populateFontCacheRange("Arial", 24, 1, 256);
populateFontCacheRange("Arial", 50, 1, 256);
populateFontCacheRange("Arial Bold", 14, 1, 256);
populateFontCacheRange("Arial Bold", 15, 1, 256);
populateFontCacheRange("Arial Bold", 16, 1, 256);
populateFontCacheRange("Arial Bold", 18, 1, 256);
populateFontCacheRange("Arial Bold", 20, 1, 256);
populateFontCacheRange("Arial Bold", 21, 1, 256);
populateFontCacheRange("Arial Bold", 22, 1, 256);
populateFontCacheRange("Arial Bold", 24, 1, 256);
populateFontCacheRange("Arial Bold", 25, 1, 256);
populateFontCacheRange("Arial Bold", 28, 1, 256);
populateFontCacheRange("Arial Bold", 32, 1, 256);
populateFontCacheRange("Lucida Console", 12, 1, 256);
writeFontCache();

