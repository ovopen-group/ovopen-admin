$Relight::Current = 0;

exec("./relightList.cs");

$Relight::mod = 1;
$Relight::modIdx = 0;
$Relight::force = 0;
$MasterServer::address = "onverseworld.onverse.local";
$MasterServer::port = 65355;
$Player::name = "";
$Player::password = "Onverse123";
$debug::enabled = 0;
$debug::port = 6060;
$debug::password = "password";

function processArguments()
{
    for (%i=1; %i < $Game::argc; %i++)
    {
        %arg = $Game::argv[%i];
        %nextArg = $Game::argv[%i + 1];
        %hasNextArg = ($Game::argc - %i) > 1;
        if (%arg $= "-f")
        {
            $Relight::force = 1;
        }
        else
        {
            if (%arg $= "-a")
            {
                if (%hasNextArg)
                {
                    $MasterServer::address = %nextArg;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -a <address>");
                }
            }
            else
            {
                if (%arg $= "-p")
                {
                    if (%hasNextArg)
                    {
                        $MasterServer::port = %nextArg;
                    }
                    else
                    {
                        error("Error: Missing Command Line argument. Usage: -p <port>");
                    }
                }
                else
                {
                    if (%arg $= "--mod")
                    {
                        if (%hasNextArg)
                        {
                            $Relight::mod = %nextArg;
                        }
                        else
                        {
                            error("Error: Missing Command Line argument. Usage: --mod <modulo>");
                        }
                    }
                    else
                    {
                        if (%arg $= "--idx")
                        {
                            if (%hasNextArg)
                            {
                                $Relight::modIdx = %nextArg;
                            }
                            else
                            {
                                error("Error: Missing Command Line argument. Usage: --idx <index>");
                            }
                        }
                        else
                        {
                            if (%arg $= "--debug")
                            {
                                $debug::enabled = 1;
                            }
                        }
                    }
                }
            }
        }
    }
    return 1;
}

function OpenALInit()
{
    $pref::Audio::driver = "OpenAL";
    $pref::Audio::forceMaxDistanceUpdate = 0;
    $pref::Audio::environmentEnabled = 0;
    $pref::Audio::masterVolume = 0.8;
    $pref::Audio::channelVolume1 = 0.8;
    $pref::Audio::channelVolume2 = 0.8;
    $pref::Audio::channelVolume3 = 0.8;
    $pref::Audio::channelVolume4 = 0.8;
    $pref::Audio::channelVolume5 = 0.8;
    $pref::Audio::channelVolume6 = 0.8;
    $pref::Audio::channelVolume7 = 0.8;
    $pref::Audio::channelVolume8 = 0.8;
    OpenALShutdownDriver();
    echo("");
    echo("OpenAL Driver Init:");
    echo($pref::Audio::driver);

    if ($pref::Audio::driver $= "OpenAL")
    {
        if (!OpenALInitDriver())
        {
            error("   Failed to initialize driver.");
            $Audio::initFailed = 1;
        }
        else
        {
            echo("   Vendor: " @ alGetString("AL_VENDOR"));
            echo("   Version: " @ alGetString("AL_VERSION"));
            echo("   Renderer: " @ alGetString("AL_RENDERER"));
            echo("   Extensions: " @ alGetString("AL_EXTENSIONS"));
            alxListenerf(AL_GAIN_LINEAR, $pref::Audio::masterVolume);

            for (%channel=1; %channel <= 8; %channel++)
            {
                alxSetChannelVolume(%channel, $pref::Audio::channelVolume[%channel]);
            }
        }
    }
}

function OpenALShutdown()
{
    OpenALShutdownDriver();
}

function onStart()
{
    setLogMode(5);
    enableWinConsole(1);
    headlessDisplay();
    dbgSetParameters($debug::port, $debug::password, $debug::enabled);
    setNetPort(0);

    if (!processArguments())
    {
        quit();
    }

    if ($Player::name $= "")
    {
        $Player::name = "lm" @ $Relight::modIdx + 1;
    }

    OpenALInit();
    ConnectToMasterserver();
}

function ConnectToMasterserver()
{
    if (isObject(MasterServerConnection))
    {
        MasterServerConnection.delete();
    }

    echo("Connecting to master server " @ $MasterServer::address @ ":" @ $MasterServer::port);

    new MasterServerObject(MasterServerConnection) {
    };

    MasterServerConnection.connect("ip:" @ $MasterServer::address @ ":" @ $MasterServer::port);
    MasterServerConnection.address = $MasterServer::address;
}

function onExit()
{
    MasterServerConnection.disconnect();
    OpenALShutdown();
}

function ASSERT(%cond, %msg)
{
    if (!%cond)
    {
        error(%msg);
        quit();
    }
}

function LoadCommonDatablocks()
{
    exec("onverse/common/avatarAnimation.cs");
    exec("onverse/common/audio.cs");
}

function DoNextRelight()
{
    if (isObject(LocationGameConnection))
    {
        LocationGameConnection.delete();
        schedule(1500, 0, DoNextRelight);
        return;
    }

    $Relight::Current = $Relight::Current + 1;
    while (($Relight::Current % $Relight::mod) != $Relight::modIdx)
    {
        $Relight::Current = $Relight::Current + 1;
    }

    deleteDataBlocks();
    LoadCommonDatablocks();
    purgeResources();

    if ($Relight::Current > $Relight::Locations)
    {
        echo("Done relighting.");
        quit();
    }
    else
    {
        resolveURL("ONVERSE://location/" @ $Relight::Location[$Relight::Current]);
    }
}

function resolveURL(%url)
{
    echo("Resloving URL: \"" @ %url @ "\"");
    $CurrentResolveURL[locationID] = -1;
    $CurrentResolveURL[location] = "";
    $CurrentResolveURL[instanceID] = -1;
    $CurrentResolveURL[instance] = "";
    $CurrentResolveURL[poi] = "";
    %rpcObject = new RPC_ResolveURL("") {
    };
    %rpcObject.resolveURL(%url);
    %rpcObject.fireRPC(MasterServerConnection);
}

function ConnectToInstance()
{
    ASSERT($CurrentResolveURL[locationID] != -1, "Bad Location ID");
    if ($CurrentResolveURL[instanceID] == -1)
    {
        echo("Resloving instance list for location:" @ $CurrentResolveURL[location]);
        %rpcObject = new RPC_ResolveURL("")
        {
        };
        %rpcObject.getInstances($CurrentResolveURL[locationID]);
        %rpcObject.fireRPC(MasterServerConnection);
    }
    else
    {
        echo("Requesting Instance:" @ $CurrentResolveURL[instance] SPC "in" SPC $CurrentResolveURL[location]);
        %rpcObject = new RPC_RequestInstance("") {
        };
        %rpcObject.locationID = $CurrentResolveURL[locationID];
        %rpcObject.instanceID = $CurrentResolveURL[instanceID];
        %rpcObject.fireRPC(MasterServerConnection);
    }
}

function RPC_ResolveURL::onResolvedLocation(%this, %locationID, %location, %instanceID, %instance, %poi)
{
    $CurrentResolveURL[locationID] = %locationID;
    $CurrentResolveURL[location] = %location;
    $CurrentResolveURL[instanceID] = %instanceID;
    $CurrentResolveURL[instance] = %instance;
    $CurrentResolveURL[poi] = %poi;
    ConnectToInstance();
}

function RPC_ResolveURL::onInstanceListReceived(%this, %location, %numInsts, %instanceListString)
{
    echo("Received (" @ %numInsts @ ") instances list for:" SPC %location);
    %i = 0;
    for (%i=0; %i < %numInsts; %i++)
    {
        %rowdata = getRecord(%instanceListString, %i);
        %instID = getField(%rowdata, 0);

        if (%instID != 0)
        {
            resolveURL("ONVERSE://location/" @ $CurrentResolveURL[location] @ "/" @ getField(%rowdata, 1) @ "/");
            return;
        }
    }

    ASSERT(0, "Could not find non-builder instance for:" @ $CurrentResolveURL[location]);
}

function RPC_ResolveURL::onReturnURLError(%this)
{
    ASSERT(0, "[URL Error]: There was an error resolving the URL");
}

function RPC_ResolveURL::onReturnUnknownLocation(%this)
{
    ASSERT(0, "[URL Error]: The requested location is Unkown");
}

function RPC_ResolveURL::onReturnUnknownInstance(%this)
{
    ASSERT(0, "[URL Error]: The requested instance is Unkown");
}

function RPC_ResolveURL::onReturnBadURLQaul(%this)
{
    ASSERT(0, "[URL Error]: The URL had a bad qualifier");
}

function RPC_ResolveURL::onReturnInvalidURL(%this)
{
    ASSERT(0, "[URL Error]: The requested url syntax is invalid");
}

function RPC_ResolveURL::onReturnInvalidUsername(%this)
{
    ASSERT(0, "[URL Error]: That username is invalid.");
}

function RPC_ResolveURL::onReturnUserOffline(%this)
{
    ASSERT(0, "[URL Error]: That user is currently offline.");
}

function RPC_ResolveURL::onReturnUserNoLoc(%this)
{
    ASSERT(0, "[URL Error]: That user is not currently in a location.");
}

function RPC_ResolveURL::onReturnUserNoHouse(%this)
{
    ASSERT(0, "[URL Error]: That user does not own a home.");
}

function RPC_ResolveURL::onReturnTeleportDenied(%this)
{
    ASSERT(0, "[URL Error]: That user is not accepting teleports.");
}

function OnverseServerObject::onBroadcastCall(%this, %functionName, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a0)
{
    %this.schedule(0, %functionName, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a0);
}

function MasterServerObject::onDisconnect(%this)
{
    echo("Master server connection disconnected.");
}

function MasterServerObject::onDropped(%this)
{
    ASSERT(0, "Master server connection dropped.");
}

function MasterServerObject::onConnectFailed(%this)
{
    ASSERT(0, "Connection to master server failed.");
}

function MasterServerObject::onServerError(%this, %code)
{
    %msg = "Unkown";
    if (%code == 4)
    {
        if (%this.authFailCode == 2)
        {
            %msg = "Authorization failed bad username/password";
        }
        else
        {
            if (%this.authFailCode == 3)
            {
                %msg = "Connection denied. This usually happens while the servers are updating.";
            }
            else
            {
                if (%this.authFailCode == 4)
                {
                    %msg = "Your account is banned from Onverse.";
                }
                else
                {
                    if (%this.authFailCode == 5)
                    {
                        %msg = "Your account is currently suspended.";
                    }
                    else
                    {
                        %msg = "Unknown permissions error.";
                    }
                }
            }
        }
    }
    else
    {
        if (%code == 8)
        {
            %msg = "Your connection was kicked from the server.";
        }
    }
    ASSERT(0, "Master server connection error:(" @ %code @ ")" SPC %msg);
}

function MasterServerObject::onRequestAuth(%this)
{
    %this.SendAuthResponse($Player::name, $Player::password);
}

function MasterServerObject::onAuthSuccess(%this, %uniqueID, %authlevel)
{
    $currentPlayerID = %uniqueID;
    echo("Connected to Master Server with authLevel:" @ %authlevel);
    DoNextRelight();
}

function MasterServerObject::onAuthFail(%this, %code)
{
    ASSERT(0, "Authorization failed with code: " @ %code);
}

function MasterServerObject::onInstanceReady(%this, %location, %instance, %port)
{
    echo("Instance ready:" @ %location SPC %instance SPC %port);
    MakeLocationConnection(%this.address, %port);
}

function MasterServerObject::onEntireFriendList(%this, %numFriends)
{
    echo("Got" SPC %numFriends SPC "friends");
}

function MasterServerObject::onAddEntireFriend(%this, %friendID, %uname, %state)
{
    echo("Add friend:[" @ %friendID @ "](" @ %state @ "):" @ %uname);
}

function MasterServerObject::onFriendUpdate(%this, %friendID, %uname, %state)
{
    echo("Update friend:[" @ %friendID @ "](" @ %state @ "):" @ %uname);
}

function MasterServerObject::onPrivateMessage(%this, %type, %from, %message)
{
    echo("**[Private Message(" @ %type @ ")]: FROM:{" @ %from @ "}:" SPC %message);
}

function MasterServerObject::onGuideReplyMessage(%this, %from, %unused, %message)
{
    echo("**[Guide Message(" @ %type @ ")]: FROM:{" @ %from @ "}:" SPC %message);
}

function MasterServerObject::onMasterVersion(%this, %serverversion)
{
    echo("Master version:" SPC %serverversion);
}

function MasterServerObject::onServerMessage(%this, %message)
{
    echo("**[Server Message]:" SPC %message);
}

function MasterServerObject::onServerAnnouncement(%this, %message)
{
    echo("**[Server Announcement]:" SPC %message);
}

function MakeLocationConnection(%address, %port)
{
    if (isObject(LocationGameConnection))
    {
        LocationGameConnection.delete();
    }

    echo("Connecting to location server at" SPC %address SPC "on port:" @ %port);
    
    %connection = new GameConnection(LocationGameConnection) {
    };
    %connection.setCredentials($Player::name, $Player::password);
    %connection.setConnectArgs(0, 0);
    %connection.connect("ip:" @ %address @ ":" @ %port);
    $progressBar::NextStage = 0;
    $progressBar::filename = "";
}

function GameConnection::onConnectionAccepted(%this)
{
    echo("Connection Accepted");
}

function GameConnection::onConnectRequestRejected(%this, %msg)
{
    ASSERT(0, "Error occured while connecting (" @ %msg @ ")");
}

function GameConnection::onConnectionDropped(%this, %reason)
{
    ASSERT(0, "Location connection dropped for reason: " @ %reason);
}

function GameConnection::onConnectRequestTimedOut(%this)
{
    ASSERT(0, "Location connection request timed out");
}

function GameConnection::onConnectionError(%this, %errorstring)
{
    ASSERT(0, "Connection Error: " @ %errorstring);
}

function GameConnection::onConnectionTimedOut(%this)
{
    ASSERT(0, "Location connection timed out");
}

function GameConnection::initialControlSet(%this)
{
    ASSERT(0, "Connection does not get this far!");
}

function onDataBlockObjectReceived(%index, %total)
{
    %pct = mFloor(((%index / %total) * 100) + 0.5);
    if (%pct >= $progressBar::NextStage)
    {
        echo("Datablocks:" SPC %pct @ "%");
        $progressBar::NextStage = $progressBar::NextStage + 25;
    }
}

function onGhostAlwaysStarted(%ghostCount)
{
    $ghostCount = %ghostCount;
    $ghostsRecvd = 0;
    $progressBar::NextStage = 0;
}

function onGhostAlwaysObjectReceived()
{
    $ghostsRecvd++;
    %pct = mFloor((($ghostsRecvd / $ghostCount) * 100) + 0.5);
    if (%pct >= $progressBar::NextStage)
    {
        echo("Ghost:" SPC %pct @ "%");
        $progressBar::NextStage = $progressBar::NextStage + 25;
    }
}

function onFileChunkReceived(%filename, %ofs, %size)
{
    if (!($progressBar::filename $= %filename))
    {
        $progressBar::NextStage = 0;
        $progressBar::filename = %filename;
    }

    %pct = mFloor(((%ofs / %size) * 100) + 0.5);
    if (%pct >= $progressBar::NextStage)
    {
        echo(%filename @ ":" SPC %pct @ "%");
        $progressBar::NextStage = $progressBar::NextStage + 25;
    }
}

function ClientCmdBeginLocationDownload()
{
    LocationGameConnection.setTurbo(1);
    LocationGameConnection.isInGame = 0;
    $currentLocationDownloadPhase = 1;
    commandToServer('LocationDownloadAck');
}

function ClientCmdLocationDownloadPhase2()
{
    if ($currentLocationDownloadPhase != 1)
    {
        echo("Received old LocationDownloadPhase2");
        return;
    }

    $currentLocationDownloadPhase = 2;
    commandToServer('LocationDownloadPhase2Ack');
}

function ClientCmdLocationDownloadPhase3()
{
    if ($currentLocationDownloadPhase != 2)
    {
        echo("Received old LocationDownloadPhase3");
        return;
    }

    $currentLocationDownloadPhase = 3;
    StartFoliageReplication();
    StartClientReplication();
    commandToServer('LocationDownloadPhase3Ack');
}

function ClientCmdLocationDownloadPhase4(%isbuilder, %sd_id)
{
    if ($currentLocationDownloadPhase != 3)
    {
        echo("Recived old LocationDownloadPhase4");
        return;
    }

    ASSERT(%isbuilder == 0, "Ended up in a builder instance?");
    $currentLocationDownloadPhase = 4;
    $location::builder = %isbuilder;
    LocationGameConnection.setTurbo(0);

    if ($location::builder)
    {
        $Client::LightingFile = "onverse/data/live_assets/engine/terrain/tempBuilder_ter_";
    }
    else
    {
        $Client::LightingFile = "onverse/data/live_assets/engine/terrain/ter_";
    }

    $Client::LightingFile = $Client::LightingFile @ %sd_id;
    schedule(10, 0, DoPhase4);
}

function DoPhase4()
{
    ASSERT($location::builder == 0, "Ended up in a builder instance?");
    $oldQuality = $pref::LightManager::sgLightingProfileQuality;
    $pref::LightManager::sgLightingProfileQuality = 0;

    if ($Relight::force)
    {
        %mode = "forceAlways";
    }
    else
    {
        %mode = "";
    }

    runLighting(0, %mode);
}

function DoneLightingPhase4()
{
    $pref::LightManager::sgLightingProfileQuality = $oldQuality;
    schedule(0, 0, DoNextRelight);
}

function runLighting(%filtered, %mode)
{
    $lightingSch = 0;
    $SceneLighting::sgFilterRelight = %filtered;

    if (lightScene("sceneLightingComplete", %mode))
    {
    }
}

function updateLightingProgress()
{
    if ($SceneLighting::lightingProgress != $SceneLighting::last_lightingProgress)
    {
        echo("Lighting: " @ $SceneLighting::lightingProgress);
    }

    $lightingSch = schedule(32, 0, "updateLightingProgress");
}

function sceneLightingComplete()
{
    if ($lightingSch != 0)
    {
        cancel($lightingSch);
    }

    DoneLightingPhase4();
}
