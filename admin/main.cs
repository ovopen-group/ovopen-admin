
package adminSystem
{
    function includeFiles()
    {
        Parent::includeFiles();

        echo("Including admin system Files...");
        exec("admin/default.bind.cs");
        exec("admin/ui/AdminMenu.cs");
        exec("admin/ui/AdminServerManagement.cs");
        exec("admin/ui/categories/CatManager.cs");
        exec("admin/ui/categories/LoadDBItem.cs");
        exec("admin/ui/createobject/createClothingObject.cs");
        exec("admin/ui/createobject/createInteractiveObject.cs");
        exec("admin/ui/createobject/createInteriorObject.cs");
        exec("admin/ui/createobject/createLightObject.cs");
        exec("admin/ui/createobject/createNPCObject.cs");
        exec("admin/ui/createobject/createParticleObject.cs");
        exec("admin/ui/createobject/createPetObject.cs");
        exec("admin/ui/createobject/createSceneObject.cs");
        exec("admin/ui/createobject/createToolObject.cs");
        exec("admin/ui/createobject/createTravelMountObject.cs");
        exec("admin/ui/createobject/storeFolders.cs");
        exec("admin/ui/ConsoleDlg.gui");
        exec("admin/ui/debugger/debugger.cs");
        exec("admin/ui/NetGraphGui.gui");
        exec("admin/ui/metrics.cs");
        exec("admin/ui/InspectDlg.gui");
        exec("admin/ui/InspectInteriorLightmaps.cs");
        exec("admin/adminCommands.cs");
        exec("admin/hackConfigureClothing.gui");
    }
    function onStart()
    {
        Parent::onStart();
        echo("--------- Initializing: Onverse Administrative System ---------");
    }
};

activatePackage(adminSystem);

