
_GCommandParser.addCommand("KickAll", 0, 0, AC_KickAll, " (Kick all players from the entire world.)");
_GCommandParser.addCommand("SimulatedNetParams", 2, 2, AC_SimulatedNetParams, "[PacketLoss] [Latency] (Simulate network issues on both client and server. PacketLoss (0-1) Latency MS)");
_GCommandParser.addCommand("Debug", 1, 1, AC_Debug, "[port] (Start accepting debug connection on port)");

function AC_KickAll(%confirm)
{
    if (!MasterServerConnection.hasNetworkAdminLevel())
    {
        return "You do not have permision to kick all players.";
    }

    if (%confirm)
    {
        %rpcObject = new RPC_CustomerService() {
        };
        %rpcObject.Command = COMMAND_KICKALL;
        %rpcObject.fireRPC(MasterServerConnection);
        return;
    }

    else
    {
        MessageBoxYesNo("You Sure?", "Are you sure you want to kick all players.", "AC_KickAll(true);", "");
    }
    return "Kicking";
}

function AC_SimulatedNetParams(%packetloss, %latency)
{
    if (!MasterServerConnection.hasNetworkAdminLevel())
    {
        return "You do not have permision to change network params.";
    }

    $Net::simPacketLoss = %packetloss;
    $Net::simLatency = mFloor(%latency / 2);
    
    if (isObject(LocationGameConnection))
    {
        commandToServer('SimulatedNetParams', $Net::simPacketLoss, $Net::simLatency);
        LocationGameConnection.setSimulatedNetParams($Net::simPacketLoss, $Net::simLatency);
    }
    
    return "Done";
}

function AC_Debug(%port)
{
    dbgSetParameters(%port, "password", 0);
    return "Done";
}

_GCommandParser.addCommand("SetClothingMask", 1, 1, AC_SetClothingMask, "[Bits] (Set your avatar\'s clothing mask)");
_GCommandParser.addCommand("SetMeshMask", 1, 1, AC_SetMeshMask, "[Bits] (Set your avatar\'s mesh mask)");
_GCommandParser.addCommand("AcquireItem", 2, 2, AC_AcquireItem, "[Type] [ItemID] (Add item \'ItemID\' of type \'Type\' to your inventory)");

function AC_SetClothingMask(%bits)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Error", "You must be connected to an instance to set your mesh mask.");
        return;
    }
    
    if (!LocationGameConnection.hasDeveloperLevel())
    {
        return "You do not have permision to change your clothing mask.";
    }

    commandToServer('SetClothingMask', %bits);
    return "Done.";
}
function AC_SetMeshMask(%bits)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Error", "You must be connected to an instance to set your mesh mask.");
        return;
    }

    if (!LocationGameConnection.hasDeveloperLevel())
    {
        return "You do not have permision to change your clothing mask.";
    }
    
    commandToServer('SetMeshMask', %bits);
    return "Done.";
}

function AC_AcquireItem(%type, %item)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Error", "You must be connected to an instance to Acquire items.");
        return;
    }
    
    if (!LocationGameConnection.hasDeveloperLevel())
    {
        return "You do not have permision to acquire items.";
    }

    commandToServer('AcquireItem', %type, %item);
    return "Done.";
}

_GCommandParser.addCommand("CopyLocation", 2, 2, AC_CopyLocation, "[locFrom] [locTo] (Create a copy of the \'locFrom\' location and name it \'locTo\')");
_GCommandParser.addCommand("DeleteLocation", 1, 1, AC_DeleteLocation, "[location] (Delete the location)");
_GCommandParser.addCommand("CreateInstance", 2, 2, AC_CreateInstance, "[location] [instanceName] (Create an instance named \'instanceName\' under \'location\')");
_GCommandParser.addCommand("CloseInstance", 2, 2, AC_CloseInstance, "[location] [instance] (Close the requested instance)");
_GCommandParser.addCommand("KickInstanceUser", 2, 2, AC_KickInstanceUser, "[username] [location] [instance] (Kick the user from the requested instance)");
_GCommandParser.addCommand("StartInstance", 2, 2, AC_StartInstance, "[location] [instance] (Start the requested instance)");

function AC_CopyLocation(%locFrom, %locTo, %conf)
{
    if (!MasterServerConnection.hasDeveloperLevel())
    {
        return "You do not have permission to copy locations.";
    }
    if (%conf $= "")
    {
        %function = "AC_CopyLocation(\"" @ %locFrom @ "\",\"" @ %locTo @ "\",true);";
        MessageBoxYesNo("You Sure?", "Do you want to copy location" SPC %locFrom SPC "to" SPC %locTo @ "?", %function, "");
        return "Copying location.";
    }
    %rpcObject = new RPC_ServerManagement() {
    };

    %rpcObject.Command = COMMAND_COPY_LOCATION;
    %rpcObject.arguments = %locFrom;
    %rpcObject.arguments = %locTo;
    %rpcObject.fireRPC(MasterServerConnection);
    return "";
}

function AC_DeleteLocation(%location, %conf)
{
    if (!MasterServerConnection.hasDeveloperLevel())
    {
        return "You do not have permission to delete locations.";
    }

    if (%conf $= "")
    {
        %function = "AC_DeleteLocation(\"" @ %location @ "\",true);";
        MessageBoxYesNo("You Sure?", "Do you want to delete location" SPC %location @ "?", %function, "");
        return "Deleting location.";
    }
    
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_DELETE_LOCATION;
    %rpcObject.arguments = %location;
    %rpcObject.fireRPC(MasterServerConnection);
    return "";
}

function AC_CreateInstance(%location, %instanceName, %conf)
{
    if (!MasterServerConnection.hasDeveloperLevel())
    {
        return "You do not have permission to create instances.";
    }

    if (%conf $= "")
    {
        %function = "AC_CreateInstance(\"" @ %location @ "\",\"" @ %instanceName @ "\",true);";
        MessageBoxYesNo("You Sure?", "Do you want to create instance" SPC %instanceName SPC "under location" SPC %location @ "?", %function, "");
        return "Creating instance.";
    }

    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_CREATE_INSTANCE;
    %rpcObject.arguments = %location;
    %rpcObject.arguments = %instanceName;
    %rpcObject.fireRPC(MasterServerConnection);
    return "";
}

function AC_CloseInstance(%location, %instance, %conf)
{
    if (!MasterServerConnection.hasDeveloperLevel())
    {
        return "You do not have permission to close instances.";
    }

    if (%conf $= "")
    {
        %function = "AC_CloseInstance(\"" @ %location @ "\",\"" @ %instance @ "\",true);";
        MessageBoxYesNo("You Sure?", "Do you want to close instance" SPC %location @ "/" @ %instance @ "?", %function, "");
        return "Closing Instance.";
    }

    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_CLOSE_INSTANCE;
    %rpcObject.arguments = %location;
    %rpcObject.arguments = %instance;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AC_KickInstanceUser(%username, %location, %instance, %conf)
{
    if (!MasterServerConnection.hasDeveloperLevel())
    {
        return "You do not have permission to kick users.";
    }
    
    if (%conf $= "")
    {
        %function = "AC_KickInstanceUser(\"" @ %username @ "\",\"" @ %location @ "\",\"" @ %instance @ "\",true);";
        MessageBoxYesNo("You Sure?", "Do you want to kick all users in the instance" SPC %location @ "/" @ %instance @ "?", %function, "");
        return "Kicking user.";
    }

    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_EMPTY_INSTANCE;
    %rpcObject.arguments = %location;
    %rpcObject.arguments = %instance;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AC_StartInstance(%location, %instance)
{
    if (!MasterServerConnection.hasDeveloperLevel())
    {
        return "You do not have permission to start instances.";
    }

    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_START_INSTANCE;
    %rpcObject.arguments = %location;
    %rpcObject.arguments = %instance;
    %rpcObject.fireRPC(MasterServerConnection);
    
    return "Starting Instance.";
}

_GCommandParser.addCommand("PermDisable", 0, 0, AC_PermDisable, " (Disable client side permissions)");
_GCommandParser.addCommand("PermEnable", 0, 0, AC_PermEnable, " (Enable client side permissions)");

function AC_PermDisable()
{
    activatePackage(permissionOverRide);
    return "Permissions disabled.";
}

function AC_PermEnable()
{
    deactivatePackage(permissionOverRide);
    return "Permissions enabled.";
}

package permissionOverRide
{
    function OnverseServerObject::getSecLevel()
    {
        return "-1";
    }
    function OnverseServerObject::hasUserLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasGuideLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasSuperGuideLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasCommManagerLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasAdminLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasSuperAdminLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasNetworkAdminLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasRootAdminLevel()
    {
        return "1";
    }
    function OnverseServerObject::hasDeveloperLevel()
    {
        return "1";
    }
};


