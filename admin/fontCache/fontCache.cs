
function onStart()
{
    setLogMode(5);
    enableWinConsole(1);
    headlessDisplay();
    schedule(0, 0, populateFontCache);
}

function onExit()
{
}

function buildCustomFont(%font, %basefont, %size, %rangeStart, %rangeEnd)
{
    populateFontCacheRange(%basefont, %size, %rangeStart, %rangeEnd);
    duplicateCachedFont(%basefont, %size, %font);
    exportCachedFont(%font, %size, "onverse/data/custom_fonts/out/" @ %font @ ".png", 2, 5);
}

function buildCustomFontString(%font, %basefont, %size, %string)
{
    populateFontCacheString(%basefont, %size, %string);
    duplicateCachedFont(%basefont, %size, %font);
    exportCachedFont(%font, %size, "onverse/data/custom_fonts/out/" @ %font @ ".png", 2, 5);
}

function importCustomFont(%font, %size)
{
    importCachedFont(%font, %size, "onverse/data/custom_fonts/in/" @ %font @ ".png", 2, 5);
}

function buildCustomFonts()
{
    buildCustomFontString("SpeedO", "Arial", 78, "1234567890- ");
}

function importCustomFonts()
{
    importCustomFont("SpeedO", 78);
}

function populateFontCache()
{
    $Gui::fontCacheDirectory = expandFilename("cache");
    populateFontCacheRange("Arial", 8, 1, 256);
    populateFontCacheRange("Arial", 12, 1, 256);
    populateFontCacheRange("Arial", 13, 1, 256);
    populateFontCacheRange("Arial", 14, 1, 256);
    populateFontCacheRange("Arial", 15, 1, 256);
    populateFontCacheRange("Arial", 16, 1, 256);
    populateFontCacheRange("Arial", 18, 1, 256);
    populateFontCacheRange("Arial", 24, 1, 256);
    populateFontCacheRange("Arial", 50, 1, 256);
    populateFontCacheRange("Arial Bold", 14, 1, 256);
    populateFontCacheRange("Arial Bold", 15, 1, 256);
    populateFontCacheRange("Arial Bold", 16, 1, 256);
    populateFontCacheRange("Arial Bold", 18, 1, 256);
    populateFontCacheRange("Arial Bold", 20, 1, 256);
    populateFontCacheRange("Arial Bold", 21, 1, 256);
    populateFontCacheRange("Arial Bold", 22, 1, 256);
    populateFontCacheRange("Arial Bold", 24, 1, 256);
    populateFontCacheRange("Arial Bold", 25, 1, 256);
    populateFontCacheRange("Arial Bold", 28, 1, 256);
    populateFontCacheRange("Arial Bold", 32, 1, 256);
    populateFontCacheRange("Lucida Console", 12, 1, 256);
    buildCustomFonts();
    importCustomFonts();
    writeFontCache();
    quit();
}
