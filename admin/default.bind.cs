
$MFDebugRenderMode = 0;

function cycleDebugRenderMode(%val)
{
    if (!%val)
    {
        return;
    }
    if ($MFDebugRenderMode == 0)
    {
        $MFDebugRenderMode = 1;
        GLEnableOutline(1);
    }
    else
    {
        if ($MFDebugRenderMode == 1)
        {
            $MFDebugRenderMode = 2;
            GLEnableOutline(0);
            setInteriorRenderMode(7);
        }
        else
        {
            if ($MFDebugRenderMode == 2)
            {
                $MFDebugRenderMode = 0;
                setInteriorRenderMode(0);
                GLEnableOutline(0);
            }
        }
    }
}

GlobalActionMap.bind(keyboard, "F9", cycleDebugRenderMode);
GlobalActionMap.bind(keyboard, "tilde", toggleConsole);
GlobalActionMap.bind(keyboard, "f12", ShowDebugger);
GlobalActionMap.bind(keyboard, "f2", ToggleAdminMenu);
gameModeActionMap.bindCmd(keyboard, "n", "NetGraph::toggleNetGraph();", "");

