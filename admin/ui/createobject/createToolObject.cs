exec("./createToolObjectDlg.gui");

function createToolObjectDlg::onWake(%this)
{
    %this._(tierList).clear();
    %this._(tierList).add("Tier 0", 0);
    %this._(tierList).add("Tier 1", 1);
    %this._(tierList).add("Tier 2", 2);
    %this._(tierList).add("Tier 3", 3);
    %this._(tierList).add("Tier 4", 4);
    %this.onNew();
}

function createToolObjectDlg::onAcquireItem(%this)
{
    AC_AcquireItem("Tool", %this.curUpdateID);
}

function createToolObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %dispName = %this._(dispNameEditBox).getText();
    %desc = %this._(descEditBox).getText();
    %tier = %this._(tierList).getSelected();
    %shapeFile = %this._(shapeEditBox).getText();
    %textureFile = %this._(textureEditBox).getText();
    %CC = %this._(ccEditBox).getText();
    %PP = %this._(ppEditBox).getText();
    %holdArmThread = %this._(holdEditBox).getText();
    %fireArmThread = %this._(fireEditBox).getText();
    %shapeBaseImage = %this._(baseImageEditBox).getText();
    %sound = %this._(soundEditBox).getText();
    %particle = %this._(particleEditBox).getText();
    %thumbNail = %this._(thumbNailEditBox).getText();
    %thumbShapeFile = %this._(thumbshapeEditBox).getText();
    %actionList = "";
    %count = %this._(actionValueStack).getCount();

    for (%i=0; %i < %count; %i++)
    {
        %obj = %this._(actionValueStack).getObject(%i);
        if (%i > 0)
        {
            %actionList = %actionList @ "," @ %obj._(actionValue).getText();
        }
        else
        {
            %actionList = %obj._(actionValue).getText();
        }
    }

    if (%textureFile $= "")
    {
        %textureFile = "$NULL";
    }
    if (%particle $= "")
    {
        %particle = "$NULL";
    }
    if (%CC $= "")
    {
        %CC = 0;
    }
    if (%PP $= "")
    {
        %PP = 0;
    }

    if (((%name !$= "")) && ((%shapeFile !$= "")))
    {
        %data = "name" TAB %name NL "dispName" TAB %dispName NL "description" TAB %desc NL "tier" TAB %tier NL "shapeFile" TAB %shapeFile NL "textureFile" TAB %textureFile NL "holdArmThread" TAB %holdArmThread NL "fireArmThread" TAB %fireArmThread NL "action" TAB %actionList NL "shapeBaseImage" TAB %shapeBaseImage NL "sound" TAB %sound NL "particleID" TAB %particle NL "thumbnail" TAB %thumbNail NL "thumbshape" TAB %thumbShapeFile NL "CC" TAB %CC NL "PP" TAB %PP NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createToolObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("tool_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createToolObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createToolObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createToolObjectTree.getParent(%parentItem);
                    %value = createToolObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("tool_objects", 0, %parentID, %data);
        }
        createToolObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and shape");
    }
}

function createToolObjectDlg::onAddAction(%this)
{
    %item = new GuiControl() {
       Profile = "GuiBaseWindowProfile";
       Extent = "177 26";

       new GuiTextEditCtrl() {
          internalName = "actionValue";
          position = "5 4";
          Extent = "145 18";
       };

       new GuiBitmapButtonCtrl() {
          position = "155 5";
          Extent = "16 16";
          bitmap = "onverse/data/live_assets/engine/ui/images/inspector_delete";
          Command = "$ThisControl.getParent().delete();";
       };
    };

    %this._(actionValueStack).add(%item);
    return %item;
}

function createToolObjectDlg::onBrowseTextures(%this, %type)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetTexture", %this._(textureEditBox).getText());
}

function createToolObjectDlg::onSetTexture(%this, %texFile)
{
    %this._(textureEditBox).setText(%texFile);
}

function createToolObjectDlg::onBrowseShapes(%this, %type)
{
    getLoadFilename("*.dts", %this @ ".onSetShape", %this._(shapeEditBox).getText());
}

function createToolObjectDlg::onSetShape(%this, %shapeFile)
{
    %this._(shapeEditBox).setText(%shapeFile);
}

function createToolObjectDlg::onBrowseThumbShapes(%this, %type)
{
    getLoadFilename("*.dts", %this @ ".onSetThumbShape", %this._(thumbshapeEditBox).getText());
}

function createToolObjectDlg::onSetThumbShape(%this, %shapeFile)
{
    %this._(thumbshapeEditBox).setText(%shapeFile);
}

function createToolObjectDlg::onBrowseThumbnail(%this)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetThumbnail", %this._(thumbNailEditBox).getText());
}

function createToolObjectDlg::onSetThumbnail(%this, %filename)
{
    %this._(thumbNailEditBox).setText(%filename);
}

function createToolObjectDlg::onBrowseDesc(%this)
{
    getLoadFilename("*.txt", %this @ ".onSetDesc", %this._(descEditBox).getText());
}

function createToolObjectDlg::onSetDesc(%this, %text)
{
    %this._(descEditBox).setText(%text);
}

function createToolObjectDlg::onBrowseParticles(%this)
{
    loadDBItemParticle(%this @ ".onSetParticle", %this._(particleEditBox).getText());
}

function createToolObjectDlg::onSetParticle(%this, %idText)
{
    %this._(particleEditBox).setText(%idText);
}

function createToolObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createToolObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[tool_objects])
        {
            %this.receivedToolObjectData(%data);
        }
    }
}

function createToolObjectDlg::receivedToolObjectData(%this, %data)
{
    %i = -1;
    %type = 0;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "dispName")
                {
                    %this._(dispNameEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "description")
                    {
                        %this._(descEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "tier")
                        {
                            %this._(tierList).setSelected(getField(%record, 1));
                        }
                        else
                        {
                            if (getField(%record, 0) $= "shapeFile")
                            {
                                %this._(shapeEditBox).setText(getField(%record, 1));
                            }
                            else
                            {
                                if (getField(%record, 0) $= "textureFile")
                                {
                                    %this._(textureEditBox).setText(getField(%record, 1));
                                }
                                else
                                {
                                    if (getField(%record, 0) $= "CC")
                                    {
                                        %this._(ccEditBox).setText(getField(%record, 1));
                                    }
                                    else
                                    {
                                        if (getField(%record, 0) $= "PP")
                                        {
                                            %this._(ppEditBox).setText(getField(%record, 1));
                                        }
                                        else
                                        {
                                            if (getField(%record, 0) $= "thumbnail")
                                            {
                                                %this._(thumbNailEditBox).setText(getField(%record, 1));
                                            }
                                            else
                                            {
                                                if (getField(%record, 0) $= "thumbshape")
                                                {
                                                    %this._(thumbshapeEditBox).setText(getField(%record, 1));
                                                }
                                                else
                                                {
                                                    if (getField(%record, 0) $= "holdArmThread")
                                                    {
                                                        %this._(holdEditBox).setText(getField(%record, 1));
                                                    }
                                                    else
                                                    {
                                                        if (getField(%record, 0) $= "fireArmThread")
                                                        {
                                                            %this._(fireEditBox).setText(getField(%record, 1));
                                                        }
                                                        else
                                                        {
                                                            if (getField(%record, 0) $= "action")
                                                            {
                                                                %actionList = getField(%record, 1);
                                                            }
                                                            else
                                                            {
                                                                if (getField(%record, 0) $= "shapeBaseImage")
                                                                {
                                                                    %this._(baseImageEditBox).setText(getField(%record, 1));
                                                                }
                                                                else
                                                                {
                                                                    if (getField(%record, 0) $= "sound")
                                                                    {
                                                                        %this._(soundEditBox).setText(getField(%record, 1));
                                                                    }
                                                                    else
                                                                    {
                                                                        if (getField(%record, 0) $= "particleID")
                                                                        {
                                                                            %this._(particleEditBox).setText(getField(%record, 1));
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %this._(idEditBox).setText(%this.curUpdateID);
    %this._(actionValueStack).clear();

    %actionList = strreplace(%actionList, ",", "\t");
    for (%i=0; %i < getFieldCount(%actionList); %i++)
    {
        %action = %this.onAddAction();
        %action._(actionValue).setText(getField(%actionList, %i));
    }
    
    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
    %this._(AcquireButton).setVisible(1);
    %this._(nameEditBox).makeFirstResponder(1);
    storeFoldersDlg.insertObj = %this.curUpdateID;
    storeFoldersDlg.insertType = 3;
}

function createToolObjectDlg::onNew(%this)
{
    %this._(idEditBox).setText("");
    %this._(nameEditBox).setText("");
    %this._(dispNameEditBox).setText("");
    %this._(descEditBox).setText("");
    %this._(tierList).setSelected(0);
    %this._(shapeEditBox).setText("");
    %this._(textureEditBox).setText("");
    %this._(thumbNailEditBox).setText("");
    %this._(thumbshapeEditBox).setText("");
    %this._(ccEditBox).setText(0);
    %this._(ppEditBox).setText(0);
    %this._(holdEditBox).setText("");
    %this._(fireEditBox).setText("");
    %this._(baseImageEditBox).setText("MeleeToolImage");
    %this._(soundEditBox).setText("");
    %this._(particleEditBox).setText("");
    %this._(actionValueStack).clear();
    %this.curUpdateItem = 0;
    %this.curUpdateID = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
    %this._(AcquireButton).setVisible(0);
}

function createToolObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.addRootType($objects::tableID[tool_objects], "Tool Objects");
}

function createToolObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("tool_objects", %catID);
}

function createToolObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createToolObjectDlg.getResourceData("tool_objects", getField(%value, 2), %item);
    }
}
