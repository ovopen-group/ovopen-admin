exec("./createParticleObjectDlg.gui");

function clientCmdreturnResolveRealDatablock(%id, %this)
{
    %this.onReturnResolveRealDatablock(%id);
}

function createParticleObjectDlg::onWake(%this)
{
    %this._(MenuBar).clearMenus();
    %this._(MenuBar).addMenu("File", 1);
    %this._(MenuBar).addMenuItem("File", "New", 1, "Ctrl N");
    %this._(MenuBar).addMenuItem("File", "Open", 1, "Ctrl O");
    %this._(MenuBar).addMenuItem("File", "Save", 2, "Ctrl S");
    %this._(MenuBar).addMenuItem("File", "Save As New", 3, "");
    %this._(MenuBar).addMenuItem("File", "-", 0, "");
    %this._(MenuBar).addMenuItem("File", "Close", 4, "");
    %this._(openDialog).setVisible(0);

    datablock ParticleEmitterNodeData(AdminEditorParticleEmitterNodeData) {
        timeMultiple = 1;
    };

    %this.CurrentWorkingID = 0;
    %this.CurrentParticleEmitter = 0;
    %this.CurrentParticle = 0;
    %this._(particleInspector).inspect(%this.CurrentParticle);
    %this._(emitterInspector).inspect(%this.CurrentParticleEmitter);
}

function createParticleObjectDlg_Menu::onMenuItemSelect(%this, %unused, %menu, %itemId, %item)
{
    if (%menu $= "File")
    {
        if (%item $= "New")
        {
            createParticleObjectDlg.onNew();
        }
        else
        {
            if (%item $= "Open")
            {
                createParticleObjectDlg.showOpen();
            }
            else
            {
                if (%item $= "Save")
                {
                    createParticleObjectDlg.onSave();
                }
                else
                {
                    if (%item $= "Save As New")
                    {
                        if (createParticleObjectDlg.CurrentWorkingID > 0)
                        {
                            createParticleObjectDlg.CurrentWorkingID = -1;
                        }
                        createParticleObjectDlg.onSave();
                    }
                    else
                    {
                        if (%item $= "Close")
                        {
                            Canvas.popDialog(createParticleObjectDlg);
                        }
                    }
                }
            }
        }
    }
}

function createParticleObjectDlg::onSleep(%this)
{
    AdminEditorParticleEmitterNodeData.delete();
}

function createParticleObjectDlg::select(%this)
{
    %this._(ParticleView).setEmitter(%this.CurrentParticleEmitter);
    %this._(particleInspector).inspect(%this.CurrentParticle);
    %this._(emitterInspector).inspect(%this.CurrentParticleEmitter);
}

function createParticleObjectDlg::onNew(%this)
{
    if (isObject(TestParticleEmitter))
    {
        TestParticleEmitter.delete();
    }
    if (isObject(TestParticle))
    {
        TestParticle.delete();
    }

    %this.CurrentParticle = new ParticleData(TestParticle) {
    };

    %this.CurrentParticleEmitter = new ParticleEmitterData(TestParticleEmitter) {
        particles = "TestParticle";
        className = "NoName";
    };

    %this.select();
    %this._(openDialog).setVisible(0);
    %this.CurrentWorkingID = -1;
}

function createParticleObjectDlg::showOpen(%this)
{
    createParticleObjectTree.onRefreshParent(createParticleObjectTree.root);
    %this._(openDialog).setVisible(1);
    %this._(openDialog).selectWindow();
}

function createParticleObjectDlg::hideOpen(%this)
{
    %this._(openDialog).setVisible(0);
    %this._(ParticleWindow).selectWindow();
}

function createParticleObjectDlg::openSelection(%this)
{
    %selection = createParticleObjectTree.getSelectedItem();
    if (%selection != 0)
    {
        %value = createParticleObjectTree.getItemValue(%selection);
        if (!getField(%value, 0))
        {
            MasterServerConnection.requestResourceData(%this, "particle_objects", getField(%value, 2));
            %this.hideOpen();
        }
    }
}

function createParticleObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[particle_objects])
        {
            %this.receivedParticleObjectData(%data);
        }
    }
}

function createParticleObjectDlg::receivedParticleObjectData(%this, %data)
{
    if (isObject(TestParticleEmitter))
    {
        TestParticleEmitter.delete();
    }
    if (isObject(TestParticle))
    {
        TestParticle.delete();
    }

    %this.pendingData = %data;
    if (isObject(LocationGameConnection))
    {
        %this.getRealDatablock(%data);
    }
    else
    {
        %this.onReturnResolveRealDatablock(0);
    }
}

function createParticleObjectDlg::getRealDatablock(%this, %data)
{
    %i = -1;
    %id = 0;
    while ((%id == 0) && ((%record = getRecord(%data, %i++)) !$= "") )
    {
        %t = getField(%record, 0);
        if (%t $= "id")
        {
            %id = getField(%record, 1);
        }
    }

    if (%id == 0)
    {
        warn("Could not find datablock id in data.");
        %this.onReturnResolveRealDatablock(0);
        return;
    }

    commandToServer('ResolveRealDatablock', "particle_objects", %id, %this);
}

function createParticleObjectDlg::onReturnResolveRealDatablock(%this, %id)
{
    if (%id == 0)
    {
        %this.CurrentParticle = new ParticleData(TestParticle) {
        };

        %this.CurrentParticleEmitter = new ParticleEmitterData(TestParticleEmitter) {
            particles = "TestParticle";
        };
    }
    else
    {
        %this.CurrentParticleEmitter = %id;
        %this.CurrentParticle = %this.CurrentParticleEmitter.getParticles().getId();
    }

    %this.updateToLatestData(%this.pendingData);
    %this.pendingData = "";
}

function createParticleObjectDlg::updateToLatestData(%this, %data)
{
    %i = -1;
    %this.CurrentWorkingID = -2;
    %baseDir = "";
    %textureList = "";
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        %t = getField(%record, 0);
        if (%t $= "id")
        {
            %this.CurrentWorkingID = getField(%record, 1);
        }
        else
        {
            if (%t $= "name")
            {
                %this.CurrentParticleEmitter.className = getField(%record, 1);
            }
            else
            {
                if (%t $= "ejectionPeriodMS")
                {
                    %this.CurrentParticleEmitter.ejectionPeriodMS = getField(%record, 1);
                }
                else
                {
                    if (%t $= "periodVarianceMS")
                    {
                        %this.CurrentParticleEmitter.periodVarianceMS = getField(%record, 1);
                    }
                    else
                    {
                        if (%t $= "ejectionVelocity")
                        {
                            %this.CurrentParticleEmitter.ejectionVelocity = getField(%record, 1);
                        }
                        else
                        {
                            if (%t $= "velocityVariance")
                            {
                                %this.CurrentParticleEmitter.velocityVariance = getField(%record, 1);
                            }
                            else
                            {
                                if (%t $= "ejectionOffset")
                                {
                                    %this.CurrentParticleEmitter.ejectionOffset = getField(%record, 1);
                                }
                                else
                                {
                                    if (%t $= "thetaMin")
                                    {
                                        %this.CurrentParticleEmitter.thetaMin = getField(%record, 1);
                                    }
                                    else
                                    {
                                        if (%t $= "thetaMax")
                                        {
                                            %this.CurrentParticleEmitter.thetaMax = getField(%record, 1);
                                        }
                                        else
                                        {
                                            if (%t $= "phiReferenceVel")
                                            {
                                                %this.CurrentParticleEmitter.phiReferenceVel = getField(%record, 1);
                                            }
                                            else
                                            {
                                                if (%t $= "phiVariance")
                                                {
                                                    %this.CurrentParticleEmitter.phiVariance = getField(%record, 1);
                                                }
                                                else
                                                {
                                                    if (%t $= "overrideAdvance")
                                                    {
                                                        %this.CurrentParticleEmitter.overrideAdvance = getField(%record, 1);
                                                    }
                                                    else
                                                    {
                                                        if (%t $= "orientParticles")
                                                        {
                                                            %this.CurrentParticleEmitter.orientParticles = getField(%record, 1);
                                                        }
                                                        else
                                                        {
                                                            if (%t $= "orientOnVelocity")
                                                            {
                                                                %this.CurrentParticleEmitter.orientOnVelocity = getField(%record, 1);
                                                            }
                                                            else
                                                            {
                                                                if (%t $= "lifetimeMS")
                                                                {
                                                                    %this.CurrentParticleEmitter.lifetimeMS = getField(%record, 1);
                                                                }
                                                                else
                                                                {
                                                                    if (%t $= "lifetimeVarianceMS")
                                                                    {
                                                                        %this.CurrentParticleEmitter.lifetimeVarianceMS = getField(%record, 1);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (%t $= "useEmitterSizes")
                                                                        {
                                                                            %this.CurrentParticleEmitter.useEmitterSizes = getField(%record, 1);
                                                                        }
                                                                        else
                                                                        {
                                                                            if (%t $= "useEmitterColors")
                                                                            {
                                                                                %this.CurrentParticleEmitter.useEmitterColors = getField(%record, 1);
                                                                            }
                                                                            else
                                                                            {
                                                                                if (%t $= "dragCoefficient")
                                                                                {
                                                                                    %this.CurrentParticle.dragCoefficient = getField(%record, 1);
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (%t $= "windCoefficient")
                                                                                    {
                                                                                        %this.CurrentParticle.windCoefficient = getField(%record, 1);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (%t $= "gravityCoefficient")
                                                                                        {
                                                                                            %this.CurrentParticle.gravityCoefficient = getField(%record, 1);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (%t $= "inheritedVelFactor")
                                                                                            {
                                                                                                %this.CurrentParticle.inheritedVelFactor = getField(%record, 1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (%t $= "constantAcceleration")
                                                                                                {
                                                                                                    %this.CurrentParticle.constantAcceleration = getField(%record, 1);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (%t $= "plifetimeMS")
                                                                                                    {
                                                                                                        %this.CurrentParticle.lifetimeMS = getField(%record, 1);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        if (%t $= "plifetimeVarianceMS")
                                                                                                        {
                                                                                                            %this.CurrentParticle.lifetimeVarianceMS = getField(%record, 1);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if (%t $= "spinSpeed")
                                                                                                            {
                                                                                                                %this.CurrentParticle.spinSpeed = getField(%record, 1);
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                if (%t $= "spinRandomMin")
                                                                                                                {
                                                                                                                    %this.CurrentParticle.spinRandomMin = getField(%record, 1);
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    if (%t $= "spinRandomMax")
                                                                                                                    {
                                                                                                                        %this.CurrentParticle.spinRandomMax = getField(%record, 1);
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        if (%t $= "useInvAlpha")
                                                                                                                        {
                                                                                                                            %this.CurrentParticle.useInvAlpha = getField(%record, 1);
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            if (%t $= "animateTexture")
                                                                                                                            {
                                                                                                                                %this.CurrentParticle.animateTexture = getField(%record, 1);
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                if (%t $= "framesPerSec")
                                                                                                                                {
                                                                                                                                    %this.CurrentParticle.framesPerSec = getField(%record, 1);
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    if (%t $= "allowLighting")
                                                                                                                                    {
                                                                                                                                        %this.CurrentParticle.allowLighting = getField(%record, 1);
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        if (%t $= "baseAnimTexDir")
                                                                                                                                        {
                                                                                                                                            %baseDir = getField(%record, 1);
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                            if (%t $= "animTextures")
                                                                                                                                            {
                                                                                                                                                %textureList = getFields(%record, 1);
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                            {
                                                                                                                                                if (%t $= "colors")
                                                                                                                                                {
                                                                                                                                                    %this.CurrentParticle.Colors[0] = getField(%record, 1);
                                                                                                                                                    %this.CurrentParticle.Colors[1] = getField(%record, 2);
                                                                                                                                                    %this.CurrentParticle.Colors[2] = getField(%record, 3);
                                                                                                                                                    %this.CurrentParticle.Colors[3] = getField(%record, 4);
                                                                                                                                                }
                                                                                                                                                else
                                                                                                                                                {
                                                                                                                                                    if (%t $= "sizes")
                                                                                                                                                    {
                                                                                                                                                        %this.CurrentParticle.sizes[0] = getField(%record, 1);
                                                                                                                                                        %this.CurrentParticle.sizes[1] = getField(%record, 2);
                                                                                                                                                        %this.CurrentParticle.sizes[2] = getField(%record, 3);
                                                                                                                                                        %this.CurrentParticle.sizes[3] = getField(%record, 4);
                                                                                                                                                    }
                                                                                                                                                    else
                                                                                                                                                    {
                                                                                                                                                        if (%t $= "times")
                                                                                                                                                        {
                                                                                                                                                            %this.CurrentParticle.times[0] = getField(%record, 1);
                                                                                                                                                            %this.CurrentParticle.times[1] = getField(%record, 2);
                                                                                                                                                            %this.CurrentParticle.times[2] = getField(%record, 3);
                                                                                                                                                            %this.CurrentParticle.times[3] = getField(%record, 4);
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    %i = -1;
    while ((%field = getField(%textureList, %i++)) !$= "")
    {
        %this.CurrentParticle.animTexName[%i] = %baseDir @ %field;
    }
    %this.select();
    %this._(openDialog).setVisible(0);
}

function createParticleObjectDlg::onSave(%this, %conf)
{
    if ((%this.CurrentWorkingID == 0) || (%this.CurrentWorkingID < -1))
    {
        MessageBoxOK("Error", "Invalid resource id");
        return;
    }
    if (!isDatablock(%this.CurrentParticle) || !isDatablock(%this.CurrentParticleEmitter))
    {
        MessageBoxOK("Error", "Could not find required datablocks");
        return;
    }
    if (!%conf && (%this.CurrentWorkingID == -1))
    {
        MessageBoxYesNo("Question", "This will insert a new particle object, Are you sure that is what you want?", "createParticleObjectDlg.onSave(true);", "");
        return;
    }

    %numTextures = 0;
    for (%i=0; %i < 50; %i++)
    {
        if (!(%this.CurrentParticle.animTexName[%i] $= ""))
        {
            %texture[%numTextures] = %this.CurrentParticle.animTexName[%i];
            %numTextures++;
        }
    }

    %baseDir = strrchrrev(%texture[0], "/");
    %textureList = "";
    for (%i=0; %i < %numTextures; %i++)
    {
        %base = strrchrrev(%texture[%i], "/");
        if (!(%base $= %baseDir))
        {
            MessageBoxOK("Error", "All particle textures must be in the same directory to save.");
            return;
        }
        %textureList = %textureList @ getSubStr(%texture[%i], strlen(%base), strlen(%texture[%i]) - strlen(%base)) @ "\t";
    }

    %data = "name" TAB %this.CurrentParticleEmitter.className;
    %data = %data NL "ejectionPeriodMS" TAB %this.CurrentParticleEmitter.ejectionPeriodMS;
    %data = %data NL "periodVarianceMS" TAB %this.CurrentParticleEmitter.periodVarianceMS;
    %data = %data NL "ejectionVelocity" TAB %this.CurrentParticleEmitter.ejectionVelocity;
    %data = %data NL "velocityVariance" TAB %this.CurrentParticleEmitter.velocityVariance;
    %data = %data NL "ejectionOffset" TAB %this.CurrentParticleEmitter.ejectionOffset;
    %data = %data NL "thetaMin" TAB %this.CurrentParticleEmitter.thetaMin;
    %data = %data NL "thetaMax" TAB %this.CurrentParticleEmitter.thetaMax;
    %data = %data NL "phiReferenceVel" TAB %this.CurrentParticleEmitter.phiReferenceVel;
    %data = %data NL "phiVariance" TAB %this.CurrentParticleEmitter.phiVariance;
    %data = %data NL "overrideAdvance" TAB %this.CurrentParticleEmitter.overrideAdvance;
    %data = %data NL "orientParticles" TAB %this.CurrentParticleEmitter.orientParticles;
    %data = %data NL "orientOnVelocity" TAB %this.CurrentParticleEmitter.orientOnVelocity;
    %data = %data NL "lifetimeMS" TAB %this.CurrentParticleEmitter.lifetimeMS;
    %data = %data NL "lifetimeVarianceMS" TAB %this.CurrentParticleEmitter.lifetimeVarianceMS;
    %data = %data NL "useEmitterSizes" TAB %this.CurrentParticleEmitter.useEmitterSizes;
    %data = %data NL "useEmitterColors" TAB %this.CurrentParticleEmitter.useEmitterColors;
    %data = %data NL "dragCoefficient" TAB %this.CurrentParticle.dragCoefficient;
    %data = %data NL "windCoefficient" TAB %this.CurrentParticle.windCoefficient;
    %data = %data NL "gravityCoefficient" TAB %this.CurrentParticle.gravityCoefficient;
    %data = %data NL "inheritedVelFactor" TAB %this.CurrentParticle.inheritedVelFactor;
    %data = %data NL "constantAcceleration" TAB %this.CurrentParticle.constantAcceleration;
    %data = %data NL "plifetimeMS" TAB %this.CurrentParticle.lifetimeMS;
    %data = %data NL "plifetimeVarianceMS" TAB %this.CurrentParticle.lifetimeVarianceMS;
    %data = %data NL "spinSpeed" TAB %this.CurrentParticle.spinSpeed;
    %data = %data NL "spinRandomMin" TAB %this.CurrentParticle.spinRandomMin;
    %data = %data NL "spinRandomMax" TAB %this.CurrentParticle.spinRandomMax;
    %data = %data NL "useInvAlpha" TAB %this.CurrentParticle.useInvAlpha;
    %data = %data NL "animateTexture" TAB %this.CurrentParticle.animateTexture;
    %data = %data NL "framesPerSec" TAB %this.CurrentParticle.framesPerSec;
    %data = %data NL "allowLighting" TAB %this.CurrentParticle.allowLighting;
    %data = %data NL "baseAnimTexDir" TAB %baseDir;
    %data = %data NL "animTextures" TAB %textureList;
    %data = %data NL "colors" @ "\t";

    for (%i=0; %i < 4; %i++)
    {
        %data = %data @ %this.CurrentParticle.Colors[%i] @ "\t";
    }
    %data = %data NL "sizes" @ "\t";

    for (%i=0; %i < 4; %i++)
    {
        %data = %data @ %this.CurrentParticle.sizes[%i] @ "\t";
    }
    %data = %data NL "times" @ "\t";

    for (%i=0; %i < 4; %i++)
    {
        %data = %data @ %this.CurrentParticle.times[%i] @ "\t";
    }

    if (%this.CurrentWorkingID != -1)
    {
        MasterServerConnection.requestUpdateResource("particle_objects", %this.CurrentWorkingID, 0, %data);
    }
    else
    {
        MasterServerConnection.requestUpdateResource("particle_objects", 0, 0, %data);
    }
}

function createParticleObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.root = %this.addRootType($objects::tableID[particle_objects], "Particle Objects");
}

function createParticleObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("particle_objects", %catID);
}
