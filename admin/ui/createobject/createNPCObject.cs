exec("./createNPCObjectDlg.gui");

function createNPCObjectDlg::onWake(%this)
{
    %this.onNew();
}

function createNPCObjectDlg::initalizeClassNames(%this)
{
    %this._(classNameList).clear();
    %this._(classNameList).add("BaseAIPlayerNPC", 0);
    %this._(classNameList).add("MaleAIPlayerNPC", 1);
    %this._(classNameList).add("FemaleAIPlayerNPC", 2);
    %this._(brainNameList).clear();
    %this._(brainNameList).add("NPCBrain_Static", 0);
    %this._(brainNameList).add("NPCBrain_StaticDialog", 1);
}

function createNPCObjectDlg::buildNvPairsString(%this, %base)
{
    %stack = %this._(%base @ "ValueStack");
    %string = "";
    %count = %stack.getCount();
    for (%i=0; %i < %count; %i++)
    {
        %obj = %stack.getObject(%i);
        %name = %obj._("valueName").getText();
        %value = %obj._("valueValue").getText();
        %name = strreplace(strreplace(%name, ",", ""), " ", "");
        %value = strreplace(%value, ",", "");
        if (%name $= "")
        {
            continue;
        }
        %string = %string @ %name TAB %value NL "";
    }
    return %string;
}

function createNPCObjectDlg::decodeNvPairsString(%this, %base, %str)
{
    %stack = %this._(%base @ "ValueStack");
    while (%stack.getCount())
    {
        %stack.getObject(0).delete();
    }
    %count = getRecordCount(%str);
    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%str, %i);
        %name = getField(%record, 0);
        %value = getField(%record, 1);
        %obj = %this.onAddValue(%base);
        %obj._("valueName").setText(%name);
        %obj._("valueValue").setText(%value);
    }
}

function createNPCObjectDlg::onCopy(%this)
{
    %this.isUpdate = 0;
    %this.curUpdateID = 0;
    %this._(idEditBox).setText("");
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
}

function createNPCObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %shapeFile = %this._(shapeEditBox).getText();
    %textureFile = %this._(textureEditBox).getText();
    %className = %this._(classNameList).getText();
    %brainName = %this._(brainNameList).getText();
    %idles = %this._(idlesEditBox).getText();
    %nvPairs = %this.buildNvPairsString(data);
    %nvPairs = strreplace(strreplace(%nvPairs, "\t", "="), "\n", ",");

    if (%textureFile $= "")
    {
        %textureFile = "$NULL";
    }
    if (%nvPairs $= "")
    {
        %nvPairs = "$NULL";
    }
    if (%shapeFile $= "")
    {
        %shapeFile = "$NULL";
    }

    if (((%name !$= "")) && ((%className !$= "")))
    {
        %data = "name" TAB %name NL "shapeFile" TAB %shapeFile NL "textureFile" TAB %textureFile NL "className" TAB %className NL "brainName" TAB %brainName NL "nvPairs" TAB %nvPairs NL "idles" TAB %idles NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createNPCObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("npc_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createNPCObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createNPCObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createNPCObjectTree.getParent(%parentItem);
                    %value = createNPCObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("npc_objects", 0, %parentID, %data);
        }
        createNPCObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and classname");
    }
}

function createNPCObjectDlg::onAddValue(%this, %stackName)
{
    %item = new GuiControl() {
       Profile = "GuiBaseWindowProfile";
       Extent = "188 26";

       new GuiTextEditCtrl() {
          internalName = "valueValue";
          position = "99 4";
          Extent = "65 18";
       };

       new GuiTextEditCtrl() {
          internalName = "valueName";
          position = "5 4";
          Extent = "89 18";
       };

       new GuiBitmapButtonCtrl() {
          position = "168 5";
          Extent = "16 16";
          bitmap = "onverse/data/live_assets/engine/ui/images/inspector_delete";
          Command = "$ThisControl.getParent().delete();";
       };
    };
    %this._(%stackName @ "ValueStack").add(%item);
    return %item;
}

function createNPCObjectDlg::onBrowseTextures(%this, %type)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetTexture", %this._(textureEditBox).getText());
}

function createNPCObjectDlg::onSetTexture(%this, %texFile)
{
    %this._(textureEditBox).setText(%texFile);
}

function createNPCObjectDlg::onBrowseShapes(%this, %type)
{
    getLoadFilename("*.dts", %this @ ".onSetShape", %this._(shapeEditBox).getText());
}

function createNPCObjectDlg::onSetShape(%this, %shapeFile)
{
    %this._(shapeEditBox).setText(%shapeFile);
}

function createNPCObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createNPCObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[npc_objects])
        {
            %this.receivedNPCObjectData(%data);
        }
    }
}

function createNPCObjectDlg::receivedNPCObjectData(%this, %data)
{
    %i = -1;
    %type = 0;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "shapeFile")
                {
                    %this._(shapeEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "textureFile")
                    {
                        %this._(textureEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "className")
                        {
                            %className = getField(%record, 1);
                        }
                        else
                        {
                            if (getField(%record, 0) $= "brainName")
                            {
                                %brainName = getField(%record, 1);
                            }
                            else
                            {
                                if (getField(%record, 0) $= "idles")
                                {
                                    %this._(idlesEditBox).setText(getField(%record, 1));
                                }
                                else
                                {
                                    if (getField(%record, 0) $= "nvPairs")
                                    {
                                        %nvPairs = getField(%record, 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %nvPairs = strreplace(strreplace(%nvPairs, "=", "\t"), ",", "\n");
    %this.decodeNvPairsString("data", %nvPairs);
    %this.initalizeClassNames();
    %classNameID = %this._(classNameList).findText(%className);
    if (%classNameID == -1)
    {
        %this._(classNameList).add(%className, 9999);
        %this._(classNameList).setSelected(9999);
    }
    else
    {
        %this._(classNameList).setSelected(%classNameID);
    }
    %brainNameID = %this._(brainNameList).findText(%brainName);
    if (%brainNameID == -1)
    {
        %this._(brainNameList).add(%brainName, 9999);
        %this._(brainNameList).setSelected(9999);
    }
    else
    {
        %this._(brainNameList).setSelected(%brainNameID);
    }
    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
    %this._(copyButton).setVisible(1);
    %this._(idEditBox).setText(%this.curUpdateID);
}

function createNPCObjectDlg::onNew(%this)
{
    %this.initalizeClassNames();
    %this._(idEditBox).setText("");
    %this._(nameEditBox).setText("");
    %this._(shapeEditBox).setText("");
    %this._(textureEditBox).setText("");
    %this._(classNameList).setSelected(0);
    %this._(brainNameList).setSelected(0);
    %this.curUpdateID = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
    while (%this._(dataValueStack).getCount())
    {
        %this._(dataValueStack).getObject(0).delete();
    }
}

function createNPCObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.curUpdateItem = 0;
    %this.addRootType($objects::tableID[npc_objects], "NPC Objects");
}

function createNPCObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("npc_objects", %catID);
}

function createNPCObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createNPCObjectDlg.getResourceData("npc_objects", getField(%value, 2), %item);
    }
}
