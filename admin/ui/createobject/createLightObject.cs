exec("./createLightObjectDlg.gui");

function createLightObjectDlg::onWake(%this)
{
    %this._(MenuBar).clearMenus();
    %this._(MenuBar).addMenu("File", 1);
    %this._(MenuBar).addMenuItem("File", "New", 1, "Ctrl N");
    %this._(MenuBar).addMenuItem("File", "Open", 1, "Ctrl O");
    %this._(MenuBar).addMenuItem("File", "Save", 2, "Ctrl S");
    %this._(MenuBar).addMenuItem("File", "Save As New", 3, "");
    %this._(MenuBar).addMenuItem("File", "-", 0, "");
    %this._(MenuBar).addMenuItem("File", "Close", 4, "");
    %this._(openDialog).setVisible(0);
    %this.CurrentWorkingID = 0;
    %this.CurrentLight = 0;
    %this._(lightInspector).inspect(%this.CurrentLight);
}

function createLightObjectDlg_Menu::onMenuItemSelect(%this, %unused, %menu, %itemId, %item)
{
    if (%menu $= "File")
    {
        if (%item $= "New")
        {
            createLightObjectDlg.onNew();
        }
        else
        {
            if (%item $= "Open")
            {
                createLightObjectDlg.showOpen();
            }
            else
            {
                if (%item $= "Save")
                {
                    createLightObjectDlg.onSave();
                }
                else
                {
                    if (%item $= "Save As New")
                    {
                        if (createLightObjectDlg.CurrentWorkingID > 0)
                        {
                            createLightObjectDlg.CurrentWorkingID = -1;
                        }
                        createLightObjectDlg.onSave();
                    }
                    else
                    {
                        if (%item $= "Close")
                        {
                            Canvas.popDialog(createLightObjectDlg);
                        }
                    }
                }
            }
        }
    }
}

function createLightObjectDlg::onSleep(%this)
{
}

function createLightObjectDlg::select(%this)
{
    %this._(lightInspector).inspect(%this.CurrentLight);
}

function createLightObjectDlg::onNew(%this)
{
    if (isObject(TestLight))
    {
        TestLight.delete();
    }
    %this.CurrentLight = new sgLightObjectData(TestLight)
    {
        className = "NoName";
    };
    %this.select();
    %this._(openDialog).setVisible(0);
    %this.CurrentWorkingID = -1;
}

function createLightObjectDlg::showOpen(%this)
{
    createLightObjectTree.onRefreshParent(createLightObjectTree.root);
    %this._(openDialog).setVisible(1);
    %this._(openDialog).selectWindow();
}

function createLightObjectDlg::hideOpen(%this)
{
    %this._(openDialog).setVisible(0);
    %this._(LightWindow).selectWindow();
}

function createLightObjectDlg::openSelection(%this)
{
    %selection = createLightObjectTree.getSelectedItem();
    if (%selection != 0)
    {
        %value = createLightObjectTree.getItemValue(%selection);
        if (!getField(%value, 0))
        {
            MasterServerConnection.requestResourceData(%this, "light_objects", getField(%value, 2));
            %this.hideOpen();
        }
    }
}

function createLightObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[light_objects])
        {
            %this.receivedLightObjectData(%data);
        }
    }
}

function createLightObjectDlg::receivedLightObjectData(%this, %data)
{
    if (isObject(TestLight))
    {
        TestLight.delete();
    }
    %this.pendingData = %data;
    if (isObject(LocationGameConnection))
    {
        %this.getRealDatablock(%data);
    }
    else
    {
        %this.onReturnResolveRealDatablock(0);
    }
}

function createLightObjectDlg::getRealDatablock(%this, %data)
{
    %i = -1;
    %id = 0;
    while ((%id == 0) && ((%record = getRecord(%data, %i++)) !$= ""))
    {
        %t = getField(%record, 0);
        if (%t $= "id")
        {
            %id = getField(%record, 1);
        }
    }
    if (%id == 0)
    {
        warn("Could not find datablock id in data.");
        %this.onReturnResolveRealDatablock(0);
        return;
    }
    commandToServer('ResolveRealDatablock', "light_objects", %id, %this);
}

function createLightObjectDlg::onReturnResolveRealDatablock(%this, %id)
{
    if (%id == 0)
    {
        %this.CurrentLight = new sgLightObjectData(TestLight) {
            className = "NoName";
        };
    }
    else
    {
        %this.CurrentLight = %id;
    }
    %this.updateToLatestData(%this.pendingData);
    %this.pendingData = "";
}

function createLightObjectDlg::updateToLatestData(%this, %data)
{
    %i = -1;
    %this.CurrentWorkingID = -2;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        %t = getField(%record, 0);
        if (%t $= "id")
        {
            %this.CurrentWorkingID = getField(%record, 1);
        }
        else
        {
            if (%t $= "name")
            {
                %this.CurrentLight.className = getField(%record, 1);
            }
            else
            {
                if (%t $= "staticLight")
                {
                    %this.CurrentLight.StaticLight = getField(%record, 1);
                }
                else
                {
                    if (%t $= "spotLight")
                    {
                        %this.CurrentLight.SpotLight = getField(%record, 1);
                    }
                    else
                    {
                        if (%t $= "spotAngle")
                        {
                            %this.CurrentLight.SpotAngle = getField(%record, 1);
                        }
                        else
                        {
                            if (%t $= "advancedLighting")
                            {
                                %this.CurrentLight.AdvancedLightingModel = getField(%record, 1);
                            }
                            else
                            {
                                if (%t $= "affectDTS")
                                {
                                    %this.CurrentLight.EffectsDTSObjects = getField(%record, 1);
                                }
                                else
                                {
                                    if (%t $= "castShadows")
                                    {
                                        %this.CurrentLight.CastsShadows = getField(%record, 1);
                                    }
                                    else
                                    {
                                        if (%t $= "diffuseZone")
                                        {
                                            %this.CurrentLight.DiffuseRestrictZone = getField(%record, 1);
                                        }
                                        else
                                        {
                                            if (%t $= "ambientZone")
                                            {
                                                %this.CurrentLight.AmbientRestrictZone = getField(%record, 1);
                                            }
                                            else
                                            {
                                                if (%t $= "localAmbAmnt")
                                                {
                                                    %this.CurrentLight.LocalAmbientAmount = getField(%record, 1);
                                                }
                                                else
                                                {
                                                    if (%t $= "smoothSpot")
                                                    {
                                                        %this.CurrentLight.SmoothSpotLight = getField(%record, 1);
                                                    }
                                                    else
                                                    {
                                                        if (%t $= "dblSidedAmb")
                                                        {
                                                            %this.CurrentLight.DoubleSidedAmbient = getField(%record, 1);
                                                        }
                                                        else
                                                        {
                                                            if (%t $= "useNormals")
                                                            {
                                                                %this.CurrentLight.UseNormals = getField(%record, 1);
                                                            }
                                                            else
                                                            {
                                                                if (%t $= "mountPoint")
                                                                {
                                                                    %this.CurrentLight.MountPoint = getField(%record, 1);
                                                                }
                                                                else
                                                                {
                                                                    if (%t $= "mountPosition")
                                                                    {
                                                                        %this.CurrentLight.MountPosition = getField(%record, 1);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (%t $= "mountRotation")
                                                                        {
                                                                            %this.CurrentLight.MountRotation = getField(%record, 1);
                                                                        }
                                                                        else
                                                                        {
                                                                            if (%t $= "lightOn")
                                                                            {
                                                                                %this.CurrentLight.LightOn = getField(%record, 1);
                                                                            }
                                                                            else
                                                                            {
                                                                                if (%t $= "radius")
                                                                                {
                                                                                    %this.CurrentLight.Radius = getField(%record, 1);
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (%t $= "brightness")
                                                                                    {
                                                                                        %this.CurrentLight.Brightness = getField(%record, 1);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (%t $= "color")
                                                                                        {
                                                                                            %this.CurrentLight.Colour = getField(%record, 1);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (%t $= "flareOn")
                                                                                            {
                                                                                                %this.CurrentLight.FlareOn = getField(%record, 1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (%t $= "flareTp")
                                                                                                {
                                                                                                    %this.CurrentLight.FlareTP = getField(%record, 1);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (%t $= "flareBitmap")
                                                                                                    {
                                                                                                        %this.CurrentLight.FlareBitmap = getField(%record, 1);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        if (%t $= "flareColor")
                                                                                                        {
                                                                                                            %this.CurrentLight.FlareColour = getField(%record, 1);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if (%t $= "constantSizeOn")
                                                                                                            {
                                                                                                                %this.CurrentLight.ConstantSizeOn = getField(%record, 1);
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                if (%t $= "constantSize")
                                                                                                                {
                                                                                                                    %this.CurrentLight.ConstantSize = getField(%record, 1);
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    if (%t $= "nearSize")
                                                                                                                    {
                                                                                                                        %this.CurrentLight.NearSize = getField(%record, 1);
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        if (%t $= "farSize")
                                                                                                                        {
                                                                                                                            %this.CurrentLight.FarSize = getField(%record, 1);
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            if (%t $= "nearDistance")
                                                                                                                            {
                                                                                                                                %this.CurrentLight.NearDistance = getField(%record, 1);
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                if (%t $= "farDisatance")
                                                                                                                                {
                                                                                                                                    %this.CurrentLight.FarDistance = getField(%record, 1);
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    if (%t $= "fadeTime")
                                                                                                                                    {
                                                                                                                                        %this.CurrentLight.FadeTime = getField(%record, 1);
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        if (%t $= "blendMode")
                                                                                                                                        {
                                                                                                                                            %this.CurrentLight.BlendMode = getField(%record, 1);
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                            if (%t $= "animColor")
                                                                                                                                            {
                                                                                                                                                %this.CurrentLight.AnimColour = getField(%record, 1);
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                            {
                                                                                                                                                if (%t $= "animBrightness")
                                                                                                                                                {
                                                                                                                                                    %this.CurrentLight.AnimBrightness = getField(%record, 1);
                                                                                                                                                }
                                                                                                                                                else
                                                                                                                                                {
                                                                                                                                                    if (%t $= "animRadius")
                                                                                                                                                    {
                                                                                                                                                        %this.CurrentLight.AnimRadius = getField(%record, 1);
                                                                                                                                                    }
                                                                                                                                                    else
                                                                                                                                                    {
                                                                                                                                                        if (%t $= "animOffsets")
                                                                                                                                                        {
                                                                                                                                                            %this.CurrentLight.AnimOffsets = getField(%record, 1);
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            if (%t $= "animRotation")
                                                                                                                                                            {
                                                                                                                                                                %this.CurrentLight.AnimRotation = getField(%record, 1);
                                                                                                                                                            }
                                                                                                                                                            else
                                                                                                                                                            {
                                                                                                                                                                if (%t $= "linkFlare")
                                                                                                                                                                {
                                                                                                                                                                    %this.CurrentLight.LinkFlare = getField(%record, 1);
                                                                                                                                                                }
                                                                                                                                                                else
                                                                                                                                                                {
                                                                                                                                                                    if (%t $= "linkFlareSize")
                                                                                                                                                                    {
                                                                                                                                                                        %this.CurrentLight.LinkFlareSize = getField(%record, 1);
                                                                                                                                                                    }
                                                                                                                                                                    else
                                                                                                                                                                    {
                                                                                                                                                                        if (%t $= "minColor")
                                                                                                                                                                        {
                                                                                                                                                                            %this.CurrentLight.MinColour = getField(%record, 1);
                                                                                                                                                                        }
                                                                                                                                                                        else
                                                                                                                                                                        {
                                                                                                                                                                            if (%t $= "maxColor")
                                                                                                                                                                            {
                                                                                                                                                                                %this.CurrentLight.MaxColour = getField(%record, 1);
                                                                                                                                                                            }
                                                                                                                                                                            else
                                                                                                                                                                            {
                                                                                                                                                                                if (%t $= "minBrightness")
                                                                                                                                                                                {
                                                                                                                                                                                    %this.CurrentLight.MinBrightness = getField(%record, 1);
                                                                                                                                                                                }
                                                                                                                                                                                else
                                                                                                                                                                                {
                                                                                                                                                                                    if (%t $= "maxBrightness")
                                                                                                                                                                                    {
                                                                                                                                                                                        %this.CurrentLight.MaxBrightness = getField(%record, 1);
                                                                                                                                                                                    }
                                                                                                                                                                                    else
                                                                                                                                                                                    {
                                                                                                                                                                                        if (%t $= "minRadius")
                                                                                                                                                                                        {
                                                                                                                                                                                            %this.CurrentLight.MinRadius = getField(%record, 1);
                                                                                                                                                                                        }
                                                                                                                                                                                        else
                                                                                                                                                                                        {
                                                                                                                                                                                            if (%t $= "maxRadius")
                                                                                                                                                                                            {
                                                                                                                                                                                                %this.CurrentLight.MaxRadius = getField(%record, 1);
                                                                                                                                                                                            }
                                                                                                                                                                                            else
                                                                                                                                                                                            {
                                                                                                                                                                                                if (%t $= "startOffset")
                                                                                                                                                                                                {
                                                                                                                                                                                                    %this.CurrentLight.StartOffset = getField(%record, 1);
                                                                                                                                                                                                }
                                                                                                                                                                                                else
                                                                                                                                                                                                {
                                                                                                                                                                                                    if (%t $= "endOffset")
                                                                                                                                                                                                    {
                                                                                                                                                                                                        %this.CurrentLight.EndOffset = getField(%record, 1);
                                                                                                                                                                                                    }
                                                                                                                                                                                                    else
                                                                                                                                                                                                    {
                                                                                                                                                                                                        if (%t $= "minRotation")
                                                                                                                                                                                                        {
                                                                                                                                                                                                            %this.CurrentLight.MinRotation = getField(%record, 1);
                                                                                                                                                                                                        }
                                                                                                                                                                                                        else
                                                                                                                                                                                                        {
                                                                                                                                                                                                            if (%t $= "maxRotation")
                                                                                                                                                                                                            {
                                                                                                                                                                                                                %this.CurrentLight.MaxRotation = getField(%record, 1);
                                                                                                                                                                                                            }
                                                                                                                                                                                                            else
                                                                                                                                                                                                            {
                                                                                                                                                                                                                if (%t $= "singleColorKeys")
                                                                                                                                                                                                                {
                                                                                                                                                                                                                    %this.CurrentLight.SingleColourKeys = getField(%record, 1);
                                                                                                                                                                                                                }
                                                                                                                                                                                                                else
                                                                                                                                                                                                                {
                                                                                                                                                                                                                    if (%t $= "redKeys")
                                                                                                                                                                                                                    {
                                                                                                                                                                                                                        %this.CurrentLight.RedKeys = getField(%record, 1);
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    else
                                                                                                                                                                                                                    {
                                                                                                                                                                                                                        if (%t $= "greenKeys")
                                                                                                                                                                                                                        {
                                                                                                                                                                                                                            %this.CurrentLight.GreenKeys = getField(%record, 1);
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                        else
                                                                                                                                                                                                                        {
                                                                                                                                                                                                                            if (%t $= "blueKeys")
                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                %this.CurrentLight.BlueKeys = getField(%record, 1);
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                            else
                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                if (%t $= "brightnessKeys")
                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                    %this.CurrentLight.BrightnessKeys = getField(%record, 1);
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                    if (%t $= "radiusKeys")
                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                        %this.CurrentLight.RadiusKeys = getField(%record, 1);
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                        if (%t $= "offsetKeys")
                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                            %this.CurrentLight.OffsetKeys = getField(%record, 1);
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                            if (%t $= "rotationKeys")
                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                %this.CurrentLight.RotationKeys = getField(%record, 1);
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                if (%t $= "colorTime")
                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                    %this.CurrentLight.ColourTime = getField(%record, 1);
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                    if (%t $= "brightnessTime")
                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                        %this.CurrentLight.BrightnessTime = getField(%record, 1);
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                        if (%t $= "radiusTime")
                                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                                            %this.CurrentLight.RadiusTime = getField(%record, 1);
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                                            if (%t $= "offsetTime")
                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                %this.CurrentLight.OffsetTime = getField(%record, 1);
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                if (%t $= "rotationTime")
                                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                                    %this.CurrentLight.RotationTime = getField(%record, 1);
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                                    if (%t $= "lerpColor")
                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                        %this.CurrentLight.LerpColour = getField(%record, 1);
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                        if (%t $= "lerpBrightness")
                                                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                                                            %this.CurrentLight.LerpBrightness = getField(%record, 1);
                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                                                            if (%t $= "lerpRadius")
                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                %this.CurrentLight.LerpRadius = getField(%record, 1);
                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                if (%t $= "lerpOffset")
                                                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                                                    %this.CurrentLight.LerpOffset = getField(%record, 1);
                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                                                    if (%t $= "lerpRotation")
                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                        %this.CurrentLight.LerpRotation = getField(%record, 1);
                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %this.select();
    %this._(openDialog).setVisible(0);
}

function createLightObjectDlg::onSave(%this, %conf)
{
    if ((%this.CurrentWorkingID == 0) || (%this.CurrentWorkingID < -1))
    {
        MessageBoxOK("Error", "Invalid resource id");
        return;
    }
    if (!isDatablock(%this.CurrentLight))
    {
        MessageBoxOK("Error", "Could not find required datablocks");
        return;
    }
    if (!%conf && (%this.CurrentWorkingID == -1))
    {
        MessageBoxYesNo("Question", "This will insert a new light object, Are you sure that is what you want?", "createLightObjectDlg.onSave(true);", "");
        return;
    }

    %data = "name" TAB %this.CurrentLight.className;
    %data = %data NL "staticLight" TAB %this.CurrentLight.StaticLight;
    %data = %data NL "spotLight" TAB %this.CurrentLight.SpotLight;
    %data = %data NL "spotAngle" TAB %this.CurrentLight.SpotAngle;
    %data = %data NL "advancedLighting" TAB %this.CurrentLight.AdvancedLightingModel;
    %data = %data NL "affectDTS" TAB %this.CurrentLight.EffectsDTSObjects;
    %data = %data NL "castShadows" TAB %this.CurrentLight.CastsShadows;
    %data = %data NL "diffuseZone" TAB %this.CurrentLight.DiffuseRestrictZone;
    %data = %data NL "ambientZone" TAB %this.CurrentLight.AmbientRestrictZone;
    %data = %data NL "localAmbAmnt" TAB %this.CurrentLight.LocalAmbientAmount;
    %data = %data NL "smoothSpot" TAB %this.CurrentLight.SmoothSpotLight;
    %data = %data NL "dblSidedAmb" TAB %this.CurrentLight.DoubleSidedAmbient;
    %data = %data NL "useNormals" TAB %this.CurrentLight.UseNormals;
    %data = %data NL "mountPoint" TAB %this.CurrentLight.MountPoint;
    %data = %data NL "mountPosition" TAB %this.CurrentLight.MountPosition;
    %data = %data NL "mountRotation" TAB %this.CurrentLight.MountRotation;
    %data = %data NL "lightOn" TAB %this.CurrentLight.LightOn;
    %data = %data NL "radius" TAB %this.CurrentLight.Radius;
    %data = %data NL "brightness" TAB %this.CurrentLight.Brightness;
    %data = %data NL "color" TAB %this.CurrentLight.Colour;
    %data = %data NL "flareOn" TAB %this.CurrentLight.FlareOn;
    %data = %data NL "flareTp" TAB %this.CurrentLight.FlareTP;
    %data = %data NL "flareBitmap" TAB %this.CurrentLight.FlareBitmap;
    %data = %data NL "flareColor" TAB %this.CurrentLight.FlareColour;
    %data = %data NL "constantSizeOn" TAB %this.CurrentLight.ConstantSizeOn;
    %data = %data NL "constantSize" TAB %this.CurrentLight.ConstantSize;
    %data = %data NL "nearSize" TAB %this.CurrentLight.NearSize;
    %data = %data NL "farSize" TAB %this.CurrentLight.FarSize;
    %data = %data NL "nearDistance" TAB %this.CurrentLight.NearDistance;
    %data = %data NL "farDisatance" TAB %this.CurrentLight.FarDistance;
    %data = %data NL "fadeTime" TAB %this.CurrentLight.FadeTime;
    %data = %data NL "blendMode" TAB %this.CurrentLight.BlendMode;
    %data = %data NL "animColor" TAB %this.CurrentLight.AnimColour;
    %data = %data NL "animBrightness" TAB %this.CurrentLight.AnimBrightness;
    %data = %data NL "animRadius" TAB %this.CurrentLight.AnimRadius;
    %data = %data NL "animOffsets" TAB %this.CurrentLight.AnimOffsets;
    %data = %data NL "animRotation" TAB %this.CurrentLight.AnimRotation;
    %data = %data NL "linkFlare" TAB %this.CurrentLight.LinkFlare;
    %data = %data NL "linkFlareSize" TAB %this.CurrentLight.LinkFlareSize;
    %data = %data NL "minColor" TAB %this.CurrentLight.MinColour;
    %data = %data NL "maxColor" TAB %this.CurrentLight.MaxColour;
    %data = %data NL "minBrightness" TAB %this.CurrentLight.MinBrightness;
    %data = %data NL "maxBrightness" TAB %this.CurrentLight.MaxBrightness;
    %data = %data NL "minRadius" TAB %this.CurrentLight.MinRadius;
    %data = %data NL "maxRadius" TAB %this.CurrentLight.MaxRadius;
    %data = %data NL "startOffset" TAB %this.CurrentLight.StartOffset;
    %data = %data NL "endOffset" TAB %this.CurrentLight.EndOffset;
    %data = %data NL "minRotation" TAB %this.CurrentLight.MinRotation;
    %data = %data NL "maxRotation" TAB %this.CurrentLight.MaxRotation;
    %data = %data NL "singleColorKeys" TAB %this.CurrentLight.SingleColourKeys;
    %data = %data NL "redKeys" TAB %this.CurrentLight.RedKeys;
    %data = %data NL "greenKeys" TAB %this.CurrentLight.GreenKeys;
    %data = %data NL "blueKeys" TAB %this.CurrentLight.BlueKeys;
    %data = %data NL "brightnessKeys" TAB %this.CurrentLight.BrightnessKeys;
    %data = %data NL "radiusKeys" TAB %this.CurrentLight.RadiusKeys;
    %data = %data NL "offsetKeys" TAB %this.CurrentLight.OffsetKeys;
    %data = %data NL "rotationKeys" TAB %this.CurrentLight.RotationKeys;
    %data = %data NL "colorTime" TAB %this.CurrentLight.ColourTime;
    %data = %data NL "brightnessTime" TAB %this.CurrentLight.BrightnessTime;
    %data = %data NL "radiusTime" TAB %this.CurrentLight.RadiusTime;
    %data = %data NL "offsetTime" TAB %this.CurrentLight.OffsetTime;
    %data = %data NL "rotationTime" TAB %this.CurrentLight.RotationTime;
    %data = %data NL "lerpColor" TAB %this.CurrentLight.LerpColour;
    %data = %data NL "lerpBrightness" TAB %this.CurrentLight.LerpBrightness;
    %data = %data NL "lerpRadius" TAB %this.CurrentLight.LerpRadius;
    %data = %data NL "lerpOffset" TAB %this.CurrentLight.LerpOffset;
    %data = %data NL "lerpRotation" TAB %this.CurrentLight.LerpRotation;
    
    if (%this.CurrentWorkingID != -1)
    {
        MasterServerConnection.requestUpdateResource("light_objects", %this.CurrentWorkingID, 0, %data);
    }
    else
    {
        MasterServerConnection.requestUpdateResource("light_objects", 0, 0, %data);
    }
}

function createLightObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.root = %this.addRootType($objects::tableID[light_objects], "Light Objects");
}

function createLightObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("light_objects", %catID);
}
