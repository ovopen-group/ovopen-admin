exec("./createSceneObjectDlg.gui");

function createSceneObjectDlg::onWake(%this)
{
    %this.onNew();
}

function createSceneObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %shapeFile = %this._(shapeEditBox).getText();
    %textureTexture = %this._(textureEditBox).getText();
    %planeSort = %this._(planeSortCheck).getValue();
    
    if (%textureTexture $= "")
    {
        %textureTexture = "$NULL";
    }

    if (((%name !$= "")) && ((%shapeFile !$= "")))
    {
        %data = "name" TAB %name NL "shapeFile" TAB %shapeFile NL "textureFile" TAB %textureTexture NL "planeSort" TAB %planeSort NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createSceneObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("scene_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createSceneObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createSceneObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createSceneObjectTree.getParent(%parentItem);
                    %value = createSceneObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("scene_objects", 0, %parentID, %data);
        }
        createSceneObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and shape");
    }
}

function createSceneObjectDlg::onBrowseTextures(%this)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetTexture", %this._(textureEditBox).getText());
}

function createSceneObjectDlg::onSetTexture(%this, %texFile)
{
    %this._(textureEditBox).setText(%texFile);
}

function createSceneObjectDlg::onBrowseShapes(%this)
{
    getLoadFilename("*.dts", %this @ ".onSetShape", %this._(shapeEditBox).getText());
}

function createSceneObjectDlg::onSetShape(%this, %shapeFilename)
{
    %this._(shapeEditBox).setText(%shapeFilename);
}

function createSceneObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createSceneObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[scene_objects])
        {
            %this.receivedSceneObjectData(%data);
        }
    }
}

function createSceneObjectDlg::receivedSceneObjectData(%this, %data)
{
    %i = -1;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "shapeFile")
                {
                    %this._(shapeEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "textureFile")
                    {
                        %this._(textureEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "planeSort")
                        {
                            %this._(planeSortCheck).setValue(getField(%record, 1));
                        }
                    }
                }
            }
        }
    }
    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
}

function createSceneObjectDlg::onNew(%this)
{
    %this._(nameEditBox).setText("");
    %this._(shapeEditBox).setText("");
    %this._(textureEditBox).setText("");
    %this._(planeSortCheck).setValue(0);
    %this.curUpdateItem = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
}

function createSceneObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.addRootType($objects::tableID[scene_objects], "Scene Objects");
}

function createSceneObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("scene_objects", %catID);
}

function createSceneObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createSceneObjectDlg.getResourceData("scene_objects", getField(%value, 2), %item);
    }
}
