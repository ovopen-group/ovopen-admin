exec("./createClothingObjectDlg.gui");
function createClothingObjectDlg::onWake(%this)
{
    %this._(typeList).clear();
    %this._(typeList).add("res_hair", $clothing::type[hair]);
    %this._(typeList).add("res_eyes", $clothing::type[eyes]);
    %this._(typeList).add("res_lips", $clothing::type[lips]);
    %this._(typeList).add("chest", $clothing::type[chest]);
    %this._(typeList).add("hands", $clothing::type[hands]);
    %this._(typeList).add("legs", $clothing::type[legs]);
    %this._(typeList).add("feet", $clothing::type[feet]);
    %this._(typeList).add("over", $clothing::type[over]);
    %this._(typeList).add("hats", $clothing::type[hats]);
    %this._(typeList).add("face", $clothing::type[face]);
    %this._(typeList).add("belt", $clothing::type[belt]);
    %this._(typeList).add("ear", $clothing::type[ear]);
    %this._(typeList).add("neck", $clothing::type[neck]);
    %this._(tierList).clear();
    %this._(tierList).add("Tier 0", 0);
    %this._(tierList).add("Tier 1", 1);
    %this._(tierList).add("Tier 2", 2);
    %this._(tierList).add("Tier 3", 3);
    %this._(tierList).add("Tier 4", 4);
    %this.onNew();
}

function createClothingObjectDlg::SetMeshMask(%this, %val)
{
    for (%i=0; %i < 22; %i++)
    {
        %this._("hideClothingMesh" @ %i + 1).setValue(%val & (1 << %i));
    }
}

function createClothingObjectDlg::getMeshMask(%this)
{
    %mask = 0;
    for (%i=0; %i < 22; %i++)
    {
        %val = %this._("hideClothingMesh" @ %i + 1).getValue();
        %mask |= (%val << %i);
    }
    return %mask;
}

function createClothingObjectDlg::SetClothingMask(%this, %val)
{
    for (%i=0; %i < 13; %i++)
    {
        %this._("hideClothing" @ %i + 1).setValue(%val & (1 << %i));
    }
}

function createClothingObjectDlg::getClothingMask(%this)
{
    %mask = 0;
    for (%i=0; %i < 13; %i++)
    {
        %val = %this._("hideClothing" @ %i + 1).getValue();
        %mask |= (%val << %i);
    }
    return %mask;
}

function createClothingObjectDlg::onAcquireItem(%this)
{
    AC_AcquireItem("Clothing", %this.curUpdateID);
}

function createClothingObjectDlg::onCopy(%this)
{
    %this.isUpdate = 0;
    %this.curUpdateID = 0;
    %this._(idEditBox).setText("");
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
    %this._(AcquireButton).setVisible(0);
}

function createClothingObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %dispName = %this._(dispNameEditBox).getText();
    %desc = %this._(descEditBox).getText();
    %male = %this._(maleCheck).getValue();
    %female = %this._(femaleCheck).getValue();
    %type = %this._(typeList).getSelected();
    %tier = %this._(tierList).getSelected();
    %meshMask = %this.getMeshMask();
    %clothingMask = %this.getClothingMask();
    %maleShapeFile = %this._(maleShapeEditBox).getText();
    %femaleShapeFile = %this._(femaleShapeEditBox).getText();
    %maleTextureFile = %this._(maleTextureEditBox).getText();
    %femaleTextureFile = %this._(femaleTextureEditBox).getText();
    %thumbNail = %this._(thumbNailEditBox).getText();
    %CC = %this._(ccEditBox).getText();
    %PP = %this._(ppEditBox).getText();

    if (%maleTextureFile $= "")
    {
        %maleTextureFile = "$NULL";
    }
    if (%femaleTextureFile $= "")
    {
        %femaleTextureFile = "$NULL";
    }
    if (%maleShapeFile $= "")
    {
        %maleShapeFile = "$NULL";
    }
    if (%femaleShapeFile $= "")
    {
        %femaleShapeFile = "$NULL";
    }
    if (%CC $= "")
    {
        %CC = 0;
    }
    if (%PP $= "")
    {
        %PP = 0;
    }

    if (((%name !$= "")) && (%type != 0))
    {
        %data = "name" TAB %name NL "dispName" TAB %dispName NL "description" TAB %desc NL "type" TAB %type NL "mask" TAB %meshMask NL "slotMask" TAB %clothingMask NL "M" TAB %male NL "F" TAB %female NL "thumbnail" TAB %thumbNail NL "M_TextureFile" TAB %maleTextureFile NL "F_TextureFile" TAB %femaleTextureFile NL "M_ShapeFile" TAB %maleShapeFile NL "F_ShapeFile" TAB %femaleShapeFile NL "CC" TAB %CC NL "PP" TAB %PP NL "tier" TAB %tier NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createClothingObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("clothing_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createClothingObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createClothingObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createClothingObjectTree.getParent(%parentItem);
                    %value = createClothingObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("clothing_objects", 0, %parentID, %data);
        }
        createClothingObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and type");
    }
}

function createClothingObjectDlg::onBrowseTextures(%this, %type)
{
    if (%type $= "Male")
    {
        getLoadFilename("*.png\t*.jpg", %this @ ".onSetTextureMale", %this._(maleTextureEditBox).getText());
    }
    else
    {
        if (%type $= "female")
        {
            getLoadFilename("*.png\t*.jpg", %this @ ".onSetTextureFemale", %this._(femaleTextureEditBox).getText());
        }
        else
        {
            MessageBoxOK("Error", "Invalid expecting Male/Female type");
        }
    }
}

function createClothingObjectDlg::onSetTextureMale(%this, %texFile)
{
    %this._(maleTextureEditBox).setText(%texFile);
}

function createClothingObjectDlg::onSetTextureFemale(%this, %texFile)
{
    %this._(femaleTextureEditBox).setText(%texFile);
}

function createClothingObjectDlg::onBrowseShapes(%this, %type)
{
    if (%type $= "Male")
    {
        getLoadFilename("*.dts", %this @ ".onSetShapeMale", %this._(maleShapeEditBox).getText());
    }
    else
    {
        if (%type $= "female")
        {
            getLoadFilename("*.dts", %this @ ".onSetShapeFemale", %this._(femaleShapeEditBox).getText());
        }
        else
        {
            MessageBoxOK("Error", "Invalid expecting Male/Female type");
        }
    }
}

function createClothingObjectDlg::onBrowseThumbnail(%this)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetThumbnail", %this._(thumbNailEditBox).getText());
}

function createClothingObjectDlg::onSetThumbnail(%this, %filename)
{
    %this._(thumbNailEditBox).setText(%filename);
}

function createClothingObjectDlg::onSetShapeMale(%this, %shapeFile)
{
    %this._(maleShapeEditBox).setText(%shapeFile);
}

function createClothingObjectDlg::onSetShapeFemale(%this, %shapeFile)
{
    %this._(femaleShapeEditBox).setText(%shapeFile);
}

function createClothingObjectDlg::onBrowseDesc(%this)
{
    getLoadFilename("*.txt", %this @ ".onSetDesc", %this._(descEditBox).getText());
}

function createClothingObjectDlg::onSetDesc(%this, %text)
{
    %this._(descEditBox).setText(%text);
}

function createClothingObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createClothingObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[clothing_objects])
        {
            %this.receivedSceneObjectData(%data);
        }
    }
}

function createClothingObjectDlg::receivedSceneObjectData(%this, %data)
{
    %i = -1;
    %type = 0;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "name")
                {
                    %this._(nameEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "dispName")
                    {
                        %this._(dispNameEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "description")
                        {
                            %this._(descEditBox).setText(getField(%record, 1));
                        }
                        else
                        {
                            if (getField(%record, 0) $= "type")
                            {
                                %type = getField(%record, 1);
                            }
                            else
                            {
                                if (getField(%record, 0) $= "tier")
                                {
                                    %this._(tierList).setSelected(getField(%record, 1));
                                }
                                else
                                {
                                    if (getField(%record, 0) $= "M")
                                    {
                                        %this._(maleCheck).setValue(getField(%record, 1));
                                    }
                                    else
                                    {
                                        if (getField(%record, 0) $= "F")
                                        {
                                            %this._(femaleCheck).setValue(getField(%record, 1));
                                        }
                                        else
                                        {
                                            if (getField(%record, 0) $= "M_TextureFile")
                                            {
                                                %this._(maleTextureEditBox).setText(getField(%record, 1));
                                            }
                                            else
                                            {
                                                if (getField(%record, 0) $= "F_TextureFile")
                                                {
                                                    %this._(femaleTextureEditBox).setText(getField(%record, 1));
                                                }
                                                else
                                                {
                                                    if (getField(%record, 0) $= "M_ShapeFile")
                                                    {
                                                        %this._(maleShapeEditBox).setText(getField(%record, 1));
                                                    }
                                                    else
                                                    {
                                                        if (getField(%record, 0) $= "F_ShapeFile")
                                                        {
                                                            %this._(femaleShapeEditBox).setText(getField(%record, 1));
                                                        }
                                                        else
                                                        {
                                                            if (getField(%record, 0) $= "mask")
                                                            {
                                                                %this.SetMeshMask(getField(%record, 1));
                                                            }
                                                            else
                                                            {
                                                                if (getField(%record, 0) $= "slotMask")
                                                                {
                                                                    %this.SetClothingMask(getField(%record, 1));
                                                                }
                                                                else
                                                                {
                                                                    if (getField(%record, 0) $= "thumbnail")
                                                                    {
                                                                        %this._(thumbNailEditBox).setText(getField(%record, 1));
                                                                    }
                                                                    else
                                                                    {
                                                                        if (getField(%record, 0) $= "CC")
                                                                        {
                                                                            %this._(ccEditBox).setText(getField(%record, 1));
                                                                        }
                                                                        else
                                                                        {
                                                                            if (getField(%record, 0) $= "PP")
                                                                            {
                                                                                %this._(ppEditBox).setText(getField(%record, 1));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
    %this._(copyButton).setVisible(1);
    %this._(typeList).setSelected(%type);
    %this._(AcquireButton).setVisible(%type > $clothing::numrestypes);
    %this._(idEditBox).setText(%this.curUpdateID);
    storeFoldersDlg.insertObj = %this.curUpdateID;
    storeFoldersDlg.insertType = 2;
}

function createClothingObjectDlg::onNew(%this)
{
    %this._(idEditBox).setText("");
    %this._(nameEditBox).setText("");
    %this._(dispNameEditBox).setText("");
    %this._(descEditBox).setText("");
    %this._(maleCheck).setValue(0);
    %this._(femaleCheck).setValue(0);
    %this._(typeList).setSelected($clothing::type[chest]);
    %this._(tierList).setSelected(0);
    %this._(maleShapeEditBox).setText("");
    %this._(maleTextureEditBox).setText("");
    %this._(femaleShapeEditBox).setText("");
    %this._(femaleTextureEditBox).setText("");
    %this.SetMeshMask(0);
    %this.SetClothingMask(0);
    %this._(thumbNailEditBox).setText("");
    %this._(ccEditBox).setText(0);
    %this._(ppEditBox).setText(0);
    %this.curUpdateID = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
    %this._(AcquireButton).setVisible(0);
}

function createClothingObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.curUpdateItem = 0;
    %this.addRootType($objects::tableID[clothing_objects], "Clothing Objects");
}

function createClothingObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("clothing_objects", %catID);
}

function createClothingObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createClothingObjectDlg.getResourceData("clothing_objects", getField(%value, 2), %item);
    }
}
