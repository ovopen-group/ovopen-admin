exec("./createPetObjectDlg.gui");

function createPetObjectDlg::onWake(%this)
{
    %this._(tierList).clear();
    %this._(tierList).add("Tier 0", 0);
    %this._(tierList).add("Tier 1", 1);
    %this._(tierList).add("Tier 2", 2);
    %this._(tierList).add("Tier 3", 3);
    %this._(tierList).add("Tier 4", 4);
    %this.onNew();
}

function createPetObjectDlg::onAcquireItem(%this)
{
    AC_AcquireItem("Pet", %this.curUpdateID);
}

function createPetObjectDlg::onCopy(%this)
{
    %this.isUpdate = 0;
    %this.curUpdateID = 0;
    %this._(idEditBox).setText("");
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
    %this._(AcquireButton).setVisible(0);
}

function createPetObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %dispName = %this._(dispNameEditBox).getText();
    %desc = %this._(descEditBox).getText();
    %tier = %this._(tierList).getSelected();
    %shapeFile = %this._(shapeEditBox).getText();
    %textureFile = %this._(textureEditBox).getText();
    %thumbNail = %this._(thumbNailEditBox).getText();
    %CC = %this._(ccEditBox).getText();
    %PP = %this._(ppEditBox).getText();
    %shoulder = %this._(shoulderCheck).getValue();
    %sound = %this._(soundEditBox).getText();
    %sound2 = %this._(sound2EditBox).getText();
    %bounds = %this._(boundsEditBox).getText();
    %scale = %this._(scaleEditBox).getText();
    %idles = %this._(idlesEditBox).getText();
    %tricks = %this._(tricksEditBox).getText();
    %decalOff = %this._(decalOffEditBox).getText();
    %decalOffY = %this._(decalOffYEditBox).getText();

    if (%textureFile $= "")
    {
        %textureFile = "$NULL";
    }
    if (%sound $= "")
    {
        %sound = "$NULL";
    }
    if (%sound2 $= "")
    {
        %sound2 = "$NULL";
    }
    if (%CC $= "")
    {
        %CC = 0;
    }
    if (%PP $= "")
    {
        %PP = 0;
    }
    if (%decalOff $= "")
    {
        %decalOff = 0;
    }
    if (%decalOffY $= "")
    {
        %decalOffY = 0;
    }

    for (%i=0; %i < 3; %i++)
    {
        if (getWord(%bounds, %i) $= "")
        {
            %bounds = setWord(%bounds, %i, 1);
        }
    }

    for (%i=0; %i < 3; %i++)
    {
        if (getWord(%scale, %i) $= "")
        {
            %scale = setWord(%scale, %i, 1);
        }
    }

    if (%idles $= "")
    {
        %idles = 0;
    }
    if (%tricks $= "")
    {
        %tricks = 0;
    }

    if (((%name !$= "")) && ((%shapeFile !$= "")))
    {
        %data = "name" TAB %name NL "dispName" TAB %dispName NL "description" TAB %desc NL "tier" TAB %tier NL "shapeFile" TAB %shapeFile NL "textureFile" TAB %textureFile NL "thumbnail" TAB %thumbNail NL "CC" TAB %CC NL "PP" TAB %PP NL "shoulder" TAB %shoulder NL "sound" TAB %sound NL "sound2" TAB %sound2 NL "bbx" TAB getWord(%bounds, 0) NL "bby" TAB getWord(%bounds, 1) NL "bbz" TAB getWord(%bounds, 2) NL "sx" TAB getWord(%scale, 0) NL "sy" TAB getWord(%scale, 1) NL "sz" TAB getWord(%scale, 2) NL "idles" TAB %idles NL "tricks" TAB %tricks NL "decalOffset" TAB %decalOff NL "decalOffsetY" TAB %decalOffY NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createPetObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("pet_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createPetObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createPetObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createPetObjectTree.getParent(%parentItem);
                    %value = createPetObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("pet_objects", 0, %parentID, %data);
        }
        createPetObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and shape");
    }
}

function createPetObjectDlg::onBrowseTextures(%this, %type)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetTexture", %this._(textureEditBox).getText());
}

function createPetObjectDlg::onSetTexture(%this, %texFile)
{
    %this._(textureEditBox).setText(%texFile);
}

function createPetObjectDlg::onBrowseShapes(%this, %type)
{
    getLoadFilename("*.dts", %this @ ".onSetShape", %this._(shapeEditBox).getText());
}

function createPetObjectDlg::onSetShape(%this, %shapeFile)
{
    %this._(shapeEditBox).setText(%shapeFile);
}

function createPetObjectDlg::onBrowseThumbnail(%this)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetThumbnail", %this._(thumbNailEditBox).getText());
}

function createPetObjectDlg::onSetThumbnail(%this, %filename)
{
    %this._(thumbNailEditBox).setText(%filename);
}

function createPetObjectDlg::onBrowseDesc(%this)
{
    getLoadFilename("*.txt", %this @ ".onSetDesc", %this._(descEditBox).getText());
}

function createPetObjectDlg::onSetDesc(%this, %text)
{
    %this._(descEditBox).setText(%text);
}

function createPetObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createPetObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[pet_objects])
        {
            %this.receivedPetObjectData(%data);
        }
    }
}

function createPetObjectDlg::receivedPetObjectData(%this, %data)
{
    %i = -1;
    %type = 0;
    %scale = "1 1 1";
    %bounds = "1 1 1";
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "dispName")
                {
                    %this._(dispNameEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "description")
                    {
                        %this._(descEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "tier")
                        {
                            %this._(tierList).setSelected(getField(%record, 1));
                        }
                        else
                        {
                            if (getField(%record, 0) $= "shapeFile")
                            {
                                %this._(shapeEditBox).setText(getField(%record, 1));
                            }
                            else
                            {
                                if (getField(%record, 0) $= "textureFile")
                                {
                                    %this._(textureEditBox).setText(getField(%record, 1));
                                }
                                else
                                {
                                    if (getField(%record, 0) $= "thumbnail")
                                    {
                                        %this._(thumbNailEditBox).setText(getField(%record, 1));
                                    }
                                    else
                                    {
                                        if (getField(%record, 0) $= "CC")
                                        {
                                            %this._(ccEditBox).setText(getField(%record, 1));
                                        }
                                        else
                                        {
                                            if (getField(%record, 0) $= "PP")
                                            {
                                                %this._(ppEditBox).setText(getField(%record, 1));
                                            }
                                            else
                                            {
                                                if (getField(%record, 0) $= "shoulder")
                                                {
                                                    %this._(shoulderCheck).setValue(getField(%record, 1));
                                                }
                                                else
                                                {
                                                    if (getField(%record, 0) $= "sound")
                                                    {
                                                        %this._(soundEditBox).setText(getField(%record, 1));
                                                    }
                                                    else
                                                    {
                                                        if (getField(%record, 0) $= "sound2")
                                                        {
                                                            %this._(sound2EditBox).setText(getField(%record, 1));
                                                        }
                                                        else
                                                        {
                                                            if (getField(%record, 0) $= "bbx")
                                                            {
                                                                %bounds = setWord(%bounds, 0, getField(%record, 1));
                                                            }
                                                            else
                                                            {
                                                                if (getField(%record, 0) $= "bby")
                                                                {
                                                                    %bounds = setWord(%bounds, 1, getField(%record, 1));
                                                                }
                                                                else
                                                                {
                                                                    if (getField(%record, 0) $= "bbz")
                                                                    {
                                                                        %bounds = setWord(%bounds, 2, getField(%record, 1));
                                                                    }
                                                                    else
                                                                    {
                                                                        if (getField(%record, 0) $= "sx")
                                                                        {
                                                                            %scale = setWord(%scale, 0, getField(%record, 1));
                                                                        }
                                                                        else
                                                                        {
                                                                            if (getField(%record, 0) $= "sy")
                                                                            {
                                                                                %scale = setWord(%scale, 1, getField(%record, 1));
                                                                            }
                                                                            else
                                                                            {
                                                                                if (getField(%record, 0) $= "sz")
                                                                                {
                                                                                    %scale = setWord(%scale, 2, getField(%record, 1));
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (getField(%record, 0) $= "idles")
                                                                                    {
                                                                                        %this._(idlesEditBox).setText(getField(%record, 1));
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (getField(%record, 0) $= "tricks")
                                                                                        {
                                                                                            %this._(tricksEditBox).setText(getField(%record, 1));
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (getField(%record, 0) $= "decalOffset")
                                                                                            {
                                                                                                %this._(decalOffEditBox).setText(getField(%record, 1));
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (getField(%record, 0) $= "decalOffsetY")
                                                                                                {
                                                                                                    %this._(decalOffYEditBox).setText(getField(%record, 1));
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    %this._(boundsEditBox).setText(%bounds);
    %this._(scaleEditBox).setText(%scale);
    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
    %this._(AcquireButton).setVisible(1);
    %this._(copyButton).setVisible(1);
    %this._(idEditBox).setText(%this.curUpdateID);
    storeFoldersDlg.insertObj = %this.curUpdateID;
    storeFoldersDlg.insertType = 4;
}

function createPetObjectDlg::onNew(%this)
{
    %this._(idEditBox).setText("");
    %this._(nameEditBox).setText("");
    %this._(dispNameEditBox).setText("");
    %this._(descEditBox).setText("");
    %this._(tierList).setSelected(0);
    %this._(shapeEditBox).setText("");
    %this._(textureEditBox).setText("");
    %this._(thumbNailEditBox).setText("");
    %this._(ccEditBox).setText("0");
    %this._(ppEditBox).setText("0");
    %this._(shoulderCheck).setValue("0");
    %this._(soundEditBox).setText("");
    %this._(sound2EditBox).setText("");
    %this._(boundsEditBox).setText("1 1 1");
    %this._(scaleEditBox).setText("1 1 1");
    %this._(idlesEditBox).setText(0);
    %this._(tricksEditBox).setText(0);
    %this._(decalOffEditBox).setText("0.10");
    %this._(decalOffYEditBox).setText("0.0");
    %this.curUpdateID = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
    %this._(AcquireButton).setVisible(0);
    %this._(copyButton).setVisible(0);
}

function createPetObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.curUpdateItem = 0;
    %this.addRootType($objects::tableID[pet_objects], "Pet Objects");
}

function createPetObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("pet_objects", %catID);
}

function createPetObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createPetObjectDlg.getResourceData("pet_objects", getField(%value, 2), %item);
    }
}
