exec("./createInteriorObjectDlg.gui");

function createInteriorObjectDlg::onWake(%this)
{
    %this._(nameEditBox).setText("");
    %this._(interiorEditBox).setText("");
}

function createInteriorObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %interiorFile = %this._(interiorEditBox).getText();
    if (((%name !$= "")) && ((%interiorFile !$= "")))
    {
        %data = "name" TAB %name NL "interiorFile" TAB %interiorFile NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createInteriorObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("interior_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createInteriorObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createInteriorObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createInteriorObjectTree.getParent(%parentItem);
                    %value = createInteriorObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("interior_objects", 0, %parentID, %data);
        }
        createInteriorObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and interior file");
    }
}

function createInteriorObjectDlg::onBrowseInteriors(%this)
{
    getLoadFilename("*.dif", %this @ ".onSetInterior", %this._(interiorEditBox).getText());
}

function createInteriorObjectDlg::onSetInterior(%this, %interiorFile)
{
    %this._(interiorEditBox).setText(%interiorFile);
}

function createInteriorObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createInteriorObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[interior_objects])
        {
            %this.receivedInteriorObjectData(%data);
        }
    }
}

function createInteriorObjectDlg::receivedInteriorObjectData(%this, %data)
{
    %i = -1;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        if (getField(%record, 0) $= "name")
        {
            %this._(nameEditBox).setText(getField(%record, 1));
        }
        if (getField(%record, 0) $= "interiorFile")
        {
            %this._(interiorEditBox).setText(getField(%record, 1));
        }
    }

    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
}

function createInteriorObjectDlg::onNew(%this)
{
    %this._(nameEditBox).setText("");
    %this._(interiorEditBox).setText("");
    %this.curUpdateItem = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
}

function createInteriorObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.addRootType($objects::tableID[interior_objects], "Interior Objects");
}

function createInteriorObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("interior_objects", %catID);
}

function createInteriorObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createInteriorObjectDlg.getResourceData("interior_objects", getField(%value, 2), %item);
    }
}
