exec("./storeFoldersDlg.gui");

function RPC_StoreFolders::onNoePermission(%this)
{
    MessageBoxOK("Error", "You do not have permission to modify store folders");
    error("RPC_StoreFolders: You do not have permission to modify store folders");
}

function RPC_StoreFolders::onReturnInvalidFolder(%this)
{
    MessageBoxOK("Error", "Invalid folder specified");
    error("RPC_StoreFolders: Invalid folder specified");
}

function RPC_StoreFolders::onReturnInvalidItem(%this)
{
    MessageBoxOK("Error", "Invalid item specified");
    error("RPC_StoreFolders: Invalid item specified");
}

function RPC_StoreFolders::onReturnInvalidCommand(%this)
{
    MessageBoxOK("Error", "(RPC_StoreFolders) Invalid command");
    error("RPC_StoreFolders: Invalid command");
}

function RPC_StoreFolders::onReturnNumberArguments(%this)
{
    MessageBoxOK("Error", "(RPC_StoreFolders) Invalid num arguments");
    error("RPC_StoreFolders: Invalid num arguments");
}

function RPC_StoreFolders::onReturnGeneralError(%this)
{
    MessageBoxOK("Error", "(RPC_StoreFolders) General Error");
    error("RPC_StoreFolders: General Error");
}

function RPC_StoreFolders::onClearChildren(%this)
{
    if (0 == %this.folderItem)
    {
        error("Received NULL folder ID on addChildren children.");
        return;
    }
    StoreFolderTree.removeAllChildren(%this.folderItem);
}

function RPC_StoreFolders::onAddFolder(%this, %folderData)
{
    %folderID = getField(%folderData, 0);
    %name = getField(%folderData, 1);
    %folder = StoreFolderTree.addFolder(%this.folderItem, %name, %folderID);
    if (%name $= getField(storeFoldersDlg.currentDir, 0))
    {
        storeFoldersDlg.currentDir = getFields(storeFoldersDlg.currentDir, 1);
        if (storeFoldersDlg.currentDir $= "")
        {
            StoreFolderTree.selectItem(%folder);
        }
        else
        {
            StoreFolderTree.expandItem(%folder);
        }
    }
}

function RPC_StoreFolders::onAddObject(%this, %objectData)
{
    %entryID = getField(%objectData, 0);
    %type = getField(%objectData, 1);
    %objectID = getField(%objectData, 2);
    %name = getField(%objectData, 3);
    StoreFolderTree.addObject(%this.folderItem, %name, %entryID, %type, %objectID);
}

function RPC_StoreFolders::onReturnOk(%this)
{
    if (!(%this.Command $= COMMAND_FOLDER_ITEMS))
    {
        StoreFolderTree.loadChildren(%this.folderItem);
    }
}

function ShowStoreFoldersManager()
{
    storeFoldersDlg.insertObj = 0;
    storeFoldersDlg.insertType = 0;
    storeFoldersDlg.currentDir = "";
    storeFoldersDlg.callback = "";
    storeFoldersDlg.doOpen = 0;
    Canvas.popDialog(storeFoldersDlg);
    Canvas.pushDialog(storeFoldersDlg);
}

function ShowStoreFoldersInsert(%objectID, %type)
{
    if ((%objectID == 0) || (%type == 0))
    {
        MessageBoxOK("Error", "Must be an existing interactive object.");
        return;
    }

    storeFoldersDlg.insertObj = %objectID;
    storeFoldersDlg.insertType = %type;
    storeFoldersDlg.currentDir = "";
    storeFoldersDlg.callback = "";
    storeFoldersDlg.doOpen = 0;
    Canvas.popDialog(storeFoldersDlg);
    Canvas.pushDialog(storeFoldersDlg);
}

function ShowStoreFoldersOpen(%callback, %current)
{
    storeFoldersDlg.insertObj = 0;
    storeFoldersDlg.insertType = 0;
    storeFoldersDlg.currentDir = strreplace(%current, "/", "\t");
    storeFoldersDlg.callback = %callback;
    storeFoldersDlg.doOpen = 1;
    Canvas.popDialog(storeFoldersDlg);
    Canvas.pushDialog(storeFoldersDlg);
}

function StoreFolderTree::addStoreFolder(%this, %name)
{
    if (%name $= "")
    {
        MessageBoxEntryOk("Add", "Name:", %this @ ".addStoreFolder", "");
        return;
    }

    %selected = %this.getSelectedItemList();
    if (!(%selected $= ""))
    {
        %parentItem = getWord(%selected, 0);
    }
    else
    {
        %parentItem = 1;
    }

    %value = %this.getItemValue(%parentItem);
    if (!getField(%value, 0))
    {
        %parentItem = %this.getParent(%parentItem);
        %value = %this.getItemValue(%parentItem);
    }
    %folderID = getField(%value, 1);
    %rpcObject = new RPC_StoreFolders() {
    };
    %rpcObject.Command = COMMAND_ADD_FOLDER;
    %rpcObject.arguments = %folderID;
    %rpcObject.arguments = %name;
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.folderItem = %parentItem;
}

function StoreFolderTree::insertStoreFolderItem(%this)
{
    if ((storeFoldersDlg.insertObj == 0) || (storeFoldersDlg.insertType == 0))
    {
        MessageBoxOK("Error", "Must be an existing interactive object.");
        return;
    }

    %selected = %this.getSelectedItemList();
    if (!(%selected $= ""))
    {
        %parentItem = getWord(%selected, 0);
    }
    else
    {
        %parentItem = 1;
    }

    %value = %this.getItemValue(%parentItem);
    if (!getField(%value, 0))
    {
        %parentItem = %this.getParent(%parentItem);
        %value = %this.getItemValue(%parentItem);
    }
    %folderID = getField(%value, 1);
    %rpcObject = new RPC_StoreFolders() {
    };
    %rpcObject.Command = COMMAND_INSERT_ITEM;
    %rpcObject.arguments = %folderID;
    %rpcObject.arguments = storeFoldersDlg.insertObj;
    %rpcObject.arguments = storeFoldersDlg.insertType;
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.folderItem = %parentItem;
}

function StoreFolderTree::onDeleteSelection(%this)
{
    %selected = %this.getSelectedItemList();
    if (!(%selected $= ""))
    {
        %folderItem = getWord(%selected, 0);
    }
    else
    {
        return;
    }

    %value = %this.getItemValue(%folderItem);
    if (getField(%value, 0))
    {
        %this.onDeleteFolder(%folderItem);
    }
    else
    {
        %this.onDeleteItem(%folderItem);
    }
}

function StoreFolderTree::onRenameSelection(%this, %name)
{
    %selected = %this.getSelectedItemList();
    if (!(%selected $= ""))
    {
        %folderItem = getWord(%selected, 0);
    }
    else
    {
        return;
    }

    %value = %this.getItemValue(%folderItem);
    if (getField(%value, 0))
    {
        if (%name $= "")
        {
            MessageBoxEntryOk("Add", "Name:", %this @ ".onRenameSelection", "");
            return;
        }
        %folderID = getField(%value, 1);
        %rpcObject = new RPC_StoreFolders() {
        };
        %rpcObject.Command = COMMAND_RENAME_FOLDER;
        %rpcObject.arguments = %folderID;
        %rpcObject.arguments = %name;
        %rpcObject.fireRPC(MasterServerConnection);
        %rpcObject.folderItem = %this.getParent(%folderItem);
    }
}

function StoreFolderTree::onMoveItem(%this, %folderItem, %newFolder)
{
    %value = %this.getItemValue(%newFolder);
    if (!getField(%value, 0))
    {
        %newFolder = %this.getParent(%newFolder);
        %value = %this.getItemValue(%newFolder);
    }
    %newFolderID = getField(%value, 1);
    %value = %this.getItemValue(%folderItem);
    %this.removeItem(%folderItem);
    %cmd = COMMAND_MOVE_FOLDER;
    if (!getField(%value, 0))
    {
        %cmd = COMMAND_MOVE_ITEM;
    }
    %rpcObject = new RPC_StoreFolders() {
    };
    %rpcObject.Command = %cmd;
    %rpcObject.arguments = getField(%value, 1);
    %rpcObject.arguments = %newFolderID;
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.folderItem = %newFolder;
    return;
}

function StoreFolderTree::onOpenSelection(%this, %name)
{
    if (!storeFoldersDlg.doOpen)
    {
        return;
    }
    %selected = %this.getSelectedItemList();
    if (%selected !$= "")
    {
        %folderItem = getWord(%selected, 0);
    }
    else
    {
        return;
    }

    %value = %this.getItemValue(%folderItem);
    if (getField(%value, 0))
    {
        %finalString = "";
        while (%folderItem != 1)
        {
            %finalString = %this.getItemText(%folderItem) @ "/" @ %finalString;
            %folderItem = %this.getParent(%folderItem);
        }
        %finalString = "/" @ %finalString;
        %this.lastOpenID = getField(%value, 1);
        eval(storeFoldersDlg.callback @ "(\"" @ %finalString @ "\");");
        Canvas.popDialog(storeFoldersDlg);
    }
}

function storeFoldersDlg::onWake(%this)
{
    if (%this.doOpen)
    {
        %this._(AddButton).setVisible(0);
        %this._(InsertButton).setVisible(0);
        %this._(deleteButton).setVisible(0);
        %this._(renameButton).setVisible(0);
        %this._(openButton).setVisible(1);
    }
    else
    {
        %isInsert = %this.insertObj != 0;
        %this._(AddButton).setVisible(!%isInsert);
        %this._(InsertButton).setVisible(%isInsert);
        %this._(deleteButton).setVisible(1);
        %this._(renameButton).setVisible(1);
        %this._(openButton).setVisible(0);
    }
}

function StoreFolderTree::onWake(%this)
{
    if (%this.destroyTreeOnSleep)
    {
        schedule(1, 0, MessageBoxOK, "Warn", "Bad Configuration, this tree will not work correctly.");
    }

    if (!MasterServerConnection.isConnected())
    {
        schedule(1, 0, MessageBoxOK, "Error", "Not connected, this dialog will not work correctly.");
    }

    %this.clearSelection();
    %this.clear();
    %this.buildIconTable();

    %rootItem = %this.addFolder(0, "Root", 0);
    if (storeFoldersDlg.doOpen)
    {
        storeFoldersDlg.currentDir = getFields(storeFoldersDlg.currentDir, 1);
        %this.expandItem(%rootItem);
    }
}

function StoreFolderTree::addFolder(%this, %parent, %name, %folderID)
{
    %parentID = %this.insertItem(%parent, %name, 1 TAB %folderID, "", 2, 1);
    %this.insertItem(%parentID, $CategoryLoadingText);
    return %parentID;
}

function StoreFolderTree::addObject(%this, %parentID, %name, %entryID, %type, %objectID)
{
    if (%type == 2)
    {
        %icon = ClientEditInteriorInstance;
    }
    else
    {
        if (%type == 3)
        {
            %icon = Item;
        }
        else
        {
            if (%type == 4)
            {
                %icon = ClientEditTrigger;
            }
            else
            {
                %icon = ClientEditTSStatic;
            }
        }
    }

    %this.insertItem(%parentID, %name, 0 TAB %entryID TAB %type TAB %objectID, %icon);
}

function StoreFolderTree::onExpand(%this, %folderItem)
{
    %child = %this.getChild(%folderItem);
    if (%child && !strcmp($CategoryLoadingText, %this.getItemText(%child)))
    {
        %this.loadChildren(%folderItem);
    }
}

function StoreFolderTree::loadChildren(%this, %folderItem)
{
    %value = %this.getItemValue(%folderItem);
    if (getField(%value, 0))
    {
        %folderID = getField(%value, 1);
        %rpcObject = new RPC_StoreFolders()
        {
        };
        %rpcObject.Command = COMMAND_FOLDER_ITEMS;
        %rpcObject.arguments = %folderID;
        %rpcObject.fireRPC(MasterServerConnection);
        %rpcObject.folderItem = %folderItem;
    }
}

function StoreFolderTree::onDeleteFolder(%this, %folderItem)
{
    if (storeFoldersDlg.doOpen)
    {
        return;
    }

    if (%this.getChild(%folderItem))
    {
        MessageBoxOK("Error", "Can only delete visted empty folders.", "");
        return;
    }

    %value = %this.getItemValue(%folderItem);
    %folderID = getField(%value, 1);
    if (%folderID == 0)
    {
        MessageBoxOK("Error", "Cannot delete root folders.", "");
        return;
    }

    %rpcObject = new RPC_StoreFolders() {
    };
    %rpcObject.Command = COMMAND_DELETE_FOLDER;
    %rpcObject.arguments = %folderID;
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.folderItem = %this.getParent(%folderItem);
}

function StoreFolderTree::onDeleteItem(%this, %objectItem)
{
    if (storeFoldersDlg.doOpen)
    {
        return;
    }

    %value = %this.getItemValue(%objectItem);
    %entryID = getField(%value, 1);
    %rpcObject = new RPC_StoreFolders() {
    };
    %rpcObject.Command = COMMAND_DELETE_ITEM;
    %rpcObject.arguments = %entryID;
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.folderItem = %this.getParent(%objectItem);
}
