exec("./createInteractiveObjectDlg.gui");

function createInteractiveObjectDlg::onWake(%this)
{
    %this._(typeList).clear();
    %this._(typeList).add("floor", 0);
    %this._(typeList).add("ceiling", 1);
    %this._(typeList).add("wall", 2);
    %this._(tierList).clear();
    %this._(tierList).add("Tier 0", 0);
    %this._(tierList).add("Tier 1", 1);
    %this._(tierList).add("Tier 2", 2);
    %this._(tierList).add("Tier 3", 3);
    %this._(tierList).add("Tier 4", 4);
    %this.onNew();
}

function createInteractiveObjectDlg::buildNvPairsString(%this, %base)
{
    %stack = %this._(%base @ "ValueStack");
    %string = "";
    %count = %stack.getCount();

    for (%i=0; %i < %count; %i++)
    {
        %obj = %stack.getObject(%i);
        %name = %obj._("valueName").getText();
        %value = %obj._("valueValue").getText();
        %name = strreplace(strreplace(%name, ",", ""), " ", "");
        %value = strreplace(%value, ",", "");
        if (%name $= "")
        {
            continue;
        }
        %string = %string @ %name TAB %value NL "";
    }

    return %string;
}

function createInteractiveObjectDlg::decodeNvPairsString(%this, %base, %str)
{
    %stack = %this._(%base @ "ValueStack");
    while (%stack.getCount())
    {
        %stack.getObject(0).delete();
    }

    %count = getRecordCount(%str);
    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%str, %i);
        %name = getField(%record, 0);
        %value = getField(%record, 1);
        %obj = %this.onAddValue(%base);
        %obj._("valueName").setText(%name);
        %obj._("valueValue").setText(%value);
    }
}

function createInteractiveObjectDlg::onAcquireItem(%this)
{
    if (%this._(furnitureCheck).getValue())
    {
        AC_AcquireItem("Furniture", %this.curUpdateID);
    }
}

function createInteractiveObjectDlg::onCopy(%this)
{
    %this.isUpdate = 0;
    %this.curUpdateID = 0;
    %this._(idEditBox).setText("");
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
    %this._(AcquireButton).setVisible(0);
}

function createInteractiveObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %dispName = %this._(dispNameEditBox).getText();
    %desc = %this._(descEditBox).getText();
    %className = %this._(classNameEditBox).getText();
    %canShowRightClick = %this._(rightClickCheck).getValue();
    %furniture = %this._(furnitureCheck).getValue();
    %wall = %this._(wallCheck).getValue();
    %planeSort = %this._(planeSortCheck).getValue();
    %type = %this._(typeList).getSelected();
    %tier = %this._(tierList).getSelected();
    %shapeFile = %this._(shapeEditBox).getText();
    %textureFile = %this._(textureEditBox).getText();
    %shadow = %this._(shadowCheck).getValue();
    %lightMount = %this._(lightMountEditBox).getText();
    %light = %this._(lightEditBox).getText();
    %particleMount = %this._(particleMountEditBox).getText();
    %particle = %this._(particleEditBox).getText();
    %CC = %this._(ccEditBox).getText();
    %PP = %this._(ppEditBox).getText();
    %sound = %this._(soundEditBox).getText();
    %nvPairs = %this.buildNvPairsString(data);
    %nvPairsObj = %this.buildNvPairsString(obj);
    %storeFolderID = %this._(storeFolderEditBox).getText();
    %tier[0] = %this._(tier0EditBox).getText();
    %tier[1] = %this._(tier1EditBox).getText();
    %tier[2] = %this._(tier2EditBox).getText();
    %tier[3] = %this._(tier3EditBox).getText();
    %tier[4] = %this._(tier4EditBox).getText();
    %nvPairs = strreplace(strreplace(%nvPairs, "\t", "="), "\n", ",");
    %nvPairsObj = strreplace(strreplace(%nvPairsObj, "\t", "="), "\n", ",");
    %thumbNail = %this._(thumbNailEditBox).getText();

    if (%textureFile $= "")
    {
        %textureFile = "$NULL";
    }
    if (%lightMount $= "")
    {
        %lightMount = "$NULL";
    }
    if (%light $= "")
    {
        %light = "$NULL";
    }
    if (%particleMount $= "")
    {
        %particleMount = "$NULL";
    }
    if (%particle $= "")
    {
        %particle = "$NULL";
    }
    if (%furniture == 0)
    {
        %type = "$NULL";
        %tier = "$NULL";
    }
    if (%nvPairs $= "")
    {
        %nvPairs = "$NULL";
    }
    if (%nvPairsObj $= "")
    {
        %nvPairsObj = "$NULL";
    }
    if (%CC $= "")
    {
        %CC = 0;
    }
    if (%PP $= "")
    {
        %PP = 0;
    }

    if (((%name !$= "")) && ((%shapeFile !$= "")))
    {
        %data = "name" TAB %name NL "className" TAB %className NL "dispName" TAB %dispName NL "description" TAB %desc NL "isFurniture" TAB %furniture NL "isWall" TAB %wall NL "planeSort" TAB %planeSort NL "furnType" TAB %type NL "tier" TAB %tier NL "shapeFile" TAB %shapeFile NL "textureFile" TAB %textureFile NL "shadowEnable" TAB %shadow NL "lightID" TAB %light NL "lightMount" TAB %lightMount NL "particleID" TAB %particle NL "particleMount" TAB %particleMount NL "nvPairs" TAB %nvPairs NL "nvPairsObj" TAB %nvPairsObj NL "thumbnail" TAB %thumbNail NL "CC" TAB %CC NL "PP" TAB %PP NL "sound" TAB %sound NL "showRightClick" TAB %canShowRightClick NL "storeFolderID" TAB %storeFolderID NL "tier0" TAB %tier[0] NL "tier1" TAB %tier[1] NL "tier2" TAB %tier[2] NL "tier3" TAB %tier[3] NL "tier4" TAB %tier[4] NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createInteractiveObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("interactive_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createInteractiveObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createInteractiveObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createInteractiveObjectTree.getParent(%parentItem);
                    %value = createInteractiveObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("interactive_objects", 0, %parentID, %data);
        }
        createInteractiveObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name and shape");
    }
}

function createInteractiveObjectDlg::onAddValue(%this, %stackName)
{
    %item = new GuiControl("") {
        Profile = "GuiBaseWindowProfile";
        Extent = "177 26";

        new GuiTextEditCtrl("") {
            internalName = "valueValue";
            position = "85 4";
            Extent = "65 18";
        };
        new GuiTextEditCtrl("") {
            internalName = "valueName";
            position = "5 4";
            Extent = "77 18";
        };
        new GuiBitmapButtonCtrl("") {
            position = "155 5";
            Extent = "16 16";
            bitmap = "onverse/data/live_assets/engine/ui/images/inspector_delete";
            Command = "$ThisControl.getParent().delete();";
        };
    };

    %this._(%stackName @ "ValueStack").add(%item);
    return %item;
}

function createInteractiveObjectDlg::onBrowseTextures(%this, %type)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetTexture", %this._(textureEditBox).getText());
}

function createInteractiveObjectDlg::onSetTexture(%this, %texFile)
{
    %this._(textureEditBox).setText(%texFile);
}

function createInteractiveObjectDlg::onBrowseShapes(%this, %type)
{
    getLoadFilename("*.dts", %this @ ".onSetShape", %this._(shapeEditBox).getText());
}

function createInteractiveObjectDlg::onSetShape(%this, %shapeFile)
{
    %this._(shapeEditBox).setText(%shapeFile);
}

function createInteractiveObjectDlg::onBrowseThumbnail(%this)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetThumbnail", %this._(thumbNailEditBox).getText());
}

function createInteractiveObjectDlg::onSetThumbnail(%this, %filename)
{
    %this._(thumbNailEditBox).setText(%filename);
}

function createInteractiveObjectDlg::onBrowseLights(%this)
{
    loadDBItemLight(%this @ ".onSetLight", %this._(lightEditBox).getText());
}

function createInteractiveObjectDlg::onSetLight(%this, %idText)
{
    %this._(lightEditBox).setText(%idText);
}

function createInteractiveObjectDlg::onBrowseParticles(%this)
{
    loadDBItemParticle(%this @ ".onSetParticle", %this._(particleEditBox).getText());
}

function createInteractiveObjectDlg::onSetParticle(%this, %idText)
{
    %this._(particleEditBox).setText(%idText);
}

function createInteractiveObjectDlg::onBrowseDesc(%this)
{
    getLoadFilename("*.txt", %this @ ".onSetDesc", %this._(descEditBox).getText());
}

function createInteractiveObjectDlg::onSetDesc(%this, %text)
{
    %this._(descEditBox).setText(%text);
}

function createInteractiveObjectDlg::onBrowseStoreFolders(%this)
{
    ShowStoreFoldersOpen(%this @ ".onSetStoreFolder", "");
}

function createInteractiveObjectDlg::onSetStoreFolder(%this, %unused)
{
    %this._(storeFolderEditBox).setText(StoreFolderTree.lastOpenID);
}

function createInteractiveObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createInteractiveObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[interactive_objects])
        {
            %this.receivedInteractiveObjectData(%data);
        }
    }
}

function createInteractiveObjectDlg::receivedInteractiveObjectData(%this, %data)
{
    %i = -1;
    %type = 0;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "dispName")
                {
                    %this._(dispNameEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "description")
                    {
                        %this._(descEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "className")
                        {
                            %this._(classNameEditBox).setText(getField(%record, 1));
                        }
                        else
                        {
                            if (getField(%record, 0) $= "showRightClick")
                            {
                                %this._(rightClickCheck).setValue(getField(%record, 1));
                            }
                            else
                            {
                                if (getField(%record, 0) $= "isFurniture")
                                {
                                    %this._(furnitureCheck).setValue(getField(%record, 1));
                                }
                                else
                                {
                                    if (getField(%record, 0) $= "isWall")
                                    {
                                        %this._(wallCheck).setValue(getField(%record, 1));
                                    }
                                    else
                                    {
                                        if (getField(%record, 0) $= "planeSort")
                                        {
                                            %this._(planeSortCheck).setValue(getField(%record, 1));
                                        }
                                        else
                                        {
                                            if (getField(%record, 0) $= "furnType")
                                            {
                                                %this._(typeList).setSelected(getField(%record, 1));
                                            }
                                            else
                                            {
                                                if (getField(%record, 0) $= "tier")
                                                {
                                                    %this._(tierList).setSelected(getField(%record, 1));
                                                }
                                                else
                                                {
                                                    if (getField(%record, 0) $= "shapeFile")
                                                    {
                                                        %this._(shapeEditBox).setText(getField(%record, 1));
                                                    }
                                                    else
                                                    {
                                                        if (getField(%record, 0) $= "textureFile")
                                                        {
                                                            %this._(textureEditBox).setText(getField(%record, 1));
                                                        }
                                                        else
                                                        {
                                                            if (getField(%record, 0) $= "shadowEnable")
                                                            {
                                                                %this._(shadowCheck).setValue(getField(%record, 1));
                                                            }
                                                            else
                                                            {
                                                                if (getField(%record, 0) $= "lightID")
                                                                {
                                                                    %this._(lightEditBox).setText(getField(%record, 1));
                                                                }
                                                                else
                                                                {
                                                                    if (getField(%record, 0) $= "lightMount")
                                                                    {
                                                                        %this._(lightMountEditBox).setText(getField(%record, 1));
                                                                    }
                                                                    else
                                                                    {
                                                                        if (getField(%record, 0) $= "particleID")
                                                                        {
                                                                            %this._(particleEditBox).setText(getField(%record, 1));
                                                                        }
                                                                        else
                                                                        {
                                                                            if (getField(%record, 0) $= "particleMount")
                                                                            {
                                                                                %this._(particleMountEditBox).setText(getField(%record, 1));
                                                                            }
                                                                            else
                                                                            {
                                                                                if (getField(%record, 0) $= "CC")
                                                                                {
                                                                                    %this._(ccEditBox).setText(getField(%record, 1));
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (getField(%record, 0) $= "PP")
                                                                                    {
                                                                                        %this._(ppEditBox).setText(getField(%record, 1));
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (getField(%record, 0) $= "sound")
                                                                                        {
                                                                                            %this._(soundEditBox).setText(getField(%record, 1));
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (getField(%record, 0) $= "nvPairs")
                                                                                            {
                                                                                                %nvPairs = getField(%record, 1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (getField(%record, 0) $= "nvPairsObj")
                                                                                                {
                                                                                                    %nvPairsObj = getField(%record, 1);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (getField(%record, 0) $= "thumbnail")
                                                                                                    {
                                                                                                        %this._(thumbNailEditBox).setText(getField(%record, 1));
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        if (getField(%record, 0) $= "storeFolderID")
                                                                                                        {
                                                                                                            %this._(storeFolderEditBox).setText(getField(%record, 1));
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if (getField(%record, 0) $= "tier0")
                                                                                                            {
                                                                                                                %this._(tier0EditBox).setText(getField(%record, 1));
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                if (getField(%record, 0) $= "tier1")
                                                                                                                {
                                                                                                                    %this._(tier1EditBox).setText(getField(%record, 1));
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    if (getField(%record, 0) $= "tier2")
                                                                                                                    {
                                                                                                                        %this._(tier2EditBox).setText(getField(%record, 1));
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        if (getField(%record, 0) $= "tier3")
                                                                                                                        {
                                                                                                                            %this._(tier3EditBox).setText(getField(%record, 1));
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            if (getField(%record, 0) $= "tier4")
                                                                                                                            {
                                                                                                                                %this._(tier4EditBox).setText(getField(%record, 1));
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %nvPairs = strreplace(strreplace(%nvPairs, "=", "\t"), ",", "\n");
    %nvPairsObj = strreplace(strreplace(%nvPairsObj, "=", "\t"), ",", "\n");
    %this.decodeNvPairsString("data", %nvPairs);
    %this.decodeNvPairsString("obj", %nvPairsObj);
    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
    %this._(AcquireButton).setVisible(%this._(furnitureCheck).getValue());
    %this._(copyButton).setVisible(1);
    %this._(idEditBox).setText(%this.curUpdateID);
    storeFoldersDlg.insertObj = %this.curUpdateID;
    storeFoldersDlg.insertType = 1;
}

function createInteractiveObjectDlg::onNew(%this)
{
    %this._(idEditBox).setText("");
    %this._(nameEditBox).setText("");
    %this._(dispNameEditBox).setText("");
    %this._(descEditBox).setText("");
    %this._(classNameEditBox).setText("");
    %this._(rightClickCheck).setValue(1);
    %this._(furnitureCheck).setValue(0);
    %this._(wallCheck).setValue(0);
    %this._(planeSortCheck).setValue(0);
    %this._(typeList).setSelected(0);
    %this._(tierList).setSelected(0);
    %this._(shapeEditBox).setText("");
    %this._(textureEditBox).setText("");
    %this._(shadowCheck).setValue(0);
    %this._(lightMountEditBox).setText("");
    %this._(lightEditBox).setText("");
    %this._(particleMountEditBox).setText("");
    %this._(particleEditBox).setText("");
    %this._(thumbNailEditBox).setText("");
    %this._(ccEditBox).setText(0);
    %this._(ppEditBox).setText(0);
    %this._(soundEditBox).setText("");
    %this._(storeFolderEditBox).setText(0);
    %this._(tier0EditBox).setText(0);
    %this._(tier1EditBox).setText(0);
    %this._(tier2EditBox).setText(0);
    %this._(tier3EditBox).setText(0);
    %this._(tier4EditBox).setText(0);
    %this.curUpdateID = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
    %this._(AcquireButton).setVisible(0);
    %this._(copyButton).setVisible(0);

    while (%this._(dataValueStack).getCount())
    {
        %this._(dataValueStack).getObject(0).delete();
    }

    while (%this._(objValueStack).getCount())
    {
        %this._(objValueStack).getObject(0).delete();
    }
}

function createInteractiveObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.curUpdateItem = 0;
    %this.addRootType($objects::tableID[interactive_objects], "Interactive Objects");
}

function createInteractiveObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("interactive_objects", %catID);
}

function createInteractiveObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createInteractiveObjectDlg.getResourceData("interactive_objects", getField(%value, 2), %item);
    }
}
