exec("./createTravelMountObjectDlg.gui");

function createTravelMountObjectDlg::onWake(%this)
{
    %this._(tierList).clear();
    %this._(tierList).add("Tier 0", 0);
    %this._(tierList).add("Tier 1", 1);
    %this._(tierList).add("Tier 2", 2);
    %this._(tierList).add("Tier 3", 3);
    %this._(tierList).add("Tier 4", 4);
    %this.onNew();
}

function createTravelMountObjectDlg::initalizeClassNames(%this)
{
    %this._(classNameList).clear();
    %this._(classNameList).add("BaseMountablePetData", 0);
    %this._(classNameList).add("BaseMountableCarData", 1);
}

function createTravelMountObjectDlg::buildNvPairsString(%this, %base)
{
    %stack = %this._(%base @ "ValueStack");
    %string = "";
    %count = %stack.getCount();

    for (%i=0; %i < %count; %i++)
    {
        %obj = %stack.getObject(%i);
        %name = %obj._("valueName").getText();
        %value = %obj._("valueValue").getText();
        %name = strreplace(strreplace(%name, ",", ""), " ", "");
        %value = strreplace(%value, ",", "");
        if (%name $= "")
        {
            continue;
        }
        %string = %string @ %name TAB %value NL "";
    }

    return %string;
}

function createTravelMountObjectDlg::decodeNvPairsString(%this, %base, %str)
{
    %stack = %this._(%base @ "ValueStack");
    while (%stack.getCount())
    {
        %stack.getObject(0).delete();
    }

    %count = getRecordCount(%str);
    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%str, %i);
        %name = getField(%record, 0);
        %value = getField(%record, 1);
        %obj = %this.onAddValue(%base);
        %obj._("valueName").setText(%name);
        %obj._("valueValue").setText(%value);
    }
}

function createTravelMountObjectDlg::onAcquireItem(%this)
{
    AC_AcquireItem("TravelMount", %this.curUpdateID);
}

function createTravelMountObjectDlg::onCopy(%this)
{
    %this.isUpdate = 0;
    %this.curUpdateID = 0;
    %this._(idEditBox).setText("");
    %this._(saveButton).setText("add");
    %this._(copyButton).setVisible(0);
    %this._(AcquireButton).setVisible(0);
}

function createTravelMountObjectDlg::onSave(%this)
{
    %name = %this._(nameEditBox).getText();
    %dispName = %this._(dispNameEditBox).getText();
    %desc = %this._(descEditBox).getText();
    %tier = %this._(tierList).getSelected();
    %shapeFile = %this._(shapeEditBox).getText();
    %textureFile = %this._(textureEditBox).getText();
    %thumbNail = %this._(thumbNailEditBox).getText();
    %CC = %this._(ccEditBox).getText();
    %PP = %this._(ppEditBox).getText();
    %className = %this._(classNameList).getText();
    %nvPairs = %this.buildNvPairsString(data);
    %nvPairs = strreplace(strreplace(%nvPairs, "\t", "="), "\n", ",");

    if (%textureFile $= "")
    {
        %textureFile = "$NULL";
    }
    if (%nvPairs $= "")
    {
        %nvPairs = "$NULL";
    }
    if (%CC $= "")
    {
        %CC = 0;
    }
    if (%PP $= "")
    {
        %PP = 0;
    }

    if ((%name !$= "") && (%className !$= "") && (%shapeFile !$= ""))
    {
        %data = "name" TAB %name NL "dispName" TAB %dispName NL "description" TAB %desc NL "tier" TAB %tier NL "shapeFile" TAB %shapeFile NL "textureFile" TAB %textureFile NL "thumbnail" TAB %thumbNail NL "CC" TAB %CC NL "PP" TAB %PP NL "className" TAB %className NL "nvPairs" TAB %nvPairs NL "";
        %parentItem = 0;
        if (%this.isUpdate)
        {
            %parentItem = createTravelMountObjectTree.getParent(%this.curUpdateItem);
            MasterServerConnection.requestUpdateResource("travelmount_objects", %this.curUpdateID, 0, %data);
        }
        else
        {
            %parentID = 0;
            %selected = createTravelMountObjectTree.getSelectedItemList();
            if (%selected)
            {
                %parentItem = getWord(%selected, 0);
                %value = createTravelMountObjectTree.getItemValue(%parentItem);
                if (!getField(%value, 0))
                {
                    %parentItem = createTravelMountObjectTree.getParent(%parentItem);
                    %value = createTravelMountObjectTree.getItemValue(%parentItem);
                }
                %parentID = getField(%value, 2);
            }
            MasterServerConnection.requestUpdateResource("travelmount_objects", 0, %parentID, %data);
        }
        createTravelMountObjectTree.onRefreshParent(%parentItem != 0 ? %parentItem : 1);
    }
    else
    {
        MessageBoxOK("Error", "Must have a name, classname and shape");
    }
}

function createTravelMountObjectDlg::onAddValue(%this, %stackName)
{
    %item = new GuiControl() {
       Profile = "GuiBaseWindowProfile";
       Extent = "188 26";

       new GuiTextEditCtrl() {
          internalName = "valueValue";
          position = "99 4";
          Extent = "65 18";
       };

       new GuiTextEditCtrl() {
          internalName = "valueName";
          position = "5 4";
          Extent = "89 18";
       };

       new GuiBitmapButtonCtrl() {
          position = "168 5";
          Extent = "16 16";
          bitmap = "onverse/data/live_assets/engine/ui/images/inspector_delete";
          Command = "$ThisControl.getParent().delete();";
       };
    };
    %this._(%stackName @ "ValueStack").add(%item);
    return %item;
}

function createTravelMountObjectDlg::onBrowseTextures(%this, %type)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetTexture", %this._(textureEditBox).getText());
}

function createTravelMountObjectDlg::onSetTexture(%this, %texFile)
{
    %this._(textureEditBox).setText(%texFile);
}

function createTravelMountObjectDlg::onBrowseShapes(%this, %type)
{
    getLoadFilename("*.dts", %this @ ".onSetShape", %this._(shapeEditBox).getText());
}

function createTravelMountObjectDlg::onSetShape(%this, %shapeFile)
{
    %this._(shapeEditBox).setText(%shapeFile);
}

function createTravelMountObjectDlg::onBrowseThumbnail(%this)
{
    getLoadFilename("*.png\t*.jpg", %this @ ".onSetThumbnail", %this._(thumbNailEditBox).getText());
}

function createTravelMountObjectDlg::onSetThumbnail(%this, %filename)
{
    %this._(thumbNailEditBox).setText(%filename);
}

function createTravelMountObjectDlg::onBrowseDesc(%this)
{
    getLoadFilename("*.txt", %this @ ".onSetDesc", %this._(descEditBox).getText());
}

function createTravelMountObjectDlg::onSetDesc(%this, %text)
{
    %this._(descEditBox).setText(%text);
}

function createTravelMountObjectDlg::getResourceData(%this, %typeName, %catID, %item)
{
    %this.curUpdateItem = %item;
    MasterServerConnection.requestResourceData(%this, %typeName, %catID);
}

function createTravelMountObjectDlg::onReceivedResourceData(%this, %unused, %type, %data)
{
    if ($objects::tableID[%type] == $objects::tableID[unknown])
    {
        error("Received bad datatype back onReceivedResourceData!.");
    }
    else
    {
        if ($objects::tableID[%type] == $objects::tableID[travelmount_objects])
        {
            %this.receivedTravelMountObjectData(%data);
        }
    }
}

function createTravelMountObjectDlg::receivedTravelMountObjectData(%this, %data)
{
    %i = -1;
    %type = 0;
    while ((%record = getRecord(%data, %i++)) !$= "")
    {
        if (getField(%record, 0) $= "id")
        {
            %this.curUpdateID = getField(%record, 1);
        }
        else
        {
            if (getField(%record, 0) $= "name")
            {
                %this._(nameEditBox).setText(getField(%record, 1));
            }
            else
            {
                if (getField(%record, 0) $= "dispName")
                {
                    %this._(dispNameEditBox).setText(getField(%record, 1));
                }
                else
                {
                    if (getField(%record, 0) $= "description")
                    {
                        %this._(descEditBox).setText(getField(%record, 1));
                    }
                    else
                    {
                        if (getField(%record, 0) $= "tier")
                        {
                            %this._(tierList).setSelected(getField(%record, 1));
                        }
                        else
                        {
                            if (getField(%record, 0) $= "shapeFile")
                            {
                                %this._(shapeEditBox).setText(getField(%record, 1));
                            }
                            else
                            {
                                if (getField(%record, 0) $= "textureFile")
                                {
                                    %this._(textureEditBox).setText(getField(%record, 1));
                                }
                                else
                                {
                                    if (getField(%record, 0) $= "thumbnail")
                                    {
                                        %this._(thumbNailEditBox).setText(getField(%record, 1));
                                    }
                                    else
                                    {
                                        if (getField(%record, 0) $= "CC")
                                        {
                                            %this._(ccEditBox).setText(getField(%record, 1));
                                        }
                                        else
                                        {
                                            if (getField(%record, 0) $= "PP")
                                            {
                                                %this._(ppEditBox).setText(getField(%record, 1));
                                            }
                                            else
                                            {
                                                if (getField(%record, 0) $= "className")
                                                {
                                                    %className = getField(%record, 1);
                                                }
                                                else
                                                {
                                                    if (getField(%record, 0) $= "nvPairs")
                                                    {
                                                        %nvPairs = getField(%record, 1);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %nvPairs = strreplace(strreplace(%nvPairs, "=", "\t"), ",", "\n");
    %this.decodeNvPairsString("data", %nvPairs);
    %this.initalizeClassNames();
    %classNameID = %this._(classNameList).findText(%className);

    if (%classNameID == -1)
    {
        %this._(classNameList).add(%className, 9999);
        %this._(classNameList).setSelected(9999);
    }
    else
    {
        %this._(classNameList).setSelected(%classNameID);
    }

    %this.isUpdate = 1;
    %this._(saveButton).setText("save");
    %this._(AcquireButton).setVisible(1);
    %this._(copyButton).setVisible(1);
    %this._(idEditBox).setText(%this.curUpdateID);
    storeFoldersDlg.insertObj = %this.curUpdateID;
    storeFoldersDlg.insertType = 5;
}

function createTravelMountObjectDlg::onNew(%this)
{
    %this.initalizeClassNames();
    %this._(idEditBox).setText("");
    %this._(nameEditBox).setText("");
    %this._(dispNameEditBox).setText("");
    %this._(descEditBox).setText("");
    %this._(tierList).setSelected(0);
    %this._(shapeEditBox).setText("");
    %this._(textureEditBox).setText("");
    %this._(thumbNailEditBox).setText("");
    %this._(ccEditBox).setText(0);
    %this._(ppEditBox).setText(0);
    %this._(classNameList).setSelected(0);
    %this.curUpdateID = 0;
    %this.isUpdate = 0;
    %this._(saveButton).setText("add");
    %this._(AcquireButton).setVisible(0);
    %this._(copyButton).setVisible(0);
    while (%this._(dataValueStack).getCount())
    {
        %this._(dataValueStack).getObject(0).delete();
    }
}

function createTravelMountObjectTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    %this.curUpdateItem = 0;
    %this.addRootType($objects::tableID[travelmount_objects], "TravelMount Objects");
}

function createTravelMountObjectTree::onDeleteObject(%this, %itemId)
{
    %value = %this.getItemValue(%itemId);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    MasterServerConnection.requestDeleteResource("travelmount_objects", %catID);
}

function createTravelMountObjectTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        createTravelMountObjectDlg.getResourceData("travelmount_objects", getField(%value, 2), %item);
    }
}
