new GuiControlProfile(GuiDebugTextArrayProfile : GuiBaseTextProfile) {
   fontType = "Lucida Console";
   fontSize = "12";
   fontColors[0] = "0 0 0";
   fontColors[1] = "0 0 255";
   fontColors[2] = "196 92 0";
   fontColors[3] = "0 128 128";
   fontColors[4] = "0 128 0";
   fontColors[5] = "163 21 21";
   fontColors[6] = "128 128 0";
   bitmap = "admin/ui/debugger/debugger";
   hasBitmapArray = "1";
};

new GuiControl(DbgCursorToolTip) {
   Profile = GuiModelessDialogProfile;
   Extent = Canvas.Extent;

   new GuiControl() {
      internalName = "Window";
      Profile = GuiToolTipProfile;

      new GuiTextCtrl() {
         internalName = "Text";
         Profile = GuiBaseTextProfile;
         position = "4 0";
      };
   };
};

exec("./DebuggerBreakConditionDlg.gui");
exec("./DebuggerConnectDlg.gui");
exec("./DebuggerEditWatchDlg.gui");
exec("./DebuggerFindDlg.gui");
exec("./DebuggerGui.gui");
exec("./DebuggerWatchDlg.gui");

new TCPObject(TCPDebugger) {
};

$DbgBreakId = 0;
$DbgWatchSeq = 1;

function TCPDebugger::onLine(%this, %line)
{
    %cmd = firstWord(%line);
    %rest = restWords(%line);
    if (%cmd $= "PASS")
    {
        %this.handlePass(%rest);
    }
    else
    {
        if (%cmd $= "COUT")
        {
            %this.handleLineOut(0, %rest);
        }
        else
        {
            if (%cmd $= "ECOUT")
            {
                %this.handleLineOut(firstWord(%rest), restWords(%rest));
            }
            else
            {
                if (%cmd $= "FILELISTOUT")
                {
                    %this.handleFileList(%rest);
                }
                else
                {
                    if (%cmd $= "LOAD")
                    {
                        %this.handleFileLoad(%rest);
                    }
                    else
                    {
                        if (%cmd $= "BREAKLISTOUT")
                        {
                            %this.handleBreakList(%rest);
                        }
                        else
                        {
                            if (%cmd $= "BREAK")
                            {
                                %this.handleBreak(%rest);
                            }
                            else
                            {
                                if (%cmd $= "BRKMOV")
                                {
                                    %this.handleBreakMove(getWord(%rest, 0), getWord(%rest, 1), getWord(%rest, 2));
                                }
                                else
                                {
                                    if (%cmd $= "BRKCLR")
                                    {
                                        %this.handleBreakMove(getWord(%rest, 0), getWord(%rest, 1), 0);
                                    }
                                    else
                                    {
                                        if (%cmd $= "RUNNING")
                                        {
                                            %this.handleRunning();
                                        }
                                        else
                                        {
                                            if (%cmd $= "EVALOUT")
                                            {
                                                %this.handleEvalOut(%rest);
                                            }
                                            else
                                            {
                                                if (%cmd $= "TABRSLV")
                                                {
                                                    %this.handleTabResolve(%rest);
                                                }
                                                else
                                                {
                                                    %this.handleError(%line);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function TCPDebugger::handlePass(%this, %message)
{
    if (%message $= "WrongPass")
    {
        DebuggerConsoleView.print("Disconnected - wrong password.");
        %this.disconnect();
    }
    else
    {
        if (%message $= "Connected.")
        {
            DebuggerConsoleView.print("Connected.");
            DebuggerStatus.setValue("INITIALIZED");
            %this.send("FILELIST\r\n");
            %count = DebuggerBreakPoints.rowCount();
            
            for (%i=0; %i < %count; %i++)
            {
                %id = DebuggerBreakPoints.getRowId(%i);
                %text = DebuggerBreakPoints.getRowTextById(%id);
                %line = getField(%text, 0);
                %file = getField(%text, 1);
                %expr = getField(%text, 2);
                %passct = getField(%text, 3);
                %clear = getField(%text, 4);
                TCPDebugger.send("BRKSET " @ %file @ " " @ %line @ " " @ %clear @ " " @ %passct @ " " @ %expr @ "\r\n");
            }
        }
    }
}

function TCPDebugger::handleLineOut(%this, %type, %line)
{
    %color = "";
    if (%type == 1)
    {
        %color = "\c1";
    }
    else
    {
        if (%type == 2)
        {
            %color = "\c2";
        }
    }
    DebuggerConsoleView.print(%color @ %line);
}

function TCPDebugger::handleFileList(%this, %line)
{
    $FileTreeNextID = 0;
    DebuggerFileTree.clear();
    while ((%file = getWord(%line, $FileTreeNextID)) !$= "")
    {
        $FileTreeNextID++;
        DbgAddfile(%file, $FileTreeNextID);
    }
}

function TCPDebugger::handleFileLoad(%this, %line)
{
    $FileTreeNextID++;
    DbgAddfile(%line, $FileTreeNextID);
}

function TCPDebugger::handleBreakList(%this, %line)
{
    DebuggerConsoleView.print("Depricated debug message: BREAKLISTOUT");
    return;
}

function TCPDebugger::handleBreak(%this, %line)
{
    DebuggerStatus.setValue("BREAK");
    if (!DebuggerGui.isAwake())
    {
        ShowDebugger(1);
    }

    DebuggerCallStack.currentStackFrame = 0;
    DbgRefreshWatches();
    DebuggerCallStack.clear();
    %file = getWord(%line, 0);
    %lineNumber = getWord(%line, 1);
    %funcName = getWord(%line, 2);
    DbgOpenFile(%file, %lineNumber, 1);
    %nextWord = 3;
    %rowId = 0;
    %id = 0;
    
    while (1)
    {
        DebuggerCallStack.setRowById(%id, %file @ "\t" @ %lineNumber @ "\t" @ %funcName);
        %id++;
        %file = getWord(%line, %nextWord);
        %lineNumber = getWord(%line, %nextWord + 1);
        %funcName = getWord(%line, %nextWord + 2);
        %nextWord+= 3;
        if (%file $= "")
        {
            break;
        }
    }
}

function TCPDebugger::handleBreakMove(%this, %file, %line, %newline)
{
    if (%file $= DebuggerFileView.getFileName())
    {
        DebuggerFileView.removeBreak(%line);
    }
    if (%newline == 0)
    {
        DebuggerBreakPoints.removeBreak(%file, %line);
    }
    else
    {
        if (%file $= DebuggerFileView.getFileName())
        {
            DebuggerFileView.setBreak(%newline);
        }
        DebuggerBreakPoints.moveBreak(%file, %line, %newline);
    }
}

function TCPDebugger::handleRunning(%this)
{
    DebuggerFileView.setCurrentLine(-1, 1);
    DebuggerCallStack.clear();
    DebuggerStatus.setValue("RUNNING...");
}

function TCPDebugger::handleEvalOut(%this, %line)
{
    %id = firstWord(%line);
    %value = restWords(%line);

    if (%id < 0)
    {
        DbgCursorToolTip._(Text).setText(DbgCursorToolTip.expr SPC "=" SPC %value);
        DbgCursorToolTip._(Window).Extent = getWord(DbgCursorToolTip._(Text).Extent, 0) + 8 SPC getWord(DbgCursorToolTip._(Text).Extent, 1);
        Canvas.pushDialog(DbgCursorToolTip);
    }
    else
    {
        %row = DebuggerWatchView.getRowTextById(%id);
        if (%row $= "")
        {
            return;
        }
        %expr = getField(%row, 0);
        DebuggerWatchView.setRowById(%id, %expr @ "\t" @ %value);
    }
}

function TCPDebugger::handleTabResolve(%this, %line)
{
    %was = firstWord(%line);
    %now = restWords(%line);
    if (DbgConsoleEntry.getText() $= %was)
    {
        DbgConsoleEntry.setText(%now);
    }
}

function TCPDebugger::handleError(%this, %line)
{
    DebuggerConsoleView.print("ERROR - bogus message: " @ %line);
}

function TCPDebugger::onDNSResolve(%this)
{
}

function TCPDebugger::onConnecting(%this)
{
}

function TCPDebugger::onConnected(%this)
{
    %this.send("VER 3\r\n");
    %this.send(%this.password @ "\r\n");
}

function TCPDebugger::onConnectFailed(%this)
{
    MessageBoxOK("Error", "Could not connect debug session.");
}

function TCPDebugger::onDisconnect(%this)
{
    if (isObject(DebuggerStatus))
    {
        DebuggerStatus.setValue("NOT CONNECTED");
    }

    if (isObject(DebuggerConsoleView))
    {
        DebuggerConsoleView.clear();
    }

    if (isObject(DebuggerFileView))
    {
        DebuggerFileView.setCurrentLine(-1, 1);
    }
}

function DebuggerConsoleView::print(%this, %line)
{
    %row = %this.addRow(0, %line);
    %this.scrollVisible(%row);
}

function DebuggerCallStack::onSelect(%this)
{
    %id = %this.getSelectedId();
    if (%id == -1)
    {
        return;
    }
    %text = %this.getRowTextById(%id);
    %file = getField(%text, 0);
    %line = getField(%text, 1);
    DebuggerCallStack.currentStackFrame = %id;
    DbgRefreshWatches();
    DbgOpenFile(%file, %line, 1);
}

function DebuggerBreakPoints::addBreak(%this, %file, %line, %clear, %passct, %expr)
{
    %textLine = %line @ "\t" @ %file @ "\t" @ %expr @ "\t" @ %passct @ "\t" @ %clear;
    %selId = %this.getSelectedId();
    %selText = %this.getRowTextById(%selId);
    if ((getField(%selText, 0) $= %line) && (getField(%selText, 1) $= %file))
    {
      %this.setRowById(%selId, %textLine);
    }
    else
    {
      %this.addRow($DbgBreakId, %textLine);
      $DbgBreakId++;
    }
}

function DebuggerBreakPoints::removeBreak(%this, %file, %line)
{
    for (%i=0; %i < %this.rowCount(); %i++)
    {
        %id = %this.getRowId(%i);
        %text = %this.getRowTextById(%id);
        if ((getField(%text, 0) $= %line) && (getField(%text, 1) $= %file))
        {
            %this.removeRowById(%id);
            return;
        }
    }
}

function DebuggerBreakPoints::moveBreak(%this, %file, %line, %newline)
{
    for (%i=0; %i < %this.rowCount(); %i++)
    {
        %id = %this.getRowId(%i);
        %text = %this.getRowTextById(%id);
        if ((getField(%text, 0) $= %line) && (getField(%text, 1) $= %file))
        {
            %textLine = %newline @ "\t" @ %file @ "\t" @ getField(%text, 2) @ "\t" @ getField(%text, 3) @ "\t" @ getField(%text, 4);
            %this.setRowById(%id, %textLine);
            return;
        }
    }
}

function DebuggerBreakPoints::clearBreaks(%this)
{
    while (%this.rowCount())
    {
        %id = %this.getRowId(0);
        %text = %this.getRowTextById(%id);
        %file = getField(%text, 1);
        %line = getField(%text, 0);
        DbgRemoveBreakPoint(%file, %line);
    }
}

function DebuggerBreakPoints::onSelect(%this)
{
    %id = %this.getSelectedId();
    if (%id == -1)
    {
        return;
    }
    %text = %this.getRowTextById(%id);
    %line = getField(%text, 0);
    %file = getField(%text, 1);
    DbgOpenFile(%file, %line, 0);
}

function DebuggerFileView::onRemoveBreakPoint(%this, %line)
{
    %file = %this.getFileName();
    DbgRemoveBreakPoint(%file, %line);
}

function DebuggerFileView::onSetBreakPoint(%this, %line)
{
    %file = %this.getFileName();
    DbgSetBreakPoint(%file, %line, 0, 0, 1);
}

function DebuggerFileTree::onWake(%this)
{
    %this.buildIconTable();
}

function DebuggerFileTree::onSelect(%this, %item)
{
    %file = getWord(%this.getItemValue(%item), 0);
    if (%file !$= "")
    {
        DbgOpenFile(getWord(%this.getItemValue(%item), 0), 0, 0);
    }
}

function DbgAddfile(%filename, %id)
{
    %newName = strreplace(%filename, "/", "\t");
    %parent = 0;

    %count = getFieldCount(%newName) - 1;
    for (%i=0; %i < %count; %i++)
    {
        %text = getField(%newName, %i);
        %id = 0;
        if (%parent)
        {
            %id = DebuggerFileTree.getChild(%parent);
        }
        else
        {
            %id = DebuggerFileTree.getFirstRootItem();
        }
        while (%id != 0)
        {
            if (!(DebuggerFileTree.getItemText(%id) $= %text))
            {
                %id = DebuggerFileTree.getNextSibling(%id);
            }
            else
            {
                break;
            }
        }
        if (%id == 0)
        {
            %parent = DebuggerFileTree.insertItem(%parent, %text, 0, "", 2, 1);
        }
        else
        {
            %parent = %id;
        }
    }
    DebuggerFileTree.insertItem(%parent, getField(%newName, %i), %filename TAB %id);
}

function DbgWatchDialogAdd()
{
    %expr = WatchDialogExpression.getValue();
    if (!(%expr $= ""))
    {
        DebuggerWatchView.setRowById($DbgWatchSeq, %expr @ "\t(unknown)");
        TCPDebugger.send("EVAL " @ $DbgWatchSeq SPC DebuggerCallStack.currentStackFrame SPC %expr @ "\r\n");
        $DbgWatchSeq++;
    }
    Canvas.popDialog(DebuggerWatchDlg);
}

function DbgWatchDialogEdit()
{
    %newValue = EditWatchDialogValue.getValue();

    %id = DebuggerWatchView.getSelectedId();
    if (%id >= 0)
    {
        %row = DebuggerWatchView.getRowTextById(%id);
        %expr = getField(%row, 0);
        if (%newValue $= "")
        {
            %assignment = %expr @ " = \"\"";
        }
        else
        {
            %assignment = %expr @ " = " @ %newValue;
        }
        TCPDebugger.send("EVAL " @ %id SPC DebuggerCallStack.currentStackFrame SPC %assignment @ "\r\n");
    }

    Canvas.popDialog(DebuggerEditWatchDlg);
}

function DbgSetCursorWatch(%expr, %x, %y)
{
    DbgCursorToolTip.expr = %expr;
    if (DbgCursorToolTip.expr $= "")
    {
        Canvas.popDialog(DbgCursorToolTip);
    }
    else
    {
        DbgCursorToolTip._(Window).position = %x SPC %y;
        TCPDebugger.send("EVAL -1" SPC DebuggerCallStack.currentStackFrame SPC DbgCursorToolTip.expr @ "\r\n");
    }
}

function DbgConnect()
{
    %address = DebuggerConnectAddress.getValue();
    %port = DebuggerConnectPort.getValue();
    %password = DebuggerConnectPassword.getValue();
    TCPDebugger.disconnect();

    if ((%address !$= "") && (%port !$= "") && (%password !$= ""))
    {
        TCPDebugger.password = %password;
        TCPDebugger.schedule(32, connect, %address @ ":" @ %port);
    }

    Canvas.popDialog(DebuggerConnectDlg);
}

function DbgBreakConditionSet()
{
    %condition = BreakCondition.getValue();
    %passct = BreakPassCount.getValue();
    %clear = BreakClear.getValue();

    if (%condition $= "")
    {
        %condition = "true";
    }
    if (%passct $= "")
    {
        %passct = "0";
    }
    if (%clear $= "")
    {
        %clear = "false";
    }

    %id = DebuggerBreakPoints.getSelectedId();
    if (%id != -1)
    {
        %bkp = DebuggerBreakPoints.getRowTextById(%id);
        DbgSetBreakPoint(getField(%bkp, 1), getField(%bkp, 0), %clear, %passct, %condition);
    }
    Canvas.popDialog(DebuggerBreakConditionDlg);
}

function DbgOpenFile(%file, %line, %selectLine)
{
    if (!(%file $= ""))
    {
        if (DebuggerFileView.open(%file))
        {
            DebuggerFileView.setCurrentLine(%line, %selectLine);
            
            for (%i = 0; %i < DebuggerBreakPoints.rowCount(); %i++)
            {
                %breakText = DebuggerBreakPoints.getRowText(%i);
                %breakLine = getField(%breakText, 0);
                %breakFile = getField(%breakText, 1);
                if (%breakFile $= DebuggerFileView.getFileName())
                {
                    DebuggerFileView.setBreak(%breakLine);
                }
            }
        }
    }
}

function DbgFileViewFind()
{
    %searchString = DebuggerFindStringText.getValue();
    DebuggerFileView.findString(%searchString);
    Canvas.popDialog(DebuggerFindDlg);
}

function DbgSetBreakPoint(%file, %line, %clear, %passct, %expr)
{
    if (!%clear)
    {
        if (%file $= DebuggerFileView.getFileName())
        {
            DebuggerFileView.setBreak(%line);
        }
    }

    DebuggerBreakPoints.addBreak(%file, %line, %clear, %passct, %expr);
    TCPDebugger.send("BRKSET " @ %file @ " " @ %line @ " " @ %clear @ " " @ %passct @ " " @ %expr @ "\r\n");
}

function DbgRemoveBreakPoint(%file, %line)
{
    if (%file $= DebuggerFileView.getFileName())
    {
        DebuggerFileView.removeBreak(%line);
    }

    TCPDebugger.send("BRKCLR " @ %file @ " " @ %line @ "\r\n");
    DebuggerBreakPoints.removeBreak(%file, %line);
}

function DbgDeleteSelectedBreak()
{
    %selectedBreak = DebuggerBreakPoints.getSelectedId();
    %rowNum = DebuggerBreakPoints.getRowNumById(%selectedWatch);

    if (%rowNum >= 0)
    {
        %breakText = DebuggerBreakPoints.getRowText(%rowNum);
        %breakLine = getField(%breakText, 0);
        %breakFile = getField(%breakText, 1);
        DbgRemoveBreakPoint(%breakFile, %breakLine);
    }
}

function DbgConsoleEntryReturn()
{
    %msg = DbgConsoleEntry.getValue();
    if (!(%msg $= ""))
    {
        DebuggerConsoleView.print("%" @ %msg);
        if (DebuggerStatus.getValue() $= "NOT CONNECTED")
        {
            DebuggerConsoleView.print("*** Not connected.");
        }
        else
        {
            if (DebuggerStatus.getValue() $= "BREAK")
            {
                DebuggerConsoleView.print("*** Target is in BREAK mode.");
            }
            else
            {
                TCPDebugger.send("CEVAL " @ %msg @ "\r\n");
            }
        }
    }
    DbgConsoleEntry.setValue("");
}

function DbgConsoleEntry::onTabComplete(%this, %unused)
{
    if (!(%this.getText() $= ""))
    {
        TCPDebugger.send("TABCMPLT " @ %this.getText() @ "\r\n");
    }
}

function DbgConsolePrint(%status)
{
    DebuggerConsoleView.print(%status);
}

function DbgDeleteSelectedWatch()
{
    %selectedWatch = DebuggerWatchView.getSelectedId();
    %rowNum = DebuggerWatchView.getRowNumById(%selectedWatch);
    DebuggerWatchView.removeRow(%rowNum);
}

function DbgRefreshWatches()
{
    for (%i=0; %i < DebuggerWatchView.rowCount(); %i++)
    {
        %id = DebuggerWatchView.getRowId(%i);
        %row = DebuggerWatchView.getRowTextById(%id);
        %expr = getField(%row, 0);
        TCPDebugger.send("EVAL " @ %id SPC DebuggerCallStack.currentStackFrame SPC %expr @ "\r\n");
    }
}

function dbgStepIn()
{
    TCPDebugger.send("STEPIN\r\n");
}

function dbgStepOut()
{
    TCPDebugger.send("STEPOUT\r\n");
}

function dbgStepOver()
{
    TCPDebugger.send("STEPOVER\r\n");
}

function dbgContinue()
{
    TCPDebugger.send("CONTINUE\r\n");
}

function DebuggerGui::onWake(%this)
{
    DebuggerCallStack.currentStackFrame = 0;
    %position = $pref::Debugger::position;
    if (%position $= "")
    {
        %position = DebuggerWindow.position;
    }

    %extent = $pref::Debugger::extent;
    if ((getWord(%extent, 0) < getWord(DebuggerWindow.MinExtent, 0)) || (getWord(%extent, 1) < getWord(DebuggerWindow.MinExtent, 1)))
    {
        %extent = DebuggerWindow.Extent;
    }

    DebuggerWindow.resize(getWord(%position, 0), getWord(%position, 1), getWord(%extent, 0), getWord(%extent, 1));
    DebuggerGui.schedule(0, defaultFrames);
}

function DebuggerGui::defaultFrames(%this)
{
    if (!($pref::Debugger::YFrame $= ""))
    {
        %this._(YFrame).rows = $pref::Debugger::YFrame;
        %this._(YFrame).updateSizes();
    }

    if (!($pref::Debugger::XFrame $= ""))
    {
        %this._(XFrame).columns = $pref::Debugger::XFrame;
        %this._(XFrame).updateSizes();
    }
}

function DebuggerGui::onSleep()
{
    $pref::Debugger::position = DebuggerWindow.position;
    $pref::Debugger::extent = DebuggerWindow.Extent;
    $pref::Debugger::YFrame = DebuggerWindow._(YFrame).rows;
    $pref::Debugger::XFrame = DebuggerWindow._(XFrame).columns;
}

function ShowDebugger(%make)
{
    if (%make)
    {
        if (DebuggerGui.isAwake())
        {
            Canvas.popDialog(DebuggerGui);
        }
        else
        {
            DebuggerConsoleView.setActive(0);
            Canvas.pushDialog(DebuggerGui);
        }
    }
}

function DebugServer(%port)
{
    $pref::DBGConnectAddress = $currentConnectAddress;
    $pref::DBGConnectPort = %port + 16385;
    if (!DebuggerGui.isAwake())
    {
        DebuggerConsoleView.setActive(0);
        Canvas.pushDialog(DebuggerGui);
    }
    Canvas.pushDialog(DebuggerConnectDlg, 80);
}
