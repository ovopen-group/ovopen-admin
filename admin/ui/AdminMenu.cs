exec("./AdminMenu.gui");
exec("./AdminStatus.gui");
$AdminGUILayer = 3;
$AdminState::Layer = 2;
$AdminState::Show = 0;

function ShowAdminStatus()
{
    if ($AdminState::Show)
    {
        Canvas.pushDialog(AdminStatus, $AdminState::Layer);
    }
    else
    {
        Canvas.popLayer($AdminState::Layer);
    }
}

function AdminStatus::onWake(%this)
{
    %this._(Version).setText(getVersionString());
}

function ToggleAdminMenu(%make)
{
    if (%make)
    {
        if (AdminMenu.isAwake())
        {
            Canvas.popLayer($AdminGUILayer);
        }
        else
        {
            Canvas.pushDialog(AdminMenu, $AdminGUILayer);
        }
    }
}

function AdminMenu::onWake(%this)
{
    %menuBar = %this._(MenuBar);
    %menuBar.clearMenus();
    %menuId = 0;
    %menuName = "File";
    %menuBar.addMenu(%menuName, %menuId++);
    %itemId = 1;
    %this.addMenuItem(%menuName, "Options", %itemId++, "Canvas.pushDialog(optionsDlg);");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Exit", %itemId++, "Quit();");
    %menuName = "Master Server";
    %menuBar.addMenu(%menuName, %menuId++);
    %itemId = 1;
    %this.addMenuItem(%menuName, "Connect", %itemId++, "Canvas.pushDialog(masterConnectDlg);");
    %this.addMenuItem(%menuName, "Disconnect", %itemId++, "MasterServerConnection.disconnect();");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Friend List", %itemId++, "Canvas.pushDialog(FriendListUI);");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Management", %itemId++, "Canvas.pushDialog(AdminServerManagement);");
    %menuName = "Location Server";
    %menuBar.addMenu(%menuName, %menuId++);
    %itemId = 1;
    %this.addMenuItem(%menuName, "Directory", %itemId++, "ShowDirectory();");
    %this.addMenuItem(%menuName, "Go to Location", %itemId++, "Canvas.pushDialog(goToLocationDlg);");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Go to URL", %itemId++, "MessageBoxEntryOk(URL,\"Enter Onverse URL\",ResolveURL,\"ONVERSE://location/\");");
    %menuName = "Admin";
    %menuBar.addMenu(%menuName, %menuId++);
    %itemId = 1;
    %this.addMenuItem(%menuName, "Category Manager", %itemId++, "Canvas.pushDialog(CategoryManagerGUI);");
    %this.addMenuItem(%menuName, "Create Clothing Object", %itemId++, "Canvas.pushDialog(createClothingObjectDlg);");
    %this.addMenuItem(%menuName, "Create Interactive Object", %itemId++, "Canvas.pushDialog(createInteractiveObjectDlg);");
    %this.addMenuItem(%menuName, "Create Interior Object", %itemId++, "Canvas.pushDialog(createInteriorObjectDlg);");
    %this.addMenuItem(%menuName, "Create Light Object", %itemId++, "Canvas.pushDialog(createLightObjectDlg);");
    %this.addMenuItem(%menuName, "Create Particle Object", %itemId++, "Canvas.pushDialog(createParticleObjectDlg);");
    %this.addMenuItem(%menuName, "Create Scene Object", %itemId++, "Canvas.pushDialog(createSceneObjectDlg);");
    %this.addMenuItem(%menuName, "Create Tool Object", %itemId++, "Canvas.pushDialog(createToolObjectDlg);");
    %this.addMenuItem(%menuName, "Create Pet Object", %itemId++, "Canvas.pushDialog(createPetObjectDlg);");
    %this.addMenuItem(%menuName, "Create TravelMount Object", %itemId++, "Canvas.pushDialog(createTravelMountObjectDlg);");
    %this.addMenuItem(%menuName, "Create NPC Object", %itemId++, "Canvas.pushDialog(createNPCObjectDlg);");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Store Folder Manager", %itemId++, "ShowStoreFoldersManager();");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Show Status", %itemId++, "AdminMenu.ToggleAdminStatus(" @ %itemId @ ");");
    %menuBar.setMenuItemChecked("Admin", %itemId, $AdminState::Show);
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Configure Avatar", %itemId++, "Canvas.pushDialog(hackConfigureClothing);");
    %menuName = "Debug";
    %menuBar.addMenu(%menuName, %menuId++);
    %itemId = 1;
    %this.addMenuItem(%menuName, "Console", %itemId++, "toggleConsole(true);", "~");
    %this.addMenuItem(%menuName, "Debugger", %itemId++, "ShowDebugger(true);", "F12");
    %this.addMenuItem(%menuName, "Global Inspector", %itemId++, "Tree();");
    %this.addMenuItem(%menuName, "Net Graph", %itemId++, "NetGraph::toggleNetGraph();", "N");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %metricMenu = %itemId++;
    %this.addMenuItem(%menuName, "Metrics", %metricMenu, "");
    %menuBar.setMenuItemSubmenuState(%menuName, %metricMenu, 1);
    %this.addSubmenuItem(%menuName, %metricMenu, "Off", %itemId++, "metrics();");
    %menuBar.addSubmenuItem(%menuName, %metricMenu, "-", 0);
    %this.addSubmenuItem(%menuName, %metricMenu, "Audio", %itemId++, "metrics(audio);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Debug", %itemId++, "metrics(debug);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Interior", %itemId++, "metrics(interior);");
    %this.addSubmenuItem(%menuName, %metricMenu, "FPS", %itemId++, "metrics(fps);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Time", %itemId++, "metrics(time);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Terrain", %itemId++, "metrics(terrain);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Texture", %itemId++, "metrics(texture);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Video", %itemId++, "metrics(video);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Vehicle", %itemId++, "metrics(vehicle);");
    %this.addSubmenuItem(%menuName, %metricMenu, "Water", %itemId++, "metrics(water);");
    %renderingMenu = %itemId++;
    %this.addMenuItem(%menuName, "Rendering", %renderingMenu, "");
    %menuBar.setMenuItemSubmenuState(%menuName, %renderingMenu, 1);
    %this.addSubmenuItem(%menuName, %renderingMenu, "Cycle Render Mode", %itemId++, "cycleDebugRenderMode(true);", "F9");
    %menuBar.addSubmenuItem(%menuName, %renderingMenu, "-", 0);
    %this.addSubmenuItem(%menuName, %renderingMenu, "NormalRender", %itemId++, "setInteriorRenderMode(0);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "NormalRenderLines", %itemId++, "setInteriorRenderMode(1);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowDetail", %itemId++, "setInteriorRenderMode(2);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowAmbiguous", %itemId++, "setInteriorRenderMode(3);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowOrphan", %itemId++, "setInteriorRenderMode(4);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowLightmaps", %itemId++, "setInteriorRenderMode(5);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowTexturesOnly", %itemId++, "setInteriorRenderMode(6);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowPortalZones", %itemId++, "setInteriorRenderMode(7);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowOutsideVisible", %itemId++, "setInteriorRenderMode(8);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowCollisionFans", %itemId++, "setInteriorRenderMode(9);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowStrips", %itemId++, "setInteriorRenderMode(10);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowNullSurfaces", %itemId++, "setInteriorRenderMode(11);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowLargeTextures", %itemId++, "setInteriorRenderMode(12);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowHullSurfaces", %itemId++, "setInteriorRenderMode(13);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowVehicleHullSurfaces", %itemId++, "setInteriorRenderMode(14);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowVertexColors", %itemId++, "setInteriorRenderMode(15);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowDetailLevel", %itemId++, "setInteriorRenderMode(16);", "");
    %this.addSubmenuItem(%menuName, %renderingMenu, "ShowVertexNormals", %itemId++, "setInteriorRenderMode(17);", "");
    %menuBar.addMenuItem(%menuName, "-", 0);
    %this.addMenuItem(%menuName, "Interior Lightmaps", %itemId++, "Canvas.pushDialog(inspectInteriorLightmapsDlg);");
}

function AdminMenu::ToggleAdminStatus(%this, %itemId)
{
    %menuBar = %this._(MenuBar);
    $AdminState::Show = !$AdminState::Show;
    ShowAdminStatus();
    %menuBar.setMenuItemChecked("Admin", %itemId, $AdminState::Show);
}

function AdminMenu::addMenuItem(%this, %menuName, %itemName, %itemId, %command, %accel)
{
    %menuBar = %this._(MenuBar);
    %menuBar.addMenuItem(%menuName, %itemName, %itemId, %accel);
    %menuBar.scriptCommand[%menuName,%itemId] = %command;
}

function AdminMenu::addSubmenuItem(%this, %menuName, %menuItemName, %itemName, %itemId, %command, %accel)
{
    %menuBar = %this._(MenuBar);
    %menuBar.addSubmenuItem(%menuName, %menuItemName, %itemName, %itemId, %accel);
    %menuBar.scriptCommand[%menuName,%itemId] = %command;
}

function AdminMenuBar::onMenuItemSelect(%this, %menuId, %menu, %itemId, %item)
{
    if (!(%this.scriptCommand[%menu,%itemId] $= ""))
    {
        eval(%this.scriptCommand[%menu,%itemId]);
    }
    else
    {
        error("No script command defined for menu " @ %menu @ " item " @ %item);
    }
}
