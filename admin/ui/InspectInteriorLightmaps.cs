exec("./InspectInteriorLightmapsDlg.gui");

function inspectInteriorLightmapsDlg::onWake(%this)
{
    %this._(InteriorTree).clear();
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        %stackPos = 0;
        %count[0] = LocationGameConnection.getCount();
        %pos[0] = 0;
        %obj[0] = LocationGameConnection;

        while ( %stackPos >= 0 )
        {
            for ( ; %pos[%stackPos] < %count[%stackPos];  )
            {
                %obj = %obj[%stackPos].getObject(%pos[%stackPos]);
                %pos[%stackPos]++;
                %className = %obj.getClassName();

                if ((%className $= "ClientEditInteriorInstance") || (%className $= "InteriorInstance"))
                {
                    inspectInteriorLightmapsDlg._(textureView).insertIntoTree(%this._(InteriorTree), 0, %obj.getId());
                }
                else if (%className $= "SimGroup")
                {
                    %stackPos++;
                    %count[%stackPos] = %obj.getCount();
                    %pos[%stackPos] = 0;
                    %obj[%stackPos] = %obj;
                    continue;
                }
            }

            // xtra
            %stackPos--;
        }
    }
    else
    {
        schedule(0, %this, "MessageBoxOk", "Error", "Must be connected to an instance to see interiors.");
    }
}

function InteriorLightmapInspectorTree::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (getWord(%value, 0) == 1)
    {
        %type = getWord(%value, 1);
        %interior = getWord(%value, 2);
        %detail = getWord(%value, 3);
        %idx = getWord(%value, 4);
        if (%type == 2)
        {
            inspectInteriorLightmapsDlg._(textureView).inspectInteriorLightmap(%interior, %detail, %idx);
        }
        if (%type == 1)
        {
            inspectInteriorLightmapsDlg._(textureView).inspectInteriorMaterialmap(%interior, %detail, %idx);
        }
    }
}
