exec("./LoadDBItem.gui");
$loadDBItem::type = "";

function loadDBItemParticle(%callback, %currentItem)
{
    loadDBItem("particle", %callback, %currentItem);
}

function loadDBItemLight(%callback, %currentItem)
{
    loadDBItem("light", %callback, %currentItem);
}

function loadDBItem(%type, %callback, %currentItem)
{
    $loadDBItem::type = %type;
    $loadDBItem::callback = %callback;
    Canvas.pushDialog(LoadDBItemDlg);
}

function LoadDBItemList::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();

    if ($loadDBItem::type $= "particle")
    {
        %this.addRootType($objects::tableID[particle_objects], "Particles");
    }
    else
    {
        if ($loadDBItem::type $= "light")
        {
            %this.addRootType($objects::tableID[light_objects], "Lights");
        }
        else
        {
            Canvas.popDialog(LoadDBItemDlg);
        }
    }
}

function LoadDBItemDlg::DoOpenDBCallback(%this)
{
    %itemId = 0;
    %selected = LoadDBItemList.getSelectedItemList();
    if (!(%selected $= ""))
    {
        %item = getWord(%selected, 0);
        %value = LoadDBItemList.getItemValue(%item);
        if (!getField(%value, 0))
        {
            %itemId = getField(%value, 2);
        }
    }
    eval($loadDBItem::callback @ "(" @ %itemId @ ");");
    Canvas.popDialog(LoadDBItemDlg);
}

function LoadDBItemList::onMoveItem(%this, %item, %unused)
{
}

function LoadDBItemList::onDeleteSelection(%this)
{
}

function LoadDBItemList::requestMoveCatObject(%this, %unused, %unused, %item, %unused)
{
}

function LoadDBItemList::requestAddCategory(%this, %unused, %unused, %unused)
{
}

function LoadDBItemList::requestDeleteCategory(%this, %unused)
{
}

function LoadDBItemList::requestRenameCatObject(%this, %unused, %unused, %unused, %unused)
{
}
