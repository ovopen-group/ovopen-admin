exec("./CatManager.gui");
$CategoryLoadingText = "Loading...";

function CategoryTreeView::onWake(%this)
{
    if (%this.destroyTreeOnSleep)
    {
        schedule(1, 0, MessageBoxOK, "Warn", "Bad Configuration, this tree will not work correctly.");
    }

    if (!MasterServerConnection.isConnected())
    {
        schedule(1, 0, MessageBoxOK, "Error", "Not connected, this dialog will not work correctly.");
    }

    %this.clearSelection();
}

function CategoryTreeView::onExpand(%this, %item)
{
    %this.loadChildren(%item);
}

function CategoryTreeView::addRootType(%this, %typeID, %name)
{
    if (%name $= "")
    {
        %name = $objects::reverseName[%typeID];
    }

    return %this.addFolder(0, %name, %typeID, 0);
}

function CategoryTreeView::addFolder(%this, %parent, %name, %typeID, %catID)
{
    %parentID = %this.insertItem(%parent, %name, 1 TAB %typeID TAB %catID, "", 2, 1);
    %this.insertItem(%parentID, $CategoryLoadingText);
    return %parentID;
}

function CategoryTreeView::GetIcon(%this, %typeID)
{
    %icon = "";
    if (%typeID == $objects::tableID[scene_objects])
    {
        %icon = "ClientEditTSStatic";
    }
    else
    {
        if (%typeID == $objects::tableID[interior_objects])
        {
            %icon = "ClientEditInteriorInstance";
        }
    }

    return %icon;
}

function CategoryTreeView::addObject(%this, %parentID, %name, %typeID, %id)
{
    %icon = %this.GetIcon(%typeID);
    %this.insertItem(%parentID, %name, 0 TAB %typeID TAB %id, %icon);
}

function CategoryTreeView::loadChildren(%this, %folderID)
{
    %child = %this.getChild(%folderID);
    if (%child && !strcmp($CategoryLoadingText, %this.getItemText(%child)))
    {
        %value = %this.getItemValue(%folderID);
        if (getField(%value, 0))
        {
            %type = getField(%value, 1);
            %catID = getField(%value, 2);
            if (%type != 0)
            {
                %this.requestCatChildren($objects::reverseName[%type], %catID, %folderID);
            }
        }
    }
}

function CategoryTreeView::addChildren(%this, %folderID, %folders, %objects)
{
    if (0 == %folderID)
    {
        error("Received NULL folder ID on cat children.");
        return;
    }

    %this.removeAllChildren(%folderID);
    %this.appendChildren(%folderID, %folders, %objects);
}

function CategoryTreeView::appendChildren(%this, %folderID, %folders, %objects)
{
    if (0 == %folderID)
    {
        error("Received NULL folder ID on cat children.Append");
        return;
    }

    %value = %this.getItemValue(%folderID);
    if (!getField(%value, 0))
    {
        error("Folder response was not on a folder.");
        return;
    }

    %type = getField(%value, 1);
    while ((%rec = getRecord(%folders, 0)) !$= "")
    {
        %folders = removeRecord(%folders, 0);
        %id = getField(%rec, 0);
        %name = getField(%rec, 1);
        %this.addFolder(%folderID, %name, %type, %id);
    }

    while ((%rec = getRecord(%objects, 0)) !$= "")
    {
        %objects = removeRecord(%objects, 0);
        %id = getField(%rec, 0);
        %name = getField(%rec, 1);
        %this.addObject(%folderID, %name, %type, %id);
    }
}

function CategoryTreeView::onMoveItem(%this, %item, %newFolder)
{
    %value = %this.getItemValue(%newFolder);
    if (!getField(%value, 0))
    {
        return;
    }

    %newFolderType = getField(%value, 1);
    %newFolderID = getField(%value, 2);
    %value = %this.getItemValue(%item);
    %moveItemIsFolder = getField(%value, 0);
    %moveItemType = getField(%value, 1);
    %moveItemID = getField(%value, 2);

    if (%moveItemType != %newFolderType)
    {
        return;
    }

    %this.removeItem(%item);
    %this.requestMoveCatObject($objects::reverseName[%moveItemType], %moveItemIsFolder, %moveItemID, %newFolderID);
    return;
}

function CategoryTreeView::onRefreshParent(%this, %parent)
{
    %value = %this.getItemValue(%parent);
    if (!getField(%value, 0))
    {
        return;
    }

    if (!%this.isExpanded(%parent))
    {
        %this.expandItem(%parent, 1);
        return;
    }

    %this.clearSelection();
    %newFolderType = getField(%value, 1);
    %newFolderID = getField(%value, 2);
    %this.removeAllChildren(%parent);
    %this.insertItem(%parent, $CategoryLoadingText);
    %this.requestCatChildren($objects::reverseName[%newFolderType], %newFolderID, %parent);
}

function CategoryTreeView::onDeleteObject(%this, %unused)
{
}

function CategoryTreeView::onDeleteFolder(%this, %folderID)
{
    if (%this.getChild(%folderID))
    {
        MessageBoxOK("Error", "Can only delete visted empty folders.", "");
        return;
    }

    %value = %this.getItemValue(%folderID);
    %type = getField(%value, 1);
    %catID = getField(%value, 2);
    if (%catID == 0)
    {
        MessageBoxOK("Error", "Cannot delete root folders.", "");
        return;
    }

    %this.requestDeleteCategory(%catID);
}

function CategoryTreeView::onRenameSelection(%this)
{
    %selected = %this.getSelectedItemList();
    if (%selected $= "")
    {
        return;
    }

    %item = getWord(%selected, 0);
    if (%item != 0)
    {
        %curText = %this.getItemText(%item);
        $CategoryTreeViewLastCall = %this;
        MessageBoxEntryOk("Rename", "New Name:", "CategoryTreeViewRenameCategory", %curText);
        %value = %this.getItemValue(%item);
        %isFolder = getField(%value, 0);
        %type = getField(%value, 1);
        %catID = getField(%value, 2);
    }
}

function CategoryTreeView::renameSelection(%this, %newName)
{
    %selected = %this.getSelectedItemList();
    if (%selected $= "")
    {
        return;
    }

    %item = getWord(%selected, 0);
    if (%item != 0)
    {
        %value = %this.getItemValue(%item);
        %isFolder = getField(%value, 0);
        %type = getField(%value, 1);
        %catID = getField(%value, 2);
        %this.requestRenameCatObject($objects::reverseName[%type], %isFolder, %newName, %catID);
        %parentItem = %this.getParent(%item);
        %value = %this.getItemValue(%parentItem);
        %catID = getField(%value, 2);
        %this.requestCatChildren($objects::reverseName[%type], %catID, %parentItem);
    }
}

function CategoryTreeView::onDeleteSelection(%this)
{
    %selected = %this.getSelectedItemList();
    if (%selected $= "")
    {
        return;
    }

    %parentRefresh = "";
    %it = -1;
    while ((%item = getWord(%selected, %it++)) != 0)
    {
        %value = %this.getItemValue(%item);
        if (getField(%value, 0))
        {
            %this.onDeleteFolder(%item);
        }
        else
        {
            %this.onDeleteObject(%item);
        }
        %parent = %this.getParent(%item);
        %inList = 0;
        %i = -1;
        while ((%curitem = getWord(%parentRefresh, %i++)) != 0)
        {
            if (%curitem == %parent)
            {
                %inList = 1;
                break;
            }
        }
        if (!%inList)
        {
            if (%parentRefresh $= "")
            {
                %parentRefresh = %parent;
            }
            else
            {
                %parentRefresh = %parentRefresh SPC %parent;
            }
        }
    }

    %it = -1;
    while ((%item = getWord(%parentRefresh, %it++) )!= 0)
    {
        %test = %item;
        %inList = 0;
        while (!%inList && ((%test = %this.getParent(%test)) != 0))
        {
            %i = -1;
            while (!%inList && ((%curitem = getWord(%parentRefresh, %i++)) != 0))
            {
                if (%curitem == %test)
                {
                    %parentRefresh = removeWord(%parentRefresh, %it);
                    %it--;
                    %inList = 1;
                }
            }
        }

        if (!%inList)
        {
            %value = %this.getItemValue(%item);
            %type = getField(%value, 1);
            %catID = getField(%value, 2);
            %this.requestCatChildren($objects::reverseName[%type], %catID, %item);
        }
    }
}


function CategoryTreeView::addCategory(%this)
{
    %selected = %this.getSelectedItemList();
    if ((%this.getNextSibling(1) != 0) && (%selected $= ""))
    {
        MessageBoxOK("Message", "A category must be selected to add into.", "");
        return;
    }

    $CategoryTreeViewLastCall = %this;
    MessageBoxEntryOk("Add Category", "Category Name:", "CategoryTreeViewAddCategoryName", "");
}

function CategoryTreeView::addNewCategory(%this, %name)
{
    %selected = %this.getSelectedItemList();
    if (!(%selected $= ""))
    {
        %parentItem = getWord(%selected, 0);
    }
    else
    {
        %parentItem = 1;
    }

    %value = %this.getItemValue(%parentItem);
    if (!getField(%value, 0))
    {
        %parentItem = %this.getParent(%parentItem);
        %value = %this.getItemValue(%parentItem);
    }

    %typeID = getField(%value, 1);
    %parentID = getField(%value, 2);
    %this.requestAddCategory($objects::reverseName[%typeID], %name, %parentID);
    if (!%this.isExpanded(%parentItem))
    {
        %this.expandItem(%parentItem, 1);
    }
    else
    {
        %this.requestCatChildren($objects::reverseName[%typeID], %parentID, %parentItem);
    }
}

function CategoryTreeViewAddCategoryName(%val)
{
    $CategoryTreeViewLastCall.addNewCategory(%val);
}

function CategoryTreeViewRenameCategory(%val)
{
    $CategoryTreeViewLastCall.renameSelection(%val);
}

function CategoryTreeView::requestCatChildren(%this, %typeName, %catID, %folderID)
{
    MasterServerConnection.requestCatChildren(%typeName, %catID, %folderID, %this);
}

function CategoryTreeView::requestMoveCatObject(%this, %typeName, %isFolder, %item, %newParent)
{
    MasterServerConnection.requestMoveCatObject(%typeName, %isFolder, %item, %newParent);
}

function CategoryTreeView::requestAddCategory(%this, %typeName, %name, %parentID)
{
    MasterServerConnection.requestAddCategory(%typeName, %name, %parentID);
}

function CategoryTreeView::requestDeleteCategory(%this, %catID)
{
    MasterServerConnection.requestDeleteCategory(%catID);
}

function CategoryTreeView::requestRenameCatObject(%this, %typeName, %isFolder, %name, %catID)
{
    MasterServerConnection.requestRenameCatObject(%typeName, %isFolder, %name, %catID);
}

function CategoryTreeView::onCatChildren(%this, %id, %folderID, %folders, %objects)
{
    if (%this != %id)
    {
        error("received onCatChildren for another tree control.");
        return;
    }

    %this.addChildren(%folderID, %folders, %objects);
}

function CategoryManagerGUI::onWake(%this)
{
    %this.AdjustSize();
}

function CategoryManagerGUI::AdjustSize(%this)
{
    %position = $pref::CatManager::position;
    if (%position $= "")
    {
        %position = CategoryManagerGUI._(CatManWindow).position;
    }

    %extent = $pref::CatManager::extent;
    if ((getWord(%extent, 0) < getWord(CategoryManagerGUI._(CatManWindow).MinExtent, 0)) || (getWord(%extent, 1) < getWord(CategoryManagerGUI._(CatManWindow).MinExtent, 1)))
    {
        %extent = CategoryManagerGUI._(CatManWindow).Extent;
    }

    CategoryManagerGUI._(CatManWindow).resize(getWord(%position, 0), getWord(%position, 1), getWord(%extent, 0), getWord(%extent, 1));
}

function CategoryManagerGUI::onSleep(%this)
{
    $pref::CatManager::position = CategoryManagerGUI._(CatManWindow).position;
    $pref::CatManager::extent = CategoryManagerGUI._(CatManWindow).Extent;
}

function CatManagerTree::onWake(%this)
{
    Parent::onWake(%this);
    %this.clear();
    %this.buildIconTable();
    for (%i=$objects::startnumTables; %i < $objects::numTables; %i++)
    {
        %name = "Type (" @ %i @ ") Root - " @ $objects::reverseName[%i];
        %this.addRootType(%i, %name);
    }
}

