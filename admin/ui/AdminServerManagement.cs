exec("./AdminServerManagement.gui");

new GuiContextMenu(adminServerManageContextMenu) {
   Profile = "GuiBaseMenuBarProfile";
   position = "0 0";
   Extent = "640 480";
};

function CopyLocationRequest(%from)
{
    $LocationRequestLastFrom = %from;
    MessageBoxEntryOk("Name", "What should the new location\'s name be?", CopyLocationResponse, "");
}

function CopyLocationResponse(%to)
{
    AC_CopyLocation($LocationRequestLastFrom, %to);
}

function CreateInstanceRequest(%from)
{
    $LocationRequestLastFrom = %from;
    MessageBoxEntryOk("Name", "What should the new instance\'s name be?", CreateInstanceResponse, "");
}

function CreateInstanceResponse(%to)
{
    AC_CreateInstance($LocationRequestLastFrom, %to);
}

function GoToInstance(%location, %instance, %add)
{
    %url = "ONVERSE://location/" @ %location @ "/" @ %instance @ "/";
    if (!%add)
    {
        resolveURL(%url);
    }
    else
    {
        if ($pref::customURL::numURLS $= "")
        {
            $pref::customURL::numURLS = 0;
        }
        $pref::customURL::URL[$pref::customURL::numURLS] = %location TAB %url;
        $pref::customURL::numURLS++;
    }
}

function adminServerManageContextMenu::Initialize(%this)
{
    if (!%this.initialized)
    {
        %menuId = 0;
        %itemId = 0;
        %this.addMenu("locations", %menuId++);
        %this.addMenuItem("locations", "New", %itemId++, "CopyLocationRequest(\"default\");");
        %this.addMenuItem("locations", "Copy", %itemId++, "CopyLocationRequest($tempVar[0]);");
        %this.addMenuItem("locations", "Delete", %itemId++, "AC_DeleteLocation($tempVar[0]);");
        %this.addMenuItem("locations", "-", 0);
        %this.addMenuItem("locations", "Add Instance", %itemId++, "CreateInstanceRequest($tempVar[0]);");
        %this.addMenuItem("locations", "Close all", %itemId++, "");
        %itemId = 0;
        %this.addMenu("instances_open", %menuId++);
        %this.addMenuItem("instances_open", "Close", %itemId++, "AC_CloseInstance($tempVar[0],$tempVar[1]);");
        %this.addMenuItem("instances_open", "Empty", %itemId++, "AC_KickInstanceUser(0,$tempVar[0],$tempVar[1]);");
        %this.addMenuItem("instances_open", "Debug", %itemId++, "AdminServerManagement.debugInstance($tempVar[0],$tempVar[1]);");
        %this.addMenuItem("instances_open", "-", 0);
        %this.addMenuItem("instances_open", "Go To", %itemId++, "GoToInstance($tempVar[0],$tempVar[1],0);");
        %this.addMenuItem("instances_open", "Add To List", %itemId++, "GoToInstance($tempVar[0],$tempVar[1],1);");
        %itemId = 0;
        %this.addMenu("instances_closed", %menuId++);
        %this.addMenuItem("instances_closed", "Start", %itemId++, "AC_StartInstance($tempVar[0],$tempVar[1]);");
        %this.addMenuItem("instances_closed", "-", 0);
        %this.addMenuItem("instances_closed", "Go To", %itemId++, "GoToInstance($tempVar[0],$tempVar[1],0);");
        %this.addMenuItem("instances_closed", "Add To List", %itemId++, "GoToInstance($tempVar[0],$tempVar[1],1);");
        %itemId = 0;
        %this.addMenu("instances_launching", %menuId++);
        %this.addMenuItem("instances_launching", "Debug", %itemId++, "AdminServerManagement.debugInstance($tempVar[0],$tempVar[1]);");
        %this.addMenuItem("instances_launching", "-", 0);
        %this.addMenuItem("instances_launching", "Go To", %itemId++, "GoToInstance($tempVar[0],$tempVar[1],0);");
        %this.addMenuItem("instances_launching", "Add To List", %itemId++, "GoToInstance($tempVar[0],$tempVar[1],1);");
    }
    %this.initialized = 1;
}

function adminServerManageContextMenu::showContextMenu(%this, %menu, %location, %instance, %layer)
{
    %this.Initialize();
    %this.var[0] = %location;
    %this.var[1] = %instance;
    Parent::showContextMenu(%this, %menu, %layer);
}

function RPC_ServerManagement::onNoePermission()
{
    MessageBoxOK("Error", "You do not have permission to run that command");
}

function RPC_ServerManagement::onReturnInvalidCommand()
{
    MessageBoxOK("Error", "Invalid command");
}

function RPC_ServerManagement::onReturnNumberArguments()
{
    MessageBoxOK("Error", "Incorrect number of arguments for command");
}

function RPC_ServerManagement::onMasterInfoList(%this, %data)
{
    AdminServerManagement.onMasterInfoList(%data);
}

function RPC_ServerManagement::onLocationList(%this, %data)
{
    AdminServerManagement.onLocationList(%data);
}

function RPC_ServerManagement::onInstanceList(%this, %data)
{
    AdminServerManagement.onInstanceList(%data);
}

function RPC_ServerManagement::onInstanceInfo(%this, %data)
{
    AdminServerManagement.onInstanceInfo(%data);
}

function RPC_ServerManagement::onUsersList(%this, %data)
{
    AdminServerManagement.onUsersList(%data);
}

function RPC_ServerManagement::onUsersFriends(%this, %data)
{
    AdminServerManagement.onUsersFriends(%this.userItem, %data);
}

function RPC_ServerManagement::onUserInfo(%this, %data)
{
    if (%data)
    {
        AdminServerManagement.onUserInfo(%data);
    }
}

function RPC_ServerManagement::onLauncherList(%this, %data)
{
    AdminServerManagement.onLauncherList(%data);
}

function RPC_ServerManagement::onLauncherInfo(%this, %data)
{
    AdminServerManagement.onLauncherInfo(%data);
}

function AdminServerManagement::onWake(%this)
{
    if (MasterServerConnection.isConnected())
    {
        %this.refreshAll();
    }
    else
    {
        schedule(0, 0, "MessageBoxOK", "Connect", "Master server must be connected to use this dialog.", "");
    }
}

function AdminServerManagement::refreshAll(%this)
{
    %this.refreshMasterList();
    %this.refreshLauncherList();
    %this.refreshLocationList();
    %this.refreshUsersList();
}

function AdminServerManagement::crashMaster(%this)
{
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_CRASH_MASTER;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::crashCurrent(%this)
{
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_CRASH_CURRENT;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::updateServers(%this)
{
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_UPDATE_SERVERS;
    %rpcObject.fireRPC(MasterServerConnection);
}

$Permission::AUTH_LEVEL_USER = 1;
$Permission::AUTH_LEVEL_GUIDE = 2;
$Permission::AUTH_LEVEL_SUPER_GUIDE = 4;
$Permission::AUTH_LEVEL_COMM_MANAGER = 8;
$Permission::AUTH_LEVEL_ADMIN = 16;
$Permission::AUTH_LEVEL_SUPER_ADMIN = 32;
$Permission::AUTH_LEVEL_NETWORK_ADMIN = 64;
$Permission::AUTH_LEVEL_ROOT_ADMIN = 128;
$Permission::AUTH_LEVEL_DEVELOPER = 0x80000000;
$Location::Options::Cars = 1;
$Location::Options::VipCars = 2;

function AdminServerManagement::refreshMasterList(%this)
{
    %this._(MasterServers)._(MasterList).clear();
    %this._(MasterServers)._(MasterList).addRow(0, "Receiving data...");
    %this._(MasterServers)._(MasterID).setText("");
    %this._(MasterServers)._(isMaster).setValue(0);
    %this._(MasterServers)._(permission).clear();
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_DEVELOPER", $Permission::AUTH_LEVEL_DEVELOPER);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_ROOT_ADMIN", $Permission::AUTH_LEVEL_ROOT_ADMIN);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_NETWORK_ADMIN", $Permission::AUTH_LEVEL_NETWORK_ADMIN);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_SUPER_ADMIN", $Permission::AUTH_LEVEL_SUPER_ADMIN);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_ADMIN", $Permission::AUTH_LEVEL_ADMIN);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_COMM_MANAGER", $Permission::AUTH_LEVEL_COMM_MANAGER);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_SUPER_GUIDE", $Permission::AUTH_LEVEL_SUPER_GUIDE);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_GUIDE", $Permission::AUTH_LEVEL_GUIDE);
    %this._(MasterServers)._(permission).add("AUTH_LEVEL_USER", $Permission::AUTH_LEVEL_USER);
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_MASTER_INFO;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::onMasterInfoList(%this, %data)
{
    %this._(MasterServers)._(MasterList).clear();
    %count = getRecordCount(%data);
    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%data, %i);
        %masterID = getField(%record, 0);
        %this._(MasterServers)._(MasterList).addRow(%masterID, "Master Server " @ %masterID TAB %record);
    }
}

function AdminServerManagement::onMasterClick(%this, %list)
{
    %id = %list.getSelectedId();
    %data = %list.getRowTextById(%id);
    %this._(MasterServers)._(MasterID).setText(%id);
    %this._(MasterServers)._(isMaster).setValue(getField(%data, 2));
    %this._(MasterServers)._(permission).setSelected(getField(%data, 3));
}

function AdminServerManagement::onApplyPermissions(%this)
{
    %permission = %this._(MasterServers)._(permission).getSelected();
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_SET_PERMISSION;
    %rpcObject.arguments = %permission;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::refreshLauncherList(%this)
{
    %this._(launchers)._(LauncherList).clear();
    %this._(launchers)._(LauncherList).addRow(0, "Receiving data...");
    %this._(launchers)._(runInstList).clear();
    %this._(launchers)._(pendInstList).clear();
    %this._(launchers)._(id).setText("");
    %this._(launchers)._(usercount).setText("");
    %this._(launchers)._(hasports).setText("");
    %this._(launchers)._(Load).setText("");
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_LAUNCHER_LIST;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::onLauncherList(%this, %data)
{
    %this._(launchers)._(LauncherList).clear();
    %count = getRecordCount(%data);
    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%data, %i);
        %this._(launchers)._(LauncherList).addRow(%record, "Launcher " @ %record);
    }
}

function AdminServerManagement::onLauncherInfo(%this, %data)
{
    %record = getRecord(%data, 0);
    %id = getField(%record, 0);
    %usercount = getField(%record, 1);
    %hasports = getField(%record, 2);
    %load = getField(%record, 3);
    %this._(launchers)._(runInstList).clear();
    %this._(launchers)._(pendInstList).clear();
    %this._(launchers)._(id).setText(%id);
    %this._(launchers)._(usercount).setText(%usercount);
    %this._(launchers)._(hasports).setText(%hasports);
    %this._(launchers)._(Load).setText(%load);

    %curRecord = 1;
    while ((%record = getRecord(%data, %curRecord)) !$= "")
    {
        %loc_id = getField(%record, 0);
        %loc_name = getField(%record, 1);
        %inst_id = getField(%record, 2);
        %inst_name = getField(%record, 3);
        %this._(launchers)._(pendInstList).addRow(%inst_id, %loc_id SPC %loc_name SPC "-" SPC %inst_id SPC %inst_name);
        %curRecord++;
    }

    %curRecord++;
    while ((%record = getRecord(%data, %curRecord)) !$= "")
    {
        %loc_id = getField(%record, 0);
        %loc_name = getField(%record, 1);
        %inst_id = getField(%record, 2);
        %inst_name = getField(%record, 3);
        %this._(launchers)._(runInstList).addRow(%inst_id, %loc_id SPC %loc_name SPC "-" SPC %inst_id SPC %inst_name);
        %curRecord++;
    }
}

function AdminServerManagement::onLauncherClick(%this, %list)
{
    %id = %list.getSelectedId();
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_LAUNCHER_INFO;
    %rpcObject.arguments = %id;
    %rpcObject.fireRPC(MasterServerConnection);
}

$LocationLoadingText = "Loading instances...";

function AdminServerManagement::refreshLocationList(%this)
{
    %this._(Locations)._(LocationList).clear();
    %this._(Locations)._(CurrentUsersList).clear();
    %this._(Locations)._(PendingUsersList).clear();
    %this._(Locations)._(ResidentsList).clear();
    %this._(Locations)._(locationID).setText("");
    %this._(Locations)._(instanceID).setText("");
    %this._(Locations)._(InstanceName).setText("");
    %this._(Locations)._(Port).setText("");
    %this._(Locations)._(State).setText("");
    %this._(Locations)._(permission).clear();
    %this._(Locations)._(permission).add("AUTH_LEVEL_DEVELOPER", $Permission::AUTH_LEVEL_DEVELOPER);
    %this._(Locations)._(permission).add("AUTH_LEVEL_ROOT_ADMIN", $Permission::AUTH_LEVEL_ROOT_ADMIN);
    %this._(Locations)._(permission).add("AUTH_LEVEL_NETWORK_ADMIN", $Permission::AUTH_LEVEL_NETWORK_ADMIN);
    %this._(Locations)._(permission).add("AUTH_LEVEL_SUPER_ADMIN", $Permission::AUTH_LEVEL_SUPER_ADMIN);
    %this._(Locations)._(permission).add("AUTH_LEVEL_ADMIN", $Permission::AUTH_LEVEL_ADMIN);
    %this._(Locations)._(permission).add("AUTH_LEVEL_SUPER_GUIDE", $Permission::AUTH_LEVEL_SUPER_GUIDE);
    %this._(Locations)._(permission).add("AUTH_LEVEL_GUIDE", $Permission::AUTH_LEVEL_GUIDE);
    %this._(Locations)._(permission).add("AUTH_LEVEL_HELPER", $Permission::AUTH_LEVEL_HELPER);
    %this._(Locations)._(permission).add("AUTH_LEVEL_USER", $Permission::AUTH_LEVEL_USER);
    %this._(Locations)._(allowCars).setValue(0);
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_LOCATION_LIST;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::onLocationList(%this, %data)
{
    %this._(Locations)._(LocationList).clear();
    %count = getRecordCount(%data);

    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%data, %i);
        %id = getField(%record, 0);
        %name = getField(%record, 1);
        %this._(Locations)._(LocationList).addLocation(%id, %name);
    }
}

function AdminServerManagement::onInstanceList(%this, %data)
{
    %count = getRecordCount(%data);
    %locationID = getRecord(%data, 0);
    %parentID = %this._(Locations)._(LocationList).findLocation(%locationID);
    %this._(Locations)._(LocationList).removeAllChildren(%parentID);

    for (%i=1; %i < %count; %i++)
    {
        %record = getRecord(%data, %i);
        %id = getField(%record, 0);
        %name = getField(%record, 1);
        %state = getField(%record, 2);
        %this._(Locations)._(LocationList).addInstance(%parentID, %locationID, %id, %name, %state);
    }
}

function AdminServerManagement::onInstanceInfo(%this, %data)
{
    %record = getRecord(%data, 0);
    %locationID = getField(%record, 0);
    %instanceID = getField(%record, 1);
    %locname = getField(%record, 2);
    %instName = getField(%record, 3);
    %port = getField(%record, 4);
    %state = getField(%record, 5);
    %secLevel = getField(%record, 6);
    %options = getField(%record, 7);
    if (%state == 0)
    {
        %state = "STATE_CLOSED";
    }
    else
    {
        if (%state == 1)
        {
            %state = "STATE_OPEN";
        }
        else
        {
            if (%state == 2)
            {
                %state = "STATE_LAUNCHING";
            }
        }
    }

    %this._(Locations)._(locationID).setText(%locationID);
    %this._(Locations)._(instanceID).setText(%instanceID);
    %this._(Locations)._(LocationName).setText(%locname);
    %this._(Locations)._(InstanceName).setText(%instName);
    %this._(Locations)._(Port).setText(%port);
    %this._(Locations)._(State).setText(%state);
    %this._(Locations)._(permission).setSelected(%secLevel);
    %this._(Locations)._(allowCars).setValue(%options & $Location::Options::Cars);
    %this._(Locations)._(vipCars).setValue(%options & $Location::Options::VipCars);

    for (%curRecord = 1; (%record = getRecord(%data, %curRecord)) !$= ""; %curRecord++)
    {
        %id = getField(%record, 0);
        %name = getField(%record, 1);
        %this._(Locations)._(CurrentUsersList).addRow(%id, %name);
    }

    for ( %curRecord++; (%record = getRecord(%data, %curRecord)) !$= ""; %curRecord++)
    {
        %id = getField(%record, 0);
        %name = getField(%record, 1);
        %this._(Locations)._(ResidentsList).addRow(%id, %name);
    }

    for ( %curRecord++; (%record = getRecord(%data, %curRecord)) !$= ""; %curRecord++)
    {
        %clusterid = getField(%record, 0);
        %name = getField(%record, 1);
        %this._(Locations)._(PendingUsersList).addRow(0, %name TAB %clusterid);
    }
}

function AdminLocationList::onWake(%this)
{
    %this.clear();
    %this.buildIconTable();
}

function AdminLocationList::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (getField(%value, 0))
    {
        %locationID = getField(%value, 1);
        %instanceID = getField(%value, 2);
        AdminServerManagement._(Locations)._(CurrentUsersList).clear();
        AdminServerManagement._(Locations)._(PendingUsersList).clear();
        AdminServerManagement._(Locations)._(ResidentsList).clear();
        %rpcObject = new RPC_ServerManagement() {
        };
        %rpcObject.Command = COMMAND_GET_INSTANCE_INFO;
        %rpcObject.arguments = %locationID;
        %rpcObject.arguments = %instanceID;
        %rpcObject.fireRPC(MasterServerConnection);
    }
}

function AdminLocationList::onExpand(%this, %item)
{
    %value = %this.getItemValue(%item);
    %this.removeAllChildren(%item);
    %this.insertItem(%item, $LocationLoadingText);
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_INSTANCE_LIST;
    %rpcObject.arguments = getField(%value, 1);
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminLocationList::addLocation(%this, %id, %name)
{
    %parentID = %this.insertItem(0, %id SPC %name, 0 TAB %id, "", 2, 1);
    %this.insertItem(%parentID, $LocationLoadingText);
    return %parentID;
}

function AdminLocationList::findLocation(%this, %locationID)
{
    for (%child = %this.getFirstRootItem(); %child; %child = %this.getNextSibling(%child) )
    {
        %value = %this.getItemValue(%child);
        if (getField(%value, 1) == %locationID)
        {
            return %child;
        }
    }
    return 0;
}

function AdminLocationList::addInstance(%this, %parentID, %locationID, %id, %name, %state)
{
    %icon = 12;
    if (%state == 1)
    {
        %icon = 16;
    }
    else
    {
        if (%state == 2)
        {
            %icon = 20;
        }
    }
    return %this.insertItem(%parentID, %id SPC %name, 1 TAB %locationID TAB %id TAB %state, "", %icon, %icon);
}

function AdminLocationList::onRightMouseDown(%this, %item, %unused)
{
    %this.selectItem(%item);
    %value = %this.getItemValue(%item);
    %locationID = getField(%value, 1);
    if (!getField(%value, 0))
    {
        %text = %this.getItemText(%item);
        adminServerManageContextMenu.showContextMenu("locations", getWord(%text, 1), "");
    }
    else
    {
        %location = getWord(%this.getItemText(%this.getParent(%item)), 1);
        %instance = getWords(%this.getItemText(%item), 1);
        if (getField(%this.getItemValue(%item), 3) == 1)
        {
            adminServerManageContextMenu.showContextMenu("instances_open", %location, %instance);
        }
        else
        {
            if (getField(%this.getItemValue(%item), 3) == 2)
            {
                adminServerManageContextMenu.showContextMenu("instances_launching", %location, %instance);
            }
            else
            {
                adminServerManageContextMenu.showContextMenu("instances_closed", %location, %instance);
            }
        }
    }
}

function AdminServerManagement::debugInstance(%this, %location, %instance)
{
    if (%this._(Locations)._(LocationName).getText() $= %location)
    {
        if (%this._(Locations)._(InstanceName).getText() $= %instance)
        {
            if (%this._(Locations)._(Port).getText() != 0)
            {
                DebugServer(%this._(Locations)._(Port).getText());
            }
        }
    }
}

function AdminServerManagement::onApplyLocPermissions(%this)
{
    %options = 0;
    %location = %this._(Locations)._(LocationName).getText();
    %permission = %this._(Locations)._(permission).getSelected();
    %options |= %this._(Locations)._(allowCars).getValue() ? $Location::Options::Cars : 0;
    %options |= %this._(Locations)._(vipCars).getValue() ? $Location::Options::VipCars : 0;
    if (((%location !$= "")) && (%permission != 0))
    {
        %rpcObject = new RPC_ServerManagement() {
        };
        %rpcObject.Command = COMMAND_SET_LOC_PERMISSION;
        %rpcObject.arguments = %location;
        %rpcObject.arguments = %permission;
        %rpcObject.arguments = %options;
        %rpcObject.fireRPC(MasterServerConnection);
    }
}

$UserLoadingText = "Loading Friends...";

function AdminServerManagement::refreshUsersList(%this)
{
    %this._(Users)._(UsersList).clear();
    %this._(Users)._(userID).setText("");
    %this._(Users)._(ClusterID).setText("");
    %this._(Users)._(username).setText("");
    %this._(Users)._(owningSlave).setText("");
    %this._(Users)._(authLevel).setText("");
    %this._(Users)._(currentLoc).setText("");
    %this._(Users)._(currentInst).setText("");

    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_USER_LIST;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminServerManagement::onUsersList(%this, %data)
{
    %this._(Users)._(UsersList).clear();
    
    %count = getRecordCount(%data);
    for (%i=0; %i < %count; %i++)
    {
        %record = getRecord(%data, %i);
        %id = getField(%record, 0);
        %name = getField(%record, 1);
        %clusterid = getField(%record, 2);
        %this._(Users)._(UsersList).addUser(%id, %name, %clusterid);
    }

    %this._(Users)._(totalUsers).setText(%count);
}

function AdminServerManagement::onUsersFriends(%this, %parentID, %data)
{
    %value = %this._(Users)._(UsersList).getItemValue(%parentID);
    if (%value)
    {
        %this._(Users)._(UsersList).removeAllChildren(%parentID);
        %count = getRecordCount(%data);
        for (%i=0; %i < %count; %i++)
        {
            %record = getRecord(%data, %i);
            %id = getField(%record, 0);
            %name = getField(%record, 1);
            %state = getField(%record, 2);
            %this._(Users)._(UsersList).AddFriend(%parentID, %id, %name, %state);
        }
    }
}

function AdminServerManagement::onUserInfo(%this, %data)
{
    %userID = getField(%data, 0);
    %clusterid = getField(%data, 1);
    %name = getField(%data, 2);
    %owner = getField(%data, 3);
    %authlevel = getField(%data, 4);
    %currentLocation = getField(%data, 5);
    %currentInstance = getField(%data, 6);
    %this._(Users)._(userID).setText(%userID);
    %this._(Users)._(ClusterID).setText(%clusterid);
    %this._(Users)._(username).setText(%name);
    %this._(Users)._(owningSlave).setText(%owner);
    %this._(Users)._(authLevel).setText(%authlevel);
    %this._(Users)._(currentLoc).setText(%currentLocation);
    %this._(Users)._(currentInst).setText(%currentInstance);
}

function AdminUsersList::onWake(%this)
{
    %this.clear();
    %this.buildIconTable();
}

function AdminUsersList::onExpand(%this, %item)
{
    %value = %this.getItemValue(%item);
    %this.removeAllChildren(%item);
    %this.insertItem(%item, $UserLoadingText);
    %rpcObject = new RPC_ServerManagement() {
    };
    %rpcObject.Command = COMMAND_GET_FRIEND_LIST;
    %rpcObject.arguments = getField(%value, 2);
    %rpcObject.userItem = %item;
    %rpcObject.fireRPC(MasterServerConnection);
}

function AdminUsersList::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (getField(%value, 0))
    {
        %clusterid = getField(%value, 2);
        %rpcObject = new RPC_ServerManagement() {
        };
        %rpcObject.Command = COMMAND_GET_USER_INFO;
        %rpcObject.arguments = getField(%value, 2);
        %rpcObject.fireRPC(MasterServerConnection);
    }
    else
    {
        AdminServerManagement._(Users)._(userID).setText(getField(%value, 2));
        AdminServerManagement._(Users)._(ClusterID).setText("");
        AdminServerManagement._(Users)._(username).setText(getField(%value, 1));
        AdminServerManagement._(Users)._(owningSlave).setText("");
        AdminServerManagement._(Users)._(authLevel).setText("");
        AdminServerManagement._(Users)._(currentLoc).setText("");
        AdminServerManagement._(Users)._(currentInst).setText("");
        %rpcObject = new RPC_ServerManagement() {
        };
        %rpcObject.Command = COMMAND_GET_FRIEND_INFO;
        %rpcObject.arguments = getField(%value, 2);
        %rpcObject.fireRPC(MasterServerConnection);
    }
}

function AdminUsersList::addUser(%this, %id, %name, %clusterid)
{
    %parentID = %this.insertItem(0, %name, 1 TAB %id TAB %clusterid, "", 30, 30);
    %this.insertItem(%parentID, $UserLoadingText);
    return %parentID;
}

function AdminUsersList::AddFriend(%this, %parent, %id, %name, %state)
{
    %icon = 31;
    if (%state $= "FRIEND_ONLINE")
    {
        %icon = 30;
    }
    else
    {
        if (%state $= "FRIEND_PENDING")
        {
            %icon = 32;
        }
        else
        {
            if (%state $= "FRIEND_ACCEPTING")
            {
                %icon = 32;
            }
            else
            {
                if (%state $= "IGNORED_USER")
                {
                    %icon = 33;
                }
            }
        }
    }
    return %this.insertItem(%parent, %name, 0 TAB %name TAB %id TAB %state, "", %icon, %icon);
}
