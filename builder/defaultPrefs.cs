
package builderSystem
{
    function setModuleDefaults()
    {
        Parent::setModuleDefaults();
    }

    function saveModulePreferences()
    {
        Parent::saveModulePreferences();

        if (!$journal::Reading)
        {
            echo("Exporting builder preferences");
            export("$builderpref::*", "cfg/builderprefs.cfg", False);
            %file = new FileObject("") {
            };

            if (%file.openForAppend("cfg/builderprefs.cfg"))
            {
                %file.writeLine("EWorldEditor.renderPlane = \"" @ EWorldEditor.renderPlane @ "\";");
                %file.writeLine("EWorldEditor.renderPlaneHashes = \"" @ EWorldEditor.renderPlaneHashes @ "\";");
                %file.writeLine("EWorldEditor.renderObjText = \"" @ EWorldEditor.renderObjText @ "\";");
                %file.writeLine("EWorldEditor.renderObjHandle = \"" @ EWorldEditor.renderObjHandle @ "\";");
                %file.writeLine("EWorldEditor.renderSelectionBox = \"" @ EWorldEditor.renderSelectionBox @ "\";");
                %file.writeLine("EWorldEditor.planeDim = \"" @ EWorldEditor.planeDim @ "\";");
                %file.writeLine("EWorldEditor.gridSize = \"" @ EWorldEditor.gridSize @ "\";");
                %file.writeLine("EWorldEditor.planarMovement = \"" @ EWorldEditor.planarMovement @ "\";");
                %file.writeLine("EWorldEditor.boundingBoxCollision = \"" @ EWorldEditor.boundingBoxCollision @ "\";");
                %file.writeLine("EWorldEditor.axisGizmoActive = \"" @ EWorldEditor.axisGizmoActive @ "\";");
                %file.writeLine("EWorldEditor.objectsUseBoxCenter = \"" @ EWorldEditor.objectsUseBoxCenter @ "\";");
                %file.writeLine("EWorldEditor.minScaleFactor = \"" @ EWorldEditor.minScaleFactor @ "\";");
                %file.writeLine("EWorldEditor.maxScaleFactor = \"" @ EWorldEditor.maxScaleFactor @ "\";");
                %file.writeLine("EWorldEditor.snapToGrid = \"" @ EWorldEditor.snapToGrid @ "\";");
                %file.writeLine("EWorldEditor.snapRotations = \"" @ EWorldEditor.snapRotations @ "\";");
                %file.writeLine("EWorldEditor.rotationSnap = \"" @ EWorldEditor.rotationSnap @ "\";");
                %file.writeLine("EWorldEditor.showMousePopupInfo = \"" @ EWorldEditor.showMousePopupInfo @ "\";");
                %file.writeLine("EWorldEditor.mouseMoveScale = \"" @ EWorldEditor.mouseMoveScale @ "\";");
                %file.writeLine("EWorldEditor.mouseRotateScale = \"" @ EWorldEditor.mouseRotateScale @ "\";");
                %file.writeLine("EWorldEditor.mouseScaleScale = \"" @ EWorldEditor.mouseScaleScale @ "\";");
                %file.writeLine("EWorldEditor.projectDistance = \"" @ EWorldEditor.projectDistance @ "\";");
                %file.writeLine("EWorldEditor.axisGizmoMaxScreenLen = \"" @ EWorldEditor.axisGizmoMaxScreenLen @ "\";");
            }
            %file.delete();
        }
    }

    function loadModulePreferences()
    {
        Parent::loadModulePreferences();
        echo("Loading builder preferences");
        %infoObject = "";
        
        %file = new FileObject() {
        };

        %open = 0;
        if ($journal::Reading)
        {
            echo("Attempting to open journal\'s matching builder config file.");
            %open = %file.openForRead($journal::File @ "_builderprefs.cfg");
            if (!%open)
            {
                echo("Using default builder config file.");
                %open = %file.openForRead("cfg/builderprefs.cfg");
            }
        }
        else
        {
            %open = %file.openForRead("cfg/builderprefs.cfg");
        }

        if (%open)
        {
            %inInfoBlock = 0;
            while (!%file.isEOF())
            {
                %line = %file.readLine();
                %line = trim(%line);
                %infoObject = %infoObject @ %line @ " ";
            }
            %file.close();
        }

        eval(%infoObject);
        %file.delete();
    }
};


