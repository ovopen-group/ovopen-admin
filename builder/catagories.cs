
function ClientCmdReturnCatChildren(%unused, %parentID, %folders, %objects)
{
    WE_ObjectSourceTree.addChildren(%parentID, %folders, %objects);
}

function ClientCmdReturnAppendCatChildren(%unused, %parentID, %folders, %objects)
{
    WE_ObjectSourceTree.appendChildren(%parentID, %folders, %objects);
}

function ClientCmdReturnGhostID(%ghostId)
{
    %id = LocationGameConnection.resolveGhostID(%ghostId);
    WE_ObjectSourceTree.selectAndDrop(%id);
}
