new GuiControlProfile(GuiBuilderSaveProfile) {
   fontColorHL = "255 0 0";
};

exec("./saveBuilderDlg.gui");

new MessageVector(SaveBuilderVectorText) {
};

function saveBuilderDlg::onWake(%this)
{
    %this._(saveVector).attach(SaveBuilderVectorText);
}

function saveBuilderDlg::onSleep(%this)
{
    SaveBuilderVectorText.clear();
}

function ClientCmdStartSaveBuilder(%username)
{
    $isInSaveMode = 1;
    saveBuilderDlg.hasError = 0;
    saveBuilderDlg._("closeButton").setVisible(0);
    Canvas.pushDialog(saveBuilderDlg);
    SaveBuilderVectorText.clear();
    SaveBuilderVectorText.pushBackLine(%username @ " requested save, saving location....", 0);
}

function ClientCmdBuilderSaveMessage(%message)
{
    if (getWord(%message, 0) $= "Error:")
    {
        %message = "\c1" @ %message;
        saveBuilderDlg.hasError = 1;
    }
    
    SaveBuilderVectorText.pushBackLine(%message, 0);
}

function ClientCmdEndSaveBuilder()
{
    ETerrainEditor.isMissionDirty = 0;
    ETerrainEditor.isDirty = 0;
    EWorldEditor.isDirty = 0;
    $isInSaveMode = 0;

    if (!saveBuilderDlg.hasError)
    {
        Canvas.popDialog(saveBuilderDlg);
    }
    else
    {
        saveBuilderDlg._("closeButton").setVisible(1);
    }
}
