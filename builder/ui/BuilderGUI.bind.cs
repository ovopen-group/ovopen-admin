if (isObject(BuilderGUIMap))
{
    BuilderGUIMap.delete();
}

new ActionMap(BuilderGUIMap) {
};

function toggleCamera(%val)
{
    if (%val)
    {
        commandToServer('ToggleCamera');
    }
}

function duplicateObject(%val)
{
    if (%val)
    {
        EWorldEditor.duplicateSelection();
    }
}

BuilderGUIMap.bind(keyboard, "alt c", toggleCamera);
BuilderGUIMap.bind(keyboard, "f5", WE_refreshSelected);
BuilderGUIMap.bind(keyboard, "ctrl d", duplicateObject);
BuilderGUIMap.bindCmd(keyboard, enter, "", "MainChatHUD.focus();");
BuilderGUIMap.bindCmd(keyboard, "ctrl enter", "", "MainChatHUD.show();");
BuilderGUIMap.bindCmd(keyboard, enter, "", "MainChatHUD.focus();");
BuilderGUIMap.bindCmd(keyboard, slash, "", "MainChatHUD.setCommand(\"/\");");
BuilderGUIMap.bindCmd(keyboard, backspace, "", "MainChatHUD.reply();");
