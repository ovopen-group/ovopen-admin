
function InitializeBuilderTree()
{
    new SimGroup(BuilderGroup) {
    };
    new SimGroup(dontTouch) {
    };
    new SimGroup(Datablocks) {
    };
    new SimGroup(scene_objects) {
    };
    new SimGroup(interior_objects) {
    };
    new SimGroup(interactive_objects) {
    };
    new SimGroup(World_Objects) {
    };
    new SimGroup(Water_Blocks) {
    };
    new SimGroup(suns) {
    };
    new SimGroup(Shape_Replicators) {
    };
    new SimGroup(Foliage_Replicators) {
    };
    new SimGroup(Particle_Emitters) {
    };
    new SimGroup(Audio_Emitters) {
    };
    new SimGroup(Lights) {
    };
    new SimGroup(Onverse_Objects) {
    };
    new SimGroup(Poi_Objects) {
    };
    new SimGroup(home_areas) {
    };
    new SimGroup(store_areas) {
    };
    new SimGroup(triggers) {
    };
    new SimGroup(game_areas) {
    };
    new SimGroup(Blockers) {
    };
    new SimGroup(gameScriptObjects) {
    };

    dontTouch.add(Datablocks);
    BuilderGroup.add(scene_objects);
    BuilderGroup.add(interior_objects);
    BuilderGroup.add(interactive_objects);
    BuilderGroup.add(World_Objects);
    BuilderGroup.add(Onverse_Objects);
    World_Objects.add(Water_Blocks);
    World_Objects.add(suns);
    World_Objects.add(Foliage_Replicators);
    World_Objects.add(Shape_Replicators);
    World_Objects.add(Particle_Emitters);
    World_Objects.add(Audio_Emitters);
    World_Objects.add(Lights);
    Onverse_Objects.add(Poi_Objects);
    Onverse_Objects.add(home_areas);
    Onverse_Objects.add(store_areas);
    Onverse_Objects.add(triggers);
    Onverse_Objects.add(game_areas);
    Onverse_Objects.add(Blockers);
    Onverse_Objects.add(gameScriptObjects);
    LocationGameConnection.add(BuilderGroup);
    LocationGameConnection.add(dontTouch);
    %skip = 0;
    %last = 0;
    while (%skip != LocationGameConnection.getCount())
    {
        %obj = LocationGameConnection.getObject(%skip);
        if (%obj == %last)
        {
            %skip++;
        }
        else
        {
            %last = %obj;
            if (!BuilderTreePlaceObject(%obj))
            {
                %skip++;
            }
        }
    }
}

function BuilderTreePlaceObject(%obj)
{
    if (isDatablock(%obj))
    {
        Datablocks.add(%obj);
        return 1;
    }
    
    if (%obj.getClassName() $= "ClientEditSky")
    {
        BuilderGroup.add(%obj);
    }
    else
    {
        if (%obj.getClassName() $= "ClientEditTSStatic")
        {
            scene_objects.add(%obj);
        }
        else
        {
            if (%obj.getClassName() $= "ClientEditInteriorInstance")
            {
                interior_objects.add(%obj);
            }
            else
            {
                if (%obj.getClassName() $= "ClientEditInteractiveObject")
                {
                    interactive_objects.add(%obj);
                }
                else
                {
                    if (%obj.getClassName() $= "ClientEditWaterBlock")
                    {
                        Water_Blocks.add(%obj);
                    }
                    else
                    {
                        if (%obj.getClassName() $= "ClientEditSunLight")
                        {
                            suns.add(%obj);
                        }
                        else
                        {
                            if (%obj.getClassName() $= "ClientEditSun")
                            {
                                suns.add(%obj);
                            }
                            else
                            {
                                if (%obj.getClassName() $= "ClientEditShapeReplicator")
                                {
                                    Shape_Replicators.add(%obj);
                                }
                                else
                                {
                                    if (%obj.getClassName() $= "ClientEditFoliageReplicator")
                                    {
                                        Foliage_Replicators.add(%obj);
                                    }
                                    else
                                    {
                                        if (%obj.getClassName() $= "ClientEditAudioEmitter")
                                        {
                                            Audio_Emitters.add(%obj);
                                        }
                                        else
                                        {
                                            if (%obj.getClassName() $= "ClientEditParticleEmitterNode")
                                            {
                                                Particle_Emitters.add(%obj);
                                            }
                                            else
                                            {
                                                if (%obj.getClassName() $= "ClientEditLightObject")
                                                {
                                                    Lights.add(%obj);
                                                }
                                                else
                                                {
                                                    if (%obj.getClassName() $= "ClientEditPoiPoint")
                                                    {
                                                        Poi_Objects.add(%obj);
                                                    }
                                                    else
                                                    {
                                                        if (%obj.getClassName() $= "ClientEditHomeArea")
                                                        {
                                                            home_areas.add(%obj);
                                                        }
                                                        else
                                                        {
                                                            if (%obj.getClassName() $= "ClientEditHomePoint")
                                                            {
                                                                home_areas.add(%obj);
                                                            }
                                                            else
                                                            {
                                                                if (%obj.getClassName() $= "ClientEditStoreArea")
                                                                {
                                                                    store_areas.add(%obj);
                                                                }
                                                                else
                                                                {
                                                                    if (%obj.getClassName() $= "ClientEditTrigger")
                                                                    {
                                                                        triggers.add(%obj);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (%obj.getClassName() $= "ClientEditGameArea")
                                                                        {
                                                                            game_areas.add(%obj);
                                                                        }
                                                                        else
                                                                        {
                                                                            if (%obj.getClassName() $= "ClientEditPlayerBlocker")
                                                                            {
                                                                                Blockers.add(%obj);
                                                                            }
                                                                            else
                                                                            {
                                                                                if (%obj.getClassName() $= "ClientEditVehicleBlocker")
                                                                                {
                                                                                    Blockers.add(%obj);
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (%obj.getClassName() $= "ClientEditCameraBlocker")
                                                                                    {
                                                                                        Blockers.add(%obj);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (%obj.getClassName() $= "ClientEditGameScriptObject")
                                                                                        {
                                                                                            gameScriptObjects.add(%obj);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (%obj.getClassName() $= "ParticleEmitter")
                                                                                            {
                                                                                                dontTouch.add(%obj);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (%obj.getClassName() $= "Camera")
                                                                                                {
                                                                                                    dontTouch.add(%obj);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (%obj.getClassName() $= "Player")
                                                                                                    {
                                                                                                        dontTouch.add(%obj);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        if (%obj.getClassName() $= "sgLightObject")
                                                                                                        {
                                                                                                            dontTouch.add(%obj);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if (%obj.getClassName() $= "PurchaseClothingObject")
                                                                                                            {
                                                                                                                dontTouch.add(%obj);
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                if (%obj.getClassName() $= "PurchaseAIPet")
                                                                                                                {
                                                                                                                    dontTouch.add(%obj);
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    if (%obj.getClassName() $= "PurchaseMountablePlayer")
                                                                                                                    {
                                                                                                                        dontTouch.add(%obj);
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        if (%obj.getClassName() $= "PurchaseInteractiveObject")
                                                                                                                        {
                                                                                                                            dontTouch.add(%obj);
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            if (%obj.getClassName() $= "PurchaseShoulderPet")
                                                                                                                            {
                                                                                                                                dontTouch.add(%obj);
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                if (%obj.getClassName() $= "PurchaseToolObject")
                                                                                                                                {
                                                                                                                                    dontTouch.add(%obj);
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    return 0;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 1;
}

function WE_ObjectSourceTree::onMoveItem(%this, %item, %unused)
{
}

function WE_ObjectSourceTree::onDeleteSelection(%this)
{
}

function WE_ObjectSourceTree::requestMoveCatObject(%this, %typeName, %unused, %item, %unused)
{
}

function WE_ObjectSourceTree::requestAddCategory(%this, %typeName, %unused, %unused)
{
}

function WE_ObjectSourceTree::requestDeleteCategory(%this, %catID)
{
}

function WE_ObjectSourceTree::requestRenameCatObject(%this, %typeName, %unused, %unused, %catID)
{
}

function WE_ObjectSourceTree::onWake(%this)
{
    %this.clear();
    %this.buildIconTable();
    %this.addRootType($objects::tableID[scene_objects], "Scene Objects");
    %this.addRootType($objects::tableID[interior_objects], "Interior Objects");
    %this.addRootType($objects::tableID[particle_objects], "Particle Objects");
    %this.addRootType($objects::tableID[light_objects], "Light Objects");
    %this.addRootType($objects::tableID[interactive_objects], "Interactive Objects");
    %this.addRootType($objects::tableID[npc_objects], "NPC Objects");
    %folder = %this.addRootType(0, "World Objects");
    %this.removeAllChildren(%folder);
    %this.addObject(%folder, "Water Block", 0, 10000);
    %this.addObject(%folder, "Sun", 0, 10001);
    %this.addObject(%folder, "Foliage Replicator", 0, 10002);
    %this.addObject(%folder, "Shape Replicator", 0, 10003);
    %this.addObject(%folder, "Audio Emitter", 0, 10005);
    %this.addObject(%folder, "Sun Light FX", 0, 10006);
    %this.addObject(%folder, "Precipitation", 0, 10011);
    %this.addObject(%folder, "Lightning", 0, 10012);
    %folder = %this.addRootType(0, "Onverse Objects");
    %this.removeAllChildren(%folder);
    %this.addObject(%folder, "PoiPoint", 0, 11005);
    %this.addObject(%folder, "HomeArea", 0, 11006);
    %this.addObject(%folder, "HomePoint", 0, 11007);
    %this.addObject(%folder, "StoreArea", 0, 11008);
    %this.addObject(%folder, "Trigger", 0, 11009);
    %this.addObject(%folder, "GameArea", 0, 11010);
    %this.addObject(%folder, "PlayerBlocker", 0, 11011);
    %this.addObject(%folder, "VehicleBlocker", 0, 11012);
    %this.addObject(%folder, "CameraBlocker", 0, 11013);
    %this.addObject(%folder, "GameScriptObject", 0, 11014);
}

function WE_ObjectSourceTree::requestCatChildren(%this, %typeName, %catID, %folderID)
{
    commandToServer('GetCatChildren', $objects::tableID[%typeName], %folderID, %catID);
}

function WE_ObjectSourceTree::onSelect(%this, %item)
{
    %this.clearSelection();
    %value = %this.getItemValue(%item);
    if (!getField(%value, 0))
    {
        %type = getField(%value, 1);
        %oid = getField(%value, 2);
        commandToServer('CreateObject', %type, %oid);
    }
}

function WE_ObjectSourceTree::selectAndDrop(%this, %obj)
{
    if (%obj > 0)
    {
        EWorldEditor.clearSelection();
        EWorldEditor.selectObject(%obj);
        EWorldEditor.dropSelection();
        EWorldEditor.clearSelection();
        EWorldEditor.selectObject(%obj);
    }
}
