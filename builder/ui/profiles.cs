
new GuiControlProfile(MissionEditorProfile) {
   canKeyFocus = "1";
};

new GuiControlProfile(EditorTextProfile) {
   fontType = "Arial Bold";
   fontColor = "0 0 0";
   autoSizeWidth = "1";
   autoSizeHeight = "1";
};

new GuiControlProfile(EditorTextProfileWhite) {
   fontType = "Arial Bold";
   fontColor = "255 255 255";
   autoSizeWidth = "1";
   autoSizeHeight = "1";
};

new GuiControlProfile(GuiEditorScrollProfile : GuiScrollProfile) {
   fillColor = "255 255 255 0";
};

new GuiControlProfile(GuiEditorWindowProfile : GuiWindowProfile) {
   fillColor = "255 255 255 0";
};

new GuiControlProfile(GuiEditorTabProfile) {
   bitmap = "";
   opaque = "0";
   borderThickness = "1";
   borderColor = "20 20 20 210";
   borderColorHL = "240 240 240 210";
   fillColor = "201 182 153 210";
   fillColorNA = "181 172 143 210";
   fillColorHL = "231 222 153 210";
   justify = "center";
};
