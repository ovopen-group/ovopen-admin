new GuiCursor(EditorHandCursor) {
   hotSpot = "7 0";
   bitmapName = "./cursors/CUR_hand.png";
};

new GuiCursor(EditorRotateCursor) {
   hotSpot = "11 18";
   bitmapName = "./cursors/CUR_rotate.png";
};

new GuiCursor(EditorMoveCursor) {
   hotSpot = "9 13";
   bitmapName = "./cursors/CUR_grab.png";
};

new GuiCursor(EditorArrowCursor) {
   hotSpot = "0 0";
   bitmapName = "./cursors/CUR_3darrow.png";
};
