exec("./BuilderGUI.gui");

package BuilderGUIFunctionOverride
{
    function escapeFromGame()
    {
        BuilderGUI.QuitLocation();
    }
};

function BuilderGUI::QuitLocation(%this)
{
    if ($location::builder && (ETerrainEditor.isMissionDirty || ETerrainEditor.isDirty || EWorldEditor.isDirty))
    {
        MessageBoxYesNo("Mission Modified", "Would you like to save your changes before quiting?", "BuilderGUI.DoQuitLocation(true);", "BuilderGUI.DoQuitLocation(false);");
    }
    else
    {
        %this.DoQuitLocation(0);
    }
}

function BuilderGUI::DoQuitLocation(%this, %saveFirst)
{
    if (%saveFirst)
    {
        commandToServer('SaveBuilder');
    }
    else
    {
        %this.close();
        deactivatePackage(BuilderGUIFunctionOverride);
        LocationSystem::disconnect(1);
    }
}

function BuilderGUI::open(%this)
{
    if ($builder::active == 0)
    {
        if ($location::running == 0)
        {
            MessageBoxOK("Error", "Cannot run builder, must be connected to a location", "");
        }
        else
        {
            BuilderGUI.Initialize();
            $builder::previousContent = Canvas.getContent();
            Canvas.setContent(BuilderGUI);
            Canvas.pushDialog(MainChatHUD);
            BuilderGUIMap.push();
            activatePackage(BuilderGUIFunctionOverride);
            BuilderGUI.setWorldEditorVisible();
            $builder::active = 1;
            
            if ($location::builder == 0)
            {
                MessageBoxOK("Warning", "This is not a builder instance, debugging purposes only.", "");
            }

            if ($isInSaveMode == 1)
            {
                echo("World is in the middle of a save, joining wait.");
                Canvas.pushDialog(saveBuilderDlg);
            }
        }
    }
    else
    {
        warn("Builder is already open.");
    }
}

function BuilderGUI::close(%this)
{
    if ($builder::active == 1)
    {
        Canvas.setContent($builder::previousContent);
        BuilderGUIMap.pop();
        $builder::active = 0;

        if ($isInSaveMode == 1)
        {
            echo("World is in the middle of a save, joining wait.");
            Canvas.pushDialog(saveBuilderDlg);
        }
    }
    else
    {
        warn("Builder is already closed.");
    }
}

function BuilderGUI::onWake(%this)
{
}

function BuilderGUI::Initialize(%this)
{
    EditorSelectorDrop.clear();
    EditorSelectorDrop.add("World Editor", 0);
    EditorSelectorDrop.add("Terrain Editor", 1);
    EditorSelectorDrop.setText(EditorSelectorDrop.getTextById(0));
    EditorMenuBar.clearMenus();
    EditorMenuBar.addMenu("File", 0);
    EditorMenuBar.addMenuItem("File", "Save Changes", 1);
    EditorMenuBar.addMenuItem("File", "-", 0);
    EditorMenuBar.addMenuItem("File", "Toggle Builder", 2, "F11");
    EditorMenuBar.addMenuItem("File", "Quit", 3);
    EditorMenuBar.addMenu("Edit", 1);
    EditorMenuBar.addMenuItem("Edit", "Undo", 1, "Ctrl Z");
    EditorMenuBar.setMenuItemBitmap("Edit", "Undo", 1);
    EditorMenuBar.addMenuItem("Edit", "Redo", 2, "Ctrl R");
    EditorMenuBar.setMenuItemBitmap("Edit", "Redo", 2);
    EditorMenuBar.addMenuItem("Edit", "-", 0);
    EditorMenuBar.addMenuItem("Edit", "Cut", 3, "Ctrl X");
    EditorMenuBar.setMenuItemBitmap("Edit", "Cut", 3);
    EditorMenuBar.addMenuItem("Edit", "Copy", 4, "Ctrl C");
    EditorMenuBar.setMenuItemBitmap("Edit", "Copy", 4);
    EditorMenuBar.addMenuItem("Edit", "Paste", 5, "Ctrl V");
    EditorMenuBar.setMenuItemBitmap("Edit", "Paste", 5);
    EditorMenuBar.addMenuItem("Edit", "Duplicate", 5, "");
    EditorMenuBar.setMenuItemBitmap("Edit", "Paste", 5);
    EditorMenuBar.addMenuItem("Edit", "-", 0);
    EditorMenuBar.addMenuItem("Edit", "Select All", 6, "Ctrl A");
    EditorMenuBar.addMenuItem("Edit", "Select None", 7, "Ctrl N");
    EditorMenuBar.addMenu("Camera", 7);
    EditorMenuBar.addMenuItem("Camera", "Drop Camera at Avatar", 1, "Alt Q");
    EditorMenuBar.addMenuItem("Camera", "Drop Avatar at Camera", 2, "Alt W");
    EditorMenuBar.addMenuItem("Camera", "Toggle Camera", 10, "Alt C");
    EditorMenuBar.addMenuItem("Camera", "-", 0);
    EditorMenuBar.addMenuItem("Camera", "Slowest", 3, "Shift 1", 1);
    EditorMenuBar.addMenuItem("Camera", "Very Slow", 4, "Shift 2", 1);
    EditorMenuBar.addMenuItem("Camera", "Slow", 5, "Shift 3", 1);
    EditorMenuBar.addMenuItem("Camera", "Medium Pace", 6, "Shift 4", 1);
    EditorMenuBar.addMenuItem("Camera", "Fast", 7, "Shift 5", 1);
    EditorMenuBar.addMenuItem("Camera", "Very Fast", 8, "Shift 6", 1);
    EditorMenuBar.addMenuItem("Camera", "Fastest", 9, "Shift 7", 1);
    EditorMenuBar.addMenu("World", 6);
    EditorMenuBar.addMenuItem("World", "Lock Selection", 10, "Ctrl L");
    EditorMenuBar.addMenuItem("World", "Unlock Selection", 11, "Ctrl Shift L");
    EditorMenuBar.addMenuItem("World", "-", 0);
    EditorMenuBar.addMenuItem("World", "Hide Selection", 12, "Ctrl H");
    EditorMenuBar.addMenuItem("World", "Show Selection", 13, "Ctrl Shift H");
    EditorMenuBar.addMenuItem("World", "-", 0);
    EditorMenuBar.addMenuItem("World", "Delete Selection", 17, "Delete");
    EditorMenuBar.addMenuItem("World", "Camera To Selection", 14);
    EditorMenuBar.addMenuItem("World", "Reset Transforms", 15);
    EditorMenuBar.addMenuItem("World", "Add Selection to Instant Group", 17);
    EditorMenuBar.addMenuItem("World", "-", 0);
    EditorMenuBar.addMenuItem("World", "Drop at Origin", 0, "", 1);
    EditorMenuBar.addMenuItem("World", "Drop at Camera", 1, "", 1);
    EditorMenuBar.addMenuItem("World", "Drop at Camera w/Rot", 2, "", 1);
    EditorMenuBar.addMenuItem("World", "Drop below Camera", 3, "", 1);
    EditorMenuBar.addMenuItem("World", "Drop at Screen Center", 4, "", 1);
    EditorMenuBar.addMenuItem("World", "Drop at Centroid", 5, "", 1);
    EditorMenuBar.addMenuItem("World", "Drop to Ground", 6, "", 1);
    EditorMenuBar.addMenu("Action", 3);
    EditorMenuBar.addMenuItem("Action", "Select", 1, "", 1);
    EditorMenuBar.addMenuItem("Action", "Adjust Selection", 2, "", 1);
    EditorMenuBar.addMenuItem("Action", "-", 0);
    EditorMenuBar.addMenuItem("Action", "Add Dirt", 3, "", 1);
    EditorMenuBar.addMenuItem("Action", "Excavate", 4, "", 1);
    EditorMenuBar.addMenuItem("Action", "Adjust Height", 5, "", 1);
    EditorMenuBar.addMenuItem("Action", "Flatten", 6, "", 1);
    EditorMenuBar.addMenuItem("Action", "Smooth", 7, "", 1);
    EditorMenuBar.addMenuItem("Action", "Set Height", 8, "", 1);
    EditorMenuBar.addMenuItem("Action", "-", 0);
    EditorMenuBar.addMenuItem("Action", "Set Empty", 9, "", 1);
    EditorMenuBar.addMenuItem("Action", "Clear Empty", 10, "", 1);
    EditorMenuBar.addMenuItem("Action", "-", 0);
    EditorMenuBar.addMenuItem("Action", "Paint Material", 11, "", 1);
    EditorMenuBar.addMenu("Brush", 4);
    EditorMenuBar.addMenuItem("Brush", "Box Brush", 91, "", 1);
    EditorMenuBar.addMenuItem("Brush", "Circle Brush", 92, "", 1);
    EditorMenuBar.addMenuItem("Brush", "-", 0);
    EditorMenuBar.addMenuItem("Brush", "Soft Brush", 93, "", 2);
    EditorMenuBar.addMenuItem("Brush", "Hard Brush", 94, "", 2);
    EditorMenuBar.addMenuItem("Brush", "-", 0);
    EditorMenuBar.addMenuItem("Brush", "Size 1 x 1", 1, "Alt 1", 3);
    EditorMenuBar.addMenuItem("Brush", "Size 3 x 3", 3, "Alt 2", 3);
    EditorMenuBar.addMenuItem("Brush", "Size 5 x 5", 5, "Alt 3", 3);
    EditorMenuBar.addMenuItem("Brush", "Size 9 x 9", 9, "Alt 4", 3);
    EditorMenuBar.addMenuItem("Brush", "Size 15 x 15", 15, "Alt 5", 3);
    EditorMenuBar.addMenuItem("Brush", "Size 25 x 25", 25, "Alt 6", 3);
    EditorMenuBar.addMenu("Lighting Tools", 5);
    EditorMenuBar.addMenuItem("Lighting Tools", "Filtered Relight", 1);
    EditorMenuBar.addMenuItem("Lighting Tools", "Full Relight", 2, "Alt L");
    EditorMenuBar.addMenuItem("Lighting Tools", "-", 0);
    EditorMenuBar.addMenuItem("Lighting Tools", "Production", 3, "", 1);
    EditorMenuBar.addMenuItem("Lighting Tools", "Design", 4, "", 1);
    EditorMenuBar.addMenuItem("Lighting Tools", "Draft", 5, "", 1);
    TEBrushShapeDrop.clear();
    TEBrushShapeDrop.add("Box Brush", 91);
    TEBrushShapeDrop.add("Circle Brush", 92);
    TEBrushStrengthDrop.clear();
    TEBrushStrengthDrop.add("Soft Brush", 93);
    TEBrushStrengthDrop.add("Hard Brush", 94);
    TEBrushSizeDrop.clear();
    TEBrushSizeDrop.add("1 x 1", 1);
    TEBrushSizeDrop.add("3 x 3", 3);
    TEBrushSizeDrop.add("5 x 5", 5);
    TEBrushSizeDrop.add("9 x 9", 9);
    TEBrushSizeDrop.add("15 x 15", 15);
    TEBrushSizeDrop.add("25 x 25", 25);
    TEBrushActionDrop.clear();
    TEBrushActionDrop.add("Select", 1);
    TEBrushActionDrop.add("Adjust Selection", 2);
    TEBrushActionDrop.add("Add Dirt", 3);
    TEBrushActionDrop.add("Excavate", 4);
    TEBrushActionDrop.add("Adjust Height", 5);
    TEBrushActionDrop.add("Flatten", 6);
    TEBrushActionDrop.add("Smooth", 7);
    TEBrushActionDrop.add("Set Height", 8);
    TEBrushActionDrop.add("Set Empty", 9);
    TEBrushActionDrop.add("Clear Empty", 10);
    TEBrushActionDrop.add("Paint Material", 11);
    EditorSelectorDrop.setSelected(0);
    EditorMenuBar.onActionMenuItemSelect(2, "Adjust Height");
    EditorMenuBar.onBrushMenuItemSelect(92, "Circle Brush");
    EditorMenuBar.onBrushMenuItemSelect(93, "Soft Brush");
    EditorMenuBar.onBrushMenuItemSelect(9, "Size 9 x 9");
    EditorMenuBar.onCameraMenuItemSelect(6, "Medium Pace");
    EditorMenuBar.onWorldMenuItemSelect(0, "Drop at Screen Center");

    EWorldEditor.init();
    if ($location::builder)
    {
        ETerrainEditor.networkAttachTerrain();
    }

    WE_ObjectTree.init();
    EWorldEditor.isDirty = 0;
    ETerrainEditor.isDirty = 0;
    ShowServerPersistOnly(1);
    if ($pref::LightManager::sgLightingProfileQuality == 0)
    {
        EditorMenuBar.setMenuItemChecked("Lighting Tools", "Production", 1);
    }
    else
    {
        if ($pref::LightManager::sgLightingProfileQuality == 1)
        {
            EditorMenuBar.setMenuItemChecked("Lighting Tools", "Design", 1);
        }
        else
        {
            if ($pref::LightManager::sgLightingProfileQuality == 2)
            {
                EditorMenuBar.setMenuItemChecked("Lighting Tools", "Draft", 1);
            }
            else
            {
                $pref::LightManager::sgLightingProfileQuality = 0;
                EditorMenuBar.setMenuItemChecked("Lighting Tools", "Production", 1);
            }
        }
    }
    ETerrainEditor.setupPaintList();
}

function EditorMenuBar::onMenuSelect(%this, %unused, %menu)
{
    if (%menu $= "Edit")
    {
        if (EWorldEditor.isVisible())
        {
            EditorMenuBar.setMenuItemEnable("Edit", "Select All", 1);
            %canCutCopy = EWorldEditor.getSelectionSize() > 0;
            EditorMenuBar.setMenuItemEnable("Edit", "Cut", 0);
            EditorMenuBar.setMenuItemEnable("Edit", "Copy", 0);
            EditorMenuBar.setMenuItemEnable("Edit", "Paste", 0);
            EditorMenuBar.setMenuItemEnable("Edit", "Duplicate", %canCutCopy);
        }
        else
        {
            if (ETerrainEditor.isVisible())
            {
                EditorMenuBar.setMenuItemEnable("Edit", "Cut", 0);
                EditorMenuBar.setMenuItemEnable("Edit", "Copy", 0);
                EditorMenuBar.setMenuItemEnable("Edit", "Paste", 0);
                EditorMenuBar.setMenuItemEnable("Edit", "Select All", 0);
                EditorMenuBar.setMenuItemEnable("Edit", "Duplicate", 0);
            }
        }
    }
    else if(%menu $= "World")
    {
      %selSize = EWorldEditor.getSelectionSize();
      %lockCount = EWorldEditor.getSelectionLockCount();
      %hideCount = EWorldEditor.getSelectionHiddenCount();

      EditorMenuBar.setMenuItemEnable("World", "Lock Selection", %lockCount < %selSize);
      EditorMenuBar.setMenuItemEnable("World", "Unlock Selection", %lockCount > 0);
      EditorMenuBar.setMenuItemEnable("World", "Hide Selection", %hideCount < %selSize);
      EditorMenuBar.setMenuItemEnable("World", "Show Selection", %hideCount > 0);

      EditorMenuBar.setMenuItemEnable("World", "Add Selection to Instant Group", %selSize > 0);
      EditorMenuBar.setMenuItemEnable("World", "Camera To Selection", %selSize > 0);
      EditorMenuBar.setMenuItemEnable("World", "Reset Transforms", %selSize > 0 && %lockCount == 0);
      EditorMenuBar.setMenuItemEnable("World", "Drop Selection", %selSize > 0 && %lockCount == 0);
      EditorMenuBar.setMenuItemEnable("World", "Delete Selection", %selSize > 0 && %lockCount == 0);
    }
}

function EditorMenuBar::onMenuItemSelect(%this, %unused, %menu, %itemId, %item)
{
    if (%menu $= "File")
    {
        %this.onFileMenuItemSelect(%itemId, %item);
    }
    else
    {
        if (%menu $= "Edit")
        {
            %this.onEditMenuItemSelect(%itemId, %item);
        }
        else
        {
            if (%menu $= "World")
            {
                %this.onWorldMenuItemSelect(%itemId, %item);
            }
            else
            {
                if (%menu $= "Camera")
                {
                    %this.onCameraMenuItemSelect(%itemId, %item);
                }
                else
                {
                    if (%menu $= "Action")
                    {
                        %this.onActionMenuItemSelect(%itemId, %item);
                    }
                    else
                    {
                        if (%menu $= "Brush")
                        {
                            %this.onBrushMenuItemSelect(%itemId, %item);
                        }
                        else
                        {
                            if (%menu $= "Lighting Tools")
                            {
                                %this.onLightMenuItemSelect(%itemId, %item);
                            }
                        }
                    }
                }
            }
        }
    }
}

function EditorMenuBar::onFileMenuItemSelect(%this, %itemId, %item)
{
    if (%item $= "Save Changes")
    {
        if ($location::builder == 0)
        {
            MessageBoxOK("Warning", "This is not a builder instance, debugging purposes only.", "");
        }
        else
        {
            commandToServer('SaveBuilder');
        }
    }
    else
    {
        if (%item $= "Toggle Builder")
        {
            BuilderGUI.close();
        }
        else
        {
            if (%item $= "Quit")
            {
                BuilderGUI.QuitLocation();
            }
        }
    }
}

function EditorMenuBar::onWorldMenuItemSelect(%this, %itemId, %item)
{
    if (%item $= "Lock Selection")
    {
        EWorldEditor.lockSelection(1);
    }
    else
    {
        if (%item $= "Unlock Selection")
        {
            EWorldEditor.lockSelection(0);
        }
        else
        {
            if (%item $= "Hide Selection")
            {
                EWorldEditor.hideSelection(1);
            }
            else
            {
                if (%item $= "Show Selection")
                {
                    EWorldEditor.hideSelection(0);
                }
                else
                {
                    if (%item $= "Camera To Selection")
                    {
                        EWorldEditor.dropCameraToSelection();
                    }
                    else
                    {
                        if (%item $= "Reset Transforms")
                        {
                            EWorldEditor.resetTransforms();
                        }
                        else
                        {
                            if (%item $= "Drop Selection")
                            {
                                EWorldEditor.dropSelection();
                            }
                            else
                            {
                                if (%item $= "Delete Selection")
                                {
                                    EWorldEditor.deleteSelection();
                                    Inspector.uninspect();
                                }
                                else
                                {
                                    if (%item $= "Add Selection to Instant Group")
                                    {
                                        EWorldEditor.addSelectionToAddGroup();
                                    }
                                    else
                                    {
                                        EditorMenuBar.setMenuItemChecked("World", %item, 1);
                                        if (%item $= "Drop at Origin")
                                        {
                                            EWorldEditor.dropType = "atOrigin";
                                        }
                                        else
                                        {
                                            if (%item $= "Drop at Camera")
                                            {
                                                EWorldEditor.dropType = "atCamera";
                                            }
                                            else
                                            {
                                                if (%item $= "Drop at Camera w/Rot")
                                                {
                                                    EWorldEditor.dropType = "atCameraRot";
                                                }
                                                else
                                                {
                                                    if (%item $= "Drop below Camera")
                                                    {
                                                        EWorldEditor.dropType = "belowCamera";
                                                    }
                                                    else
                                                    {
                                                        if (%item $= "Drop at Screen Center")
                                                        {
                                                            EWorldEditor.dropType = "screenCenter";
                                                        }
                                                        else
                                                        {
                                                            if (%item $= "Drop to Ground")
                                                            {
                                                                EWorldEditor.dropType = "toGround";
                                                            }
                                                            else
                                                            {
                                                                if (%item $= "Drop at Centroid")
                                                                {
                                                                    EWorldEditor.dropType = "atCentroid";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function EditorMenuBar::onEditMenuItemSelect(%this, %itemId, %item)
{
    if (EWorldEditor.isVisible())
    {
        if (%item $= "Undo")
        {
            EWorldEditor.undo();
        }
        else
        {
            if (%item $= "Redo")
            {
                EWorldEditor.redo();
            }
            else
            {
                if (%item $= "Copy")
                {
                    EWorldEditor.copySelection();
                }
                else
                {
                    if (%item $= "Cut")
                    {
                        EWorldEditor.copySelection();
                        EWorldEditor.deleteSelection();
                        Inspector.uninspect();
                    }
                    else
                    {
                        if (%item $= "Paste")
                        {
                            EWorldEditor.pasteSelection();
                        }
                        else
                        {
                            if (%item $= "Duplicate")
                            {
                                EWorldEditor.duplicateSelection();
                            }
                            else
                            {
                                if (%item $= "Select All")
                                {
                                }
                                else
                                {
                                    if (%item $= "Select None")
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (ETerrainEditor.isVisible())
        {
            if (%item $= "Undo")
            {
                ETerrainEditor.undo();
            }
            else
            {
                if (%item $= "Redo")
                {
                    ETerrainEditor.redo();
                }
                else
                {
                    if (%item $= "Select None")
                    {
                        ETerrainEditor.clearSelection();
                    }
                }
            }
        }
    }
}

function EditorMenuBar::onCameraMenuItemSelect(%this, %itemId, %item)
{
    if (%item $= "Drop Camera at Avatar")
    {
        AC_DropCam();
    }
    else
    {
        if (%item $= "Drop Avatar at Camera")
        {
            AC_DropAvatar();
        }
        else
        {
            if (%item $= "Toggle Camera")
            {
                AC_Fly();
            }
            else
            {
                %this.setMenuItemChecked("Camera", %itemId, 1);
                %values[0] = 5;
                %values[1] = 7.5;
                %values[2] = 13;
                %values[3] = 38;
                %values[4] = 80;
                %values[5] = 160;
                %values[6] = 320;
                AC_CamSpeed(%values[%itemId - 3]);
            }
        }
    }
}

function EditorMenuBar::onActionMenuItemSelect(%this, %itemId, %item, %recurse)
{
    EditorMenuBar.setMenuItemChecked("Action", %item, 1);
    if (%item $= "Select")
    {
        ETerrainEditor.currentMode = "select";
        ETerrainEditor.selectionHidden = 0;
        ETerrainEditor.renderVertexSelection = 1;
        ETerrainEditor.setAction("select");
    }
    else
    {
        if (%item $= "Adjust Selection")
        {
            ETerrainEditor.currentMode = "adjust";
            ETerrainEditor.selectionHidden = 0;
            ETerrainEditor.setAction("adjustHeight");
            ETerrainEditor.currentAction = brushAdjustHeight;
            ETerrainEditor.renderVertexSelection = 1;
        }
        else
        {
            ETerrainEditor.currentMode = "paint";
            ETerrainEditor.selectionHidden = 1;
            ETerrainEditor.setAction(ETerrainEditor.currentAction);
            if (%item $= "Add Dirt")
            {
                ETerrainEditor.currentAction = raiseHeight;
                ETerrainEditor.renderVertexSelection = 1;
            }
            else
            {
                if (%item $= "Paint Material")
                {
                    ETerrainEditor.currentAction = paintMaterial;
                    ETerrainEditor.renderVertexSelection = 1;
                }
                else
                {
                    if (%item $= "Excavate")
                    {
                        ETerrainEditor.currentAction = lowerHeight;
                        ETerrainEditor.renderVertexSelection = 1;
                    }
                    else
                    {
                        if (%item $= "Set Height")
                        {
                            ETerrainEditor.currentAction = setHeight;
                            ETerrainEditor.renderVertexSelection = 1;
                        }
                        else
                        {
                            if (%item $= "Adjust Height")
                            {
                                ETerrainEditor.currentAction = brushAdjustHeight;
                                ETerrainEditor.renderVertexSelection = 1;
                            }
                            else
                            {
                                if (%item $= "Flatten")
                                {
                                    ETerrainEditor.currentAction = flattenHeight;
                                    ETerrainEditor.renderVertexSelection = 1;
                                }
                                else
                                {
                                    if (%item $= "Smooth")
                                    {
                                        ETerrainEditor.currentAction = smoothHeight;
                                        ETerrainEditor.renderVertexSelection = 1;
                                    }
                                    else
                                    {
                                        if (%item $= "Set Empty")
                                        {
                                            ETerrainEditor.currentAction = setEmpty;
                                            ETerrainEditor.renderVertexSelection = 0;
                                        }
                                        else
                                        {
                                            if (%item $= "Clear Empty")
                                            {
                                                ETerrainEditor.currentAction = clearEmpty;
                                                ETerrainEditor.renderVertexSelection = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (ETerrainEditor.currentMode $= "select")
            {
                ETerrainEditor.processAction(ETerrainEditor.currentAction);
            }
            else
            {
                if (ETerrainEditor.currentMode $= "paint")
                {
                    ETerrainEditor.setAction(ETerrainEditor.currentAction);
                }
            }
        }
    }
    if (%recurse $= "")
    {
        TEBrushActionDrop.setSelected(TEBrushActionDrop.findText(%item));
    }
}

function EditorMenuBar::onBrushMenuItemSelect(%this, %itemId, %item, %recurse)
{
    EditorMenuBar.setMenuItemChecked("Brush", %item, 1);
    if (%item $= "Box Brush")
    {
        ETerrainEditor.setBrushType(box);
        if (%recurse $= "")
        {
            TEBrushShapeDrop.setSelected(%itemId);
        }
    }
    else
    {
        if (%item $= "Circle Brush")
        {
            ETerrainEditor.setBrushType(ellipse);
            if (%recurse $= "")
            {
                TEBrushShapeDrop.setSelected(%itemId);
            }
        }
        else
        {
            if (%item $= "Soft Brush")
            {
                ETerrainEditor.enableSoftBrushes = 1;
                if (%recurse $= "")
                {
                    TEBrushStrengthDrop.setSelected(%itemId);
                }
            }
            else
            {
                if (%item $= "Hard Brush")
                {
                    ETerrainEditor.enableSoftBrushes = 0;
                    if (%recurse $= "")
                    {
                        TEBrushStrengthDrop.setSelected(%itemId);
                    }
                }
                else
                {
                    ETerrainEditor.brushSize = %itemId;
                    if (%recurse $= "")
                    {
                        TEBrushSizeDrop.setSelected(%itemId);
                    }
                    ETerrainEditor.setBrushSize(%itemId, %itemId);
                }
            }
        }
    }
}

function EditorMenuBar::onLightMenuItemSelect(%this, %itemId, %item, %recurse)
{
    if (%item $= "Filtered Relight")
    {
        RelightScene(1, "", forceAlways);
    }
    else
    {
        if (%item $= "Full Relight")
        {
            RelightScene(0, "", forceAlways);
        }
        else
        {
            if (%item $= "Production")
            {
                $pref::LightManager::sgLightingProfileQuality = 0;
                EditorMenuBar.setMenuItemChecked("Lighting Tools", %item, 1);
            }
            else
            {
                if (%item $= "Design")
                {
                    $pref::LightManager::sgLightingProfileQuality = 1;
                    EditorMenuBar.setMenuItemChecked("Lighting Tools", %item, 1);
                }
                else
                {
                    if (%item $= "Draft")
                    {
                        $pref::LightManager::sgLightingProfileQuality = 2;
                        EditorMenuBar.setMenuItemChecked("Lighting Tools", %item, 1);
                    }
                }
            }
        }
    }
}

function BuilderGUI::setWorldEditorVisible(%this)
{
    TerrainEditorTabs.setVisible(0);
    WorldEditorTabs.setVisible(1);
    EWorldEditor.setVisible(1);
    ETerrainEditor.setVisible(0);
    EditorMenuBar.setMenuVisible("World", 1);
    EditorMenuBar.setMenuVisible("Action", 0);
    EditorMenuBar.setMenuVisible("Brush", 0);
    EWorldEditor.makeFirstResponder(1);
    WE_ObjectTree.open(LocationGameConnection, 1);
    %this._(FramePassThru).setTarget(EWorldEditor);
}

function BuilderGUI::setTerrainEditorVisible(%this)
{
    WorldEditorTabs.setVisible(0);
    TerrainEditorTabs.setVisible(1);
    EWorldEditor.setVisible(0);
    ETerrainEditor.setVisible(1);
    ETerrainEditor.networkAttachTerrain();
    EditorMenuBar.setMenuVisible("World", 0);
    EditorMenuBar.setMenuVisible("Action", 1);
    EditorMenuBar.setMenuVisible("Brush", 1);
    ETerrainEditor.makeFirstResponder(1);
    %this._(FramePassThru).setTarget(ETerrainEditor);
}

function EditorSelectorDrop::onSelect(%this, %id, %text)
{
    if (%text $= "World Editor")
    {
        BuilderGUI.setWorldEditorVisible();
    }
    else
    {
        if (%text $= "Terrain Editor")
        {
            if ($location::builder)
            {
                BuilderGUI.setTerrainEditorVisible();
            }
            else
            {
                MessageBoxOK("Warning", "This is not a builder instance, debugging purposes only.", "");
            }
        }
    }
}

function TEBrushShapeDrop::onSelect(%this, %id)
{
    %name = %this.getText();
    EditorMenuBar.onBrushMenuItemSelect(%id, %name, "false");
}

function TEBrushStrengthDrop::onSelect(%this, %id)
{
    %name = %this.getText();
    EditorMenuBar.onBrushMenuItemSelect(%id, %name, "false");
}

function TEBrushSizeDrop::onSelect(%this, %id)
{
    %name = "Size " @ %this.getText();
    EditorMenuBar.onBrushMenuItemSelect(%id, %name, "false");
}

function TEBrushActionDrop::onSelect(%this, %id)
{
    %name = %this.getText();
    EditorMenuBar.onActionMenuItemSelect(%id, %name, "false");
}

function ETerrainEditor::setupPaintList(%this)
{
    if (!$location::builder)
    {
        return;
    }

    %mats = ETerrainEditor.getTerrainMaterials();
    %valid = 1;

    for (%i=0; %i < 13; %i++)
    {
        %mat = getRecord(%mats, %i);
        ETerrainEditor.mat[%i] = %mat;
        ("TEMaterialText" @ %i).setText(fileBase(%mat));
        ("TEMaterialBitmap" @ %i).setBitmap(%mat);
        ("TEMaterialChange" @ %i).setActive(1);
        ("TEMaterialButton" @ %i).setActive(!(%mat $= ""));
        if (%mat $= "")
        {
            ("TEMaterialChange" @ %i).setText("Add...");
            if (%valid)
            {
                %valid = 0;
            }
            else
            {
                ("TEMaterialChange" @ %i).setActive(0);
            }
        }
        else
        {
            ("TEMaterialChange" @ %i).setText("Change...");
        }
    }
}

function ClientEditTerrainBlock::onServerUpdateMaterials(%this)
{
    if ($builder::active == 1)
    {
        %selected = -1;
        for (%i=0; %i < 13; %i++)
        {
            if (("TEMaterialButton" @ %i).getValue())
            {
                %selected = %i;
            }
        }

        ETerrainEditor.setupPaintList();
        if (%selected != -1)
        {
            ("TEMaterialButton" @ %selected).performClick();
        }
    }
}

function ETerrainEditor::setPaintMaterial(%this, %matIndex)
{
    ETerrainEditor.paintMaterial = ETerrainEditor.mat[%matIndex];
}

function ETerrainEditor::changeMaterial(%this, %matIndex)
{
    ETerrainEditor.matIndex = %matIndex;
    getLoadFilename("*.png\t*.jpg", EPainterChangeMat);
}

function EPainterChangeMat(%file)
{
    for (%i=0; %i < 13; %i++)
    {
        if (ETerrainEditor.mat[%i] $= %file)
        {
            return;
        }
    }
    ETerrainEditor.mat[ETerrainEditor.matIndex] = %file;

    %mats = "";
    for (%i=0; %i < 13; %i++)
    {
        %mats = %mats @ ETerrainEditor.mat[%i] @ "\n";
    }

    ETerrainEditor.setTerrainMaterials(%mats);
    ETerrainEditor.setupPaintList();
    ("TEMaterialButton" @ ETerrainEditor.matIndex).performClick();
}

function ETerrainEditor::applySoftFilter(%this, %val)
{
    ETerrainEditor.softSelectFilter = %val;
    ETerrainEditor.resetSelWeights(1);
    ETerrainEditor.processAction("softSelect");
}

function ETerrainEditor::onGuiUpdate(%this, %text)
{
    %mouseBrushInfo = " (Mouse Brush) #: " @ getWord(%text, 0) @ "  avg: " @ getWord(%text, 1);
    %selectionInfo = " (Selection) #: " @ getWord(%text, 2) @ "  avg: " @ getWord(%text, 3);
    %this._(MouseBrushInfo).setValue(%mouseBrushInfo);
    %this._(MouseBrushInfo1).setValue(%mouseBrushInfo);
    %this._(SelectionInfo).setValue(%selectionInfo);
    %this._(SelectionInfo1).setValue(%selectionInfo);
}

function WE_ObjectTree::onDeleteSelection(%this)
{
    EWorldEditor.copySelection();
}

function WE_ObjectTree::onObjectDeleteCompleted(%this)
{
    Inspector.uninspect();
}

function WE_ObjectTree::onClearSelected(%this)
{
    EWorldEditor.clearSelection();
}

function WE_ObjectTree::init(%this)
{
}

function WE_ObjectTree::onInspect(%this, %obj)
{
    Inspector.inspect(%obj);
}

function WE_ObjectTree::onAddSelection(%this, %obj)
{
    EWorldEditor.selectObject(%obj);
}

function WE_ObjectTree::onRemoveSelection(%this, %obj)
{
    EWorldEditor.unselectObject(%obj);
}

function WE_ObjectTree::onSelect(%this, %obj)
{
    $WE_ObjectTree::CalledWorldSelect = 1;
    EWorldEditor.selectObject(%obj);
    $WE_ObjectTree::CalledWorldSelect = 0;
}

function WE_ObjectTree::onUnselect(%this, %obj)
{
    EWorldEditor.unselectObject(%obj);
}

function ETContextPopup::onSelect(%this, %index, %unused)
{
    if (%index == 0)
    {
        WE_ObjectTree.contextObj.delete();
    }
}

function WE_ObjectTree::onDeleteObject(%this, %obj)
{
    EWorldEditor.onDelete(%obj);
}

function onNeedRelight()
{
    if (RelightMessage.Visible == 0)
    {
        RelightMessage.Visible = 1;
    }
}

function ShowServerPersistOnly(%val)
{
    WE_ServerPersistCheck.setStateOn(%val);
    Inspector.showServerPersist(%val);
}

function WorldEditor::init(%this)
{
    %this.ignoreObjClass(fxShapeReplicatedStatic, ParticleEmitter);
    %this.ignoreObjClass(ClientEditTerrainBlock, ClientEditSky, AIObjective);
    %this.numEditModes = 3;
    %this.editMode[0] = "move";
    %this.editMode[1] = "rotate";
    %this.editMode[2] = "scale";
}

function WorldEditor::onDelete(%this, %obj)
{
    if ($location::builder == 0)
    {
        MessageBoxOK("Warning", "This is not a builder instance, debugging purposes only.", "");
    }
    else
    {
        commandToServer('DeleteObject', %obj.getGhostID());
    }
}

function WorldEditor::onDuplicate(%this, %obj)
{
    if ($location::builder == 0)
    {
        MessageBoxOK("Warning", "This is not a builder instance, debugging purposes only.", "");
    }
    else
    {
        commandToServer('DuplicateObject', %obj.getGhostID());
    }
}

function WorldEditor::onSelect(%this, %obj)
{
    if (!$WE_ObjectTree::CalledWorldSelect)
    {
        if (1 == EWorldEditor.getSelectionSize())
        {
            WE_ObjectTree.clearSelection();
        }
    }

    WE_ObjectTree.addSelection(%obj);
    Inspector.inspect(%obj);
}

function WorldEditor::onUnselect(%this, %obj)
{
    WE_ObjectTree.removeSelection(%obj);
    Inspector.uninspect();
}

function WorldEditor::onClearSelected(%this)
{
    WE_ObjectTree.clearSelection();
    Inspector.uninspect();
}

function WorldEditor::onClearSelection(%this)
{
    WE_ObjectTree.clearSelection();
    Inspector.uninspect();
}

function WE_refreshSelected(%val)
{
    if (%val)
    {
        for (%i=0; %i < EWorldEditor.getSelectionSize(); %i++)
        {
            %obj = EWorldEditor.getSelectedObject(%i);
            if (%obj > 0)
            {
                RefreshObject(%obj);
            }
        }
        EWorldEditor.clearSelection();
    }
}

function RefreshObject(%obj)
{
    commandToServer('RefreshObject', %obj.getGhostID());
}

function WorldEditor::onClick(%this, %obj)
{
    Inspector.inspect(%obj);
}

function WorldEditor::onEndDrag(%this, %obj)
{
    Inspector.inspect(%obj);
}

function WorldEditor::onGuiUpdate(%this, %text)
{
}

function WorldEditor::getSelectionLockCount(%this)
{
    %ret = 0;
    for (%i=0; %i < %this.getSelectionSize(); %i++)
    {
        %obj = %this.getSelectedObject(%i);
        if (%obj.locked $= "true")
        {
            %ret++;
        }
    }
    return %ret;
}

function WorldEditor::getSelectionHiddenCount(%this)
{
    %ret = 0;
    for (%i=0; %i < %this.getSelectionSize(); %i++)
    {
        %obj = %this.getSelectedObject(%i);
        if (%obj.hidden $= "true")
        {
            %ret++;
        }
    }
    return %ret;
}

function WorldEditor::dropCameraToSelection(%this)
{
    if (%this.getSelectionSize() == 0)
    {
        return;
    }

    %pos = %this.getSelectionCentroid();
    %cam = LocalClientConnection.Camera.getTransform();
    %cam = setWord(%cam, 0, getWord(%pos, 0));
    %cam = setWord(%cam, 1, getWord(%pos, 1));
    %cam = setWord(%cam, 2, getWord(%pos, 2));
    LocalClientConnection.Camera.setTransform(%cam);
}

function WorldEditor::moveSelectionInPlace(%this)
{
    %saveDropType = %this.dropType;
    %this.dropType = "atCentroid";
    %this.copySelection();
    %this.deleteSelection();
    %this.pasteSelection();
    %this.dropType = %saveDropType;
}

function WorldEditor::addSelectionToAddGroup(%this)
{
    for (%i=0; %i < %this.getSelectionSize(); %i++)
    {
        %obj = %this.getSelectedObject(%i);
        $InstantGroup.add(%obj);
    }
}

function WorldEditor::resetTransforms(%this)
{
    %this.addUndoState();
    for (%i=0; %i < %this.getSelectionSize(); %i++)
    {
        %obj = %this.getSelectedObject(%i);
        %transform = %obj.getTransform();
        %transform = setWord(%transform, 3, 0);
        %transform = setWord(%transform, 4, 0);
        %transform = setWord(%transform, 5, 1);
        %transform = setWord(%transform, 6, 0);
        %obj.setTransform(%transform);
        %obj.setScale("1 1 1");
    }
}

