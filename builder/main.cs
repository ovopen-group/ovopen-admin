
package builderSystem
{
    function includeFiles()
    {
        Parent::includeFiles();

        echo("Including builder Files...");
        exec("builder/default.bind.cs");
        exec("builder/defaultPrefs.cs");
        exec("builder/ui/BuilderGUI.bind.cs");
        exec("builder/ui/cursors.cs");
        exec("builder/ui/profiles.cs");
        exec("builder/ui/ColorPickerDlg.gui");
        exec("builder/ui/BuilderGUI.cs");
        exec("builder/ui/BuilderSourceTree.cs");
        exec("builder/ui/saveBuilderDlg.cs");
        exec("builder/catagories.cs");
    }
    function onStart()
    {
        Parent::onStart();
        echo("--------- Initializing: Onverse Builder System ---------");
        $builder::active = 0;
    }
};

activatePackage(builderSystem);

function ToggleBuilderUI(%val)
{
    if (%val != 0)
    {
        return;
    }
    
    if ($builder::active == 0)
    {
        BuilderGUI.open();
    }
    else
    {
        BuilderGUI.close();
    }
}
