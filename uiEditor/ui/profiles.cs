
new GuiControlProfile(BackFillProfile) {
   opaque = "1";
   fillColor = "0 94 94";
   border = "1";
   borderColor = "255 128 128";
   fontType = "Arial";
   fontSize = "12";
   fontColor = "0 0 0";
   fixedExtent = "1";
   justify = "center";
};

new GuiControlProfile(GuiEditorClassProfile) {
   opaque = "1";
   fillColor = "232 232 232";
   border = "1";
   borderColor = "0 0 0";
   borderColorHL = "127 127 127";
   fontColor = "0 0 0";
   fixedExtent = "1";
   justify = "center";
   bitmap = ( $platform $= "macos" ) ? "onverse/data/live_assets/engine/ui/images/osxScroll" : "onverse/data/live_assets/engine/ui/images/darkScroll";
   hasBitmapArray = "1";
};
