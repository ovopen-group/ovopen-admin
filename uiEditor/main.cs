
package uiEditor
{
    function includeFiles()
    {
        Parent::includeFiles();
        echo("Including uiEditor Files...");
        exec("uiEditor/default.bind.cs");
        exec("uiEditor/ui/profiles.cs");
        exec("uiEditor/ui/GuiEditorGui.cs");
    }
    
    function onStart()
    {
        Parent::onStart();
        echo("--------- Initializing: Onverse User Interface Editor ---------");
    }
};

activatePackage(uiEditor);

